﻿using System;

namespace Utility.License
{
    public class ShortDate
    {
        public int Day;
        public int Month;
        public int Year;

        public ShortDate()
        {
            DateTime now    = DateTime.Now;
            Day             = now.Date.Day;
            Month           = now.Month - 1;
            Year            = now.Year;
        }

        public ShortDate(int dd, int mm, int yy, bool isDecrementRequired = true)
        {
            if (dd >= 0 && dd <= 31)
            {
                Day = dd;
            }
            else
            {
                throw new FormatException("Invalid Day in Date");
            }

            Month = isDecrementRequired ? mm - 1 : mm;

            if (Month < 0 && Month > 11)
            {
                throw new FormatException("Invalid Month in Date");
            }

            if (yy >= 0 && yy <= 99)
            {
                Year = yy;
            }
            else
            {
                throw new FormatException("Invalid Year in Date");
            }
        }

        public override string ToString()
        {
            return Day + "/" + Month + "/" + Year;
        }
    }
}
