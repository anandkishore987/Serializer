﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Utility.Model
{
    [XmlRoot(IsNullable = true)]
    [Serializable]
    public class DaysofWeekModel
    {
        public bool Sunday      { get; set; }

        public bool Monday      { get; set; }

        public bool Tuesday     { get; set; }

        public bool Wednesday   { get; set; }

        public bool Thursday    { get; set; }

        public bool Friday      { get; set; }

        public bool Saturday    { get; set; }

    }
}
