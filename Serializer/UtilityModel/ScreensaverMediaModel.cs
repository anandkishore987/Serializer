﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Xml.Serialization;
using Utility.Common;

namespace Utility.Model
{
    [Serializable]
    public class ScreensaverMediaModelBase
    {
        [XmlElement("Media")]
        public ObservableCollection<ScreensaverMediaModel> screensaverMediaModel { get;set;}

        public ScreensaverMediaModelBase()
        {
            screensaverMediaModel = new ObservableCollection<ScreensaverMediaModel>();
        }
    }

    [Serializable]
    public class ScreensaverMediaModel : ModelBase
    {
        [XmlElement("MediaPath")]
        public string MediaPath { get; set; }

        [XmlIgnore]
        private bool selectedMediaprop;

        [XmlIgnore]
        public bool SelectedMediaProp
        {
            get
            {
                return selectedMediaprop;
            }
            set
            {
                selectedMediaprop = value;
                OnPropertyChanged("SelectedMediaProp");
            }
        }

        public event EventHandler Click;

        //[XmlIgnore]
        //private RelayCommand _click;

        //[XmlIgnore]
        //public ICommand ClickCommand
        //{
        //    get
        //    {
        //        if (_click == null)
        //            _click = new RelayCommand(param => OnClick());
        //        return _click;
        //    }
        //}

        private void OnClick()
        {
            Click?.Invoke(this, null);
        }

    }
}
