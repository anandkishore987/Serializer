﻿namespace Utility.Model
{
    public class SureLockInfoModel
    {
        public string ExpiredBuildDate  { get; set; }

        public bool IsGracePeriod       { get; set; }
    }
}
