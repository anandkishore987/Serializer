﻿using System.Xml.Serialization;

namespace Utility.Model
{
    public class SureLockAnalyticsModel
    {
        public SureLockAnalyticsModel()
        {
            daysofWeek = new DaysofWeekModel();
        }

        [XmlElement("EnableAnalytics")]
        public bool EnableAnalytics;

        [XmlElement("ScheduleExport")]
        public bool ScheduleExport;

        [XmlElement("ExportAt")]
        public string ExportAt = "10:00 PM";

        [XmlElement("DaysOfTheWeek")]
        public DaysofWeekModel daysofWeek;

        [XmlElement("ClearAnalyticsDataAfterExport")]
        public bool ClearDataAfterExport;
        
    }
}
