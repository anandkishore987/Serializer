﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;
using Utility.Common;

namespace Utility.Model
{
    public class InstalledApplicationModel : ModelBase
    {
        public string AppName       { get; set; }

        public string AppPath       { get; set; }

        public ImageSource AppIcon  { get; set; }

        private bool _selected;
        public bool Selected        
        { 
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                OnPropertyChanged("Selected");
            }
        }
    }
}
