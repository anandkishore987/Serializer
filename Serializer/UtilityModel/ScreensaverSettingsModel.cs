﻿using System;
using System.Xml.Serialization;

namespace Utility.Model
{
    [XmlRoot(IsNullable = true)]
    [Serializable]
    public class ScreensaverSettingsModel
    {
        [XmlElement("EnableScreensaver")]
        public bool EnableScreenSaver;

        [XmlElement("UseSystemWallpaper")]
        public bool UseSystemWallpaper;

        [XmlElement("ScreensaverPath")]
        public string ScreensaverPath = "";

        [XmlElement("ScreensaverTimeout")]
        public int ScreensaverTimeout = 10;

        [XmlElement("PlayRandomMedia")]
        public bool PlayRandomMedia = true;

        [XmlElement("ScreensaverMedia")]
        public ScreensaverMediaModelBase ScreensaverMedia;

        public ScreensaverSettingsModel()
        {
            ScreensaverMedia = new ScreensaverMediaModelBase();
        }
    }
}
