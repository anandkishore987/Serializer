﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Utility.Model
{
    public enum URLType
    {
        [Description("http://")]
        HTTP,

        [Description("https://")]
        HTTPS,

        [Description("file://")]
        FILE
    }
}
