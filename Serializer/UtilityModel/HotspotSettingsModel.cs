﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Utility.Model
{
    [Serializable]
    public class HotspotSettingsModel
    {
        [XmlElement("SSID")]
        public string SSID { get; set; }

        [XmlElement("Password")]
        public string Password { get; set; }

        public HotspotSettingsModel()
        {
            SSID        = string.Empty;
            Password    = string.Empty;
        }

    }
}
