﻿namespace Utility.Model
{
    public class KeyNodeModel
    {
        public string Key          { get; set; }

        public ushort scanCode     { get; set; }
    }
}
