﻿using System;
using System.Xml.Serialization;

namespace Utility.Model
{
    [XmlRoot(IsNullable = true)]
    [Serializable]
    public class TimeoutSettingsModel
    {
        public TimeoutSettingsModel()
        {
            daysofWeek = new DaysofWeekModel();
        }

        [XmlElement("PreventSuspend")]
        public bool PreventSuspend;

        [XmlElement("IdleTimeout")]
        public bool IdleTimeout;

        [XmlElement("CloseOpenApplications")]
        public bool CloseOpenApplications;

        [XmlElement("IdleTimeoutInterval")]
        public int idleTimeoutInterval = 600;

        [XmlElement("SchedulePreventSuspend")]
        public bool SchedulePreventSuspend;

        [XmlElement("PreventSuspendOnACPower")]
        public bool ACPreventSuspend;

        [XmlElement("DaysOfTheWeek")]
        public DaysofWeekModel daysofWeek;

        //0->(Do Nothing), 1->(Sleep), 2->(Hibernate), 3->(Shutdown)
        [XmlElement("LidCloseAction")]
        public int lidCloseAction = 1;

        //0->(Do Nothing), 1->(Sleep), 2->(Hibernate), 3->(Shutdown)
        [XmlElement("PowerButtonAction")]
        public int powerButtonAction = 1;

        //0->(Do Nothing), 1->(Sleep), 2->(Hibernate), 3->(Shutdown)
        [XmlElement("SleepButtonAction")]
        public int sleepButtonAction = 1;

        [XmlElement("StartAt")]
        public string StartAt;

        [XmlElement("EndAt")]
        public string EndAt;
    }
}
