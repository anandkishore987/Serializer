﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility.Model
{
    public class ContextMenuSettingsModel
    {
        public ContextMenuSettingsModel()
        {
            this.Refresh    = true;
            this.Stop       = true;
            this.Back       = true;
            this.Forward    = true;
            this.Home       = true;
        }

        public bool Refresh { get; set; }

        public bool Stop    { get; set; }

        public bool Back    { get; set; }

        public bool Forward { get; set; }

        public bool Home    { get; set; }
    }
}
