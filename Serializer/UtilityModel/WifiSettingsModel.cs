﻿using System;
using System.Xml.Serialization;

namespace Utility.Model
{
    [XmlRoot("WifiSettings", Namespace = "", IsNullable = true)]
    [Serializable]
    public class WifiSettingsModel
    {
        public bool WifiCenter { get; set; }
    }
}
