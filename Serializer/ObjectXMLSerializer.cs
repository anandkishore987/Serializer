using System;
using System.Xml.Serialization;	 // For serialization of an object to an XML Document file.
using System.Runtime.Serialization.Formatters.Binary; // For serialization of an object to an XML Binary file.
using System.IO;				 // For reading/writing data to an XML file.
using System.IO.IsolatedStorage; // For accessing user isolated data.
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using Utility.Common;

namespace Utility
{
    /// <summary>
    /// Serialization format types.
    /// </summary>
    public enum SerializedFormat
    {
        /// <summary>
        /// Binary serialization format.
        /// </summary>
        Binary,

        /// <summary>
        /// String serialization format.
        /// </summary>
        String,

        /// <summary>
        /// Document serialization format.
        /// </summary>
        Document
    }

    /// <summary>
    /// Facade to XML serialization and deserialization of strongly typed objects to/from an XML file.
    /// 
    /// References: XML Serialization at http://samples.gotdotnet.com/:
    /// http://samples.gotdotnet.com/QuickStart/howto/default.aspx?url=/quickstart/howto/doc/xmlserialization/rwobjfromxml.aspx
    /// </summary>
    public static class ObjectXMLSerializer<T> where T : class // Specify that T must be a class.
    {
        #region Load methods

        /// <summary>
        /// Loads an object from an XML file in Document format.
        /// </summary>
        /// <example>
        /// <code>
        /// serializableObject = ObjectXMLSerializer&lt;SerializableObject&gt;.Load(@"C:\XMLObjects.xml");
        /// </code>
        /// </example>
        /// <param name="path">Path of the file to load the object from.</param>
        /// <returns>Object loaded from an XML file in Document format.</returns>
        public static T Load(string path)
        {
            T serializableObject = LoadFromDocumentFormat(null, path, null);
            return serializableObject;
        }

        #endregion

        #region Save methods

        /// <summary>
        /// Saves an object to an XML file using a specified serialized format.
        /// </summary>
        /// <example>
        /// <code>
        /// SerializableObject serializableObject = new SerializableObject();
        /// 
        /// ObjectXMLSerializer&lt;SerializableObject&gt;.Save(serializableObject, @"C:\XMLObjects.xml", SerializedFormat.Binary);
        /// </code>
        /// </example>
        /// <param name="serializableObject">Serializable object to be saved to file.</param>
        /// <param name="path">Path of the file to save the object to.</param>
        /// <param name="serializedFormat">XML serialized format used to save the object.</param>
        public static string Save(T serializableObject, string path, SerializedFormat serializedFormat,bool exportActivationCode = false)
        {
            switch (serializedFormat)
            {
                case SerializedFormat.Binary:
                    SaveToBinaryFormat(serializableObject, path, null);
                    break;

                case SerializedFormat.Document:
                    SaveToDocumentFormat(serializableObject, null, path, null);
                    break;
                case SerializedFormat.String:
                default:
                    return SaveToStringFormat(serializableObject, null, path, null,exportActivationCode);

            }
            return null;
        }

        #endregion

        #region Private

        private static FileStream CreateFileStream(IsolatedStorageFile isolatedStorageFolder, string path)
        {
            FileStream fileStream = null;

            if (isolatedStorageFolder == null)
                fileStream = new FileStream(path, FileMode.OpenOrCreate);
            else
                fileStream = new IsolatedStorageFileStream(path, FileMode.OpenOrCreate, isolatedStorageFolder);

            return fileStream;
        }

        private static T LoadFromBinaryFormat(string path, IsolatedStorageFile isolatedStorageFolder)
        {
            T serializableObject = null;

            using (FileStream fileStream = CreateFileStream(isolatedStorageFolder, path))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                serializableObject = binaryFormatter.Deserialize(fileStream) as T;
            }

            return serializableObject;
        }

        private static T LoadFromDocumentFormat(System.Type[] extraTypes, string path, IsolatedStorageFile isolatedStorageFolder)
        {
            T serializableObject = null;

            using (TextReader textReader = CreateTextReader(isolatedStorageFolder, path))
            {
                XmlSerializer xmlSerializer = CreateXmlSerializer(extraTypes);
                string contents             = textReader.ReadToEnd();
                contents                    = contents.Replace(ApplicationConstants.HexadecimalNul, string.Empty);
                contents                    = BackwardCompatibilitySettings(contents);
                serializableObject          = xmlSerializer.Deserialize(GenerateStreamFromString(contents)) as T;
            }
            return serializableObject;
        }

        public static string BackwardCompatibilitySettings(string settings)
        {
            try
            {
                XElement tempElement;

                //Load the settings file
                XDocument doc = XDocument.Parse(settings);

                //Change RootModel -> SureLock
                if (doc.Elements("RootModel").Count() != 0)
                {
                    tempElement = doc.Elements("RootModel").Single();
                    tempElement.Name = "SureLock";
                }

                //Change allowedAppsModel -> AllowedApplications
                if (doc.Root.Elements("allowedAppsModel").Count() != 0)
                {
                    tempElement = doc.Root.Elements("allowedAppsModel").Single();
                    tempElement.Name = "AllowedApplications";
                }

                //Change ssModel -> SureLockSettings
                if (doc.Root.Elements("ssModel").Count() != 0)
                {
                    tempElement = doc.Root.Elements("ssModel").Single();
                    tempElement.Name = "SureLockSettings";
                }

                //Return the new XML
                return doc.ToString();
            }
            catch (Exception exp)
            {
                return settings;
            }
        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position     = 0;
            return stream;
        }

        private static T LoadFromStringFormat(System.Type[] extraTypes, string data, IsolatedStorageFile isolatedStorageFolder)
        {
            T serializableObject = null;
            using (TextReader textReader = new StringReader(data))
            {
                XmlSerializer xmlSerializer = CreateXmlSerializer(extraTypes);
                serializableObject = xmlSerializer.Deserialize(textReader) as T;
            }

            return serializableObject;
        }

        private static TextReader CreateTextReader(IsolatedStorageFile isolatedStorageFolder, string path)
        {
            TextReader textReader = null;

            if (isolatedStorageFolder == null)
                textReader = new StreamReader(path);
            else
                textReader = new StreamReader(new IsolatedStorageFileStream(path, FileMode.Open, isolatedStorageFolder));

            return textReader;
        }

        private static TextWriter CreateTextWriter(IsolatedStorageFile isolatedStorageFolder, string path)
        {
            TextWriter textWriter = null;

            if (isolatedStorageFolder == null)
                textWriter = new StreamWriter(path);
            else
                textWriter = new StreamWriter(new IsolatedStorageFileStream(path, FileMode.OpenOrCreate, isolatedStorageFolder));

            return textWriter;
        }

        private static XmlSerializer CreateXmlSerializer(System.Type[] extraTypes)
        {
            Type ObjectType = typeof(T);

            XmlSerializer xmlSerializer = null;

            if (extraTypes != null)
                xmlSerializer = new XmlSerializer(ObjectType, extraTypes);
            else
                xmlSerializer = new XmlSerializer(ObjectType);

            return xmlSerializer;
        }

        private static void SaveToDocumentFormat(T serializableObject, System.Type[] extraTypes, string path, IsolatedStorageFile isolatedStorageFolder)
        {
            using (TextWriter textWriter = CreateTextWriter(isolatedStorageFolder, path))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer xmlSerializer = CreateXmlSerializer(extraTypes);
                    xmlSerializer.Serialize(new XmlWriterFull(xmlWriter), serializableObject, ns);
                }
            }
        }

        private static string SaveToStringFormat(T serializableObject, System.Type[] extraTypes, string path, IsolatedStorageFile isolatedStorageFolder,bool exportActivationCode)
        {
            StringBuilder outputSB = new StringBuilder();
            using (TextWriter textWriter = new StringWriter(outputSB))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, new XmlWriterSettings 
                                             { 
                                                  OmitXmlDeclaration = true,
                                                  Indent = true,
                                                  IndentChars = "\t"
                                             }))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer xmlSerializer = CreateXmlSerializer(extraTypes);
                    xmlSerializer.Serialize(new XmlWriterFull(xmlWriter), serializableObject, ns);
                }
            }

            string xml              = outputSB.ToString();

            if (exportActivationCode && !string.IsNullOrWhiteSpace(xml))
            {
                var baseModel       = serializableObject as dynamic;
                if(baseModel != null && !string.IsNullOrWhiteSpace(baseModel.ActivationCode))
                {
                    var xDoc            = XDocument.Parse(xml);
                    var rootElement     = xDoc.Root;
                    rootElement.SetAttributeValue(ApplicationConstants.ACTCODE_ATTRIBUTE, MD5.EncryptString(baseModel.ActivationCode, ApplicationConstants.ACTCODE_PASSPHRASE));
                    xml                 = xDoc.ToString();
                }
            }
            return xml;
        }

        private static void SaveToBinaryFormat(T serializableObject, string path, IsolatedStorageFile isolatedStorageFolder)
        {
            using (FileStream fileStream = CreateFileStream(isolatedStorageFolder, path))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, serializableObject);
            }
        }

        #endregion
    }
}