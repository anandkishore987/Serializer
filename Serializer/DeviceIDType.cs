﻿namespace Utility.Enum
{
    public enum DeviceIDType
    {
        IMEI        = 2,
        IMSI        = 3,
        WIFI_MAC    = 5,
        BT_MAC      = 7,
        UUID        = 11,
        GUID        = 13
    }
}
