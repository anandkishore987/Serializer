﻿namespace Utility.Enum
{
    public enum WallpaperSize
    {
        None = 0,
        Uniform,
        UniformToFill,
        Fill
    }
}
