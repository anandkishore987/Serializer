﻿using System;
using System.Reflection;
using System.Xml.Serialization;
using Utility.Common;
using Utility.Enum;

namespace Utility.Model
{
    [XmlRoot("SureLockSettings", Namespace = "", IsNullable = true)]
    [Serializable]
    public class SureLockSettingsModel
    {
        public SureLockSettingsModel()
        {
            TimeoutSettingsModel        = new TimeoutSettingsModel();
            AnalyticsModel              = new SureLockAnalyticsModel();
            WifiSettingsModel           = new WifiSettingsModel();
            ScreensaverSettingsModel    = new ScreensaverSettingsModel();
            wallSize                    = WallpaperSize.None;
            hotspotSettingsModel        = new HotspotSettingsModel();
        }

        [XmlElement("Wallpaper")]
        public string Wallpaper         = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Images\\surelock_logo_trial1(Win8).png";

        [XmlElement("DiagnosticLog")]
        public bool EnableDiagnosticLog = true;

        [XmlElement("DoubleClick")]
        public bool EnableDoubleClick;

        [XmlElement("ExportFileName")]
        public string ExportFileName    = "";

        [XmlElement("ImportFileName")]
        public string ImportFileName    = "";

        public WallpaperSize wallSize { get; set; }

        [XmlElement("IconSize")]
        public int IconSize             = 50;

        [XmlElement("IconRelocation")]
        public bool IconRelocation      = false;

        [XmlElement("FontSize")]
        public double FontSize          = 12;

        [XmlElement("FontFamily")]
        public string FontFamily        = "Tahoma";

        public string FontColor         = "Black";

        [XmlElement("MouseScheme")]
        public string MouseScheme       = "Windows Default";

        [XmlElement("SureLockTaskbar")]
        public bool EnableTaskbar;

        [XmlElement("TouchKeyboard")]
        public TouchKeyboardType TouchKeyboard;

        public string TaskbarColor      = ApplicationConstants.TaskbarBackgroundColor;

        [XmlElement("UseSmallIcons")]
        public bool UseSmallIcons;

        [XmlElement("UseWideIcons")]
        public bool UseWideIcons;

        [XmlElement("AutoHide")]
        public bool AutoHide;

        [XmlElement("Reboot")]
        public bool Reboot;

        [XmlElement("Notifications")]
        public bool Notifications;

        [XmlElement("Shutdown")]
        public bool Shutdown;

        [XmlElement("Logoff")]
        public bool Logoff;

        [XmlElement("WatchDogService")]
        public bool WatchDogService;

        [XmlElement("RequirePasswordonWakeUp")]
        public bool RequirePasswordonWakeUp = true;

        [XmlElement("DisableVolume")]
        public bool DisableVolume           =   true;

        [XmlElement("TimeoutSettings")]
        public TimeoutSettingsModel TimeoutSettingsModel;

        [XmlElement("WifiSettings")]
        public WifiSettingsModel WifiSettingsModel;

        [XmlElement("HotspotSettings")]
        public HotspotSettingsModel hotspotSettingsModel;

        [XmlElement("Password")]
        public string Password              = MD5.EncryptString(ApplicationConstants.DefaultPassword, ApplicationConstants.PASSPHRASE);

        [XmlElement("NumberOfTaps")]
        public int NumberOfTaps             = 5;

        [XmlElement("ProductVersion")]
        public string ProductVersion        = "2.25";

        [XmlElement("SureLockAnalyticsSettings")]
        public SureLockAnalyticsModel AnalyticsModel;

        [XmlElement("ScreensaverSettings")]
        public ScreensaverSettingsModel ScreensaverSettingsModel;
    }
}
