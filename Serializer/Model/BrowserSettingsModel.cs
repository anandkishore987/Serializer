﻿using System;
using System.Xml.Serialization;
using Utility.Common;

namespace Utility.Model
{
    [XmlRoot("BrowserSettings", Namespace = "", IsNullable = true)]
    [Serializable]
    public class BrowserSettingsModel
    {
        public BrowserSettingsModel()
        {
            DisplaySettingsModel    = new DisplaySettingsModel();
            BrowserAgent            = ApplicationConstants.IE;
        }

        [XmlElement("DisplaySettings")]
        public DisplaySettingsModel DisplaySettingsModel { get; set; }

        public bool UseLegacyJS                          { get; set; }

        public bool SavePassword                         { get; set; }

        public string BrowserAgent                       { get; set; }
    }
}
