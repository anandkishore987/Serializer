﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Utility;
using System.ComponentModel;

namespace SureLockWin8.Model
{
      [Serializable]

   public class MiscellaneousSettingsModel
    {

        [XmlElement("NumberOfTaps")]
         public int NumberOfTaps;

        [XmlElement("SettingsTimeout")]
        public long SettingsTimeout;
    }
}
