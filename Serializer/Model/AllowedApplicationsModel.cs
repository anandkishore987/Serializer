﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SureLockWin8.Model
{
   [Serializable]
   public class AllowedApplicationsModel
   {
        //[XmlAttribute("Application")]
        public ObservableCollection<ApplicationSettingsModel> allowedapps;

        public AllowedApplicationsModel()
        {
            allowedapps = new ObservableCollection<ApplicationSettingsModel>();
        }
   }
}
