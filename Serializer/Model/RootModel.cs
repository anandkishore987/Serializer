﻿using System;
using Microsoft.Win32;
using System.Xml.Serialization;
using System.Reflection;
using System.IO;
using System.Linq;
using Utility.Model;
using Utility.Common;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using Utility.Enum;
using Utility.License;

namespace SureLockWin8.Model
{
    [Serializable]
    [XmlRoot("SureLock")]
    public class RootModel
    {
        public event EventHandler<EventArgs> NeedRefresh;

        public event EventHandler<EventArgs> RecheckLicense;

        [XmlAttribute("Platform")]
        public string Platform  = "WINDOWS";

        [XmlAttribute("OS_NAME")]
        public string OsName="";

        [XmlAttribute("OS_ARCHITECTURE")]
        public string OsArchitecture;

        [XmlAttribute("PROCESSOR_ARCHITECTURE")]
        public string ProcessorArchitecture;

        [XmlElement("AllowedApplications")]
        public AllowedApplicationsModel      allowedAppsModel;

        [XmlElement("AllowedWebsites")]
        public AllowedWebsitesModel         allowedWebsitesModel;

        [XmlElement("SureLockSettings")]
        public SureLockSettingsModel        ssModel;

        [XmlElement("BrowserSettings")]
        public BrowserSettingsModel         browserSettings;

        [XmlElement("SingleApplicationModeSettings")]
        public SingleApplicationModeModel singleAppModel;
        
        [XmlElement("KeyboardFilteringSettings")]
        public KeyboardFiltering            keyboardFiltering;

        [XmlElement("MouseFilteringSettings")]
        public MouseFiltering               mouseFiltering;

        [XmlIgnore]
        public bool                          importSuccess;

        [XmlIgnore]
        public string                        ActivationCode;

        public bool                          enableAutoImport;

        public bool                          ExportActivationCode;

        /// <summary>
        /// 0->File
        /// 1->Cloud
        /// </summary>
        public int                           AutoImportSource;

        /// <summary>
        /// 0->None
        /// 1->Wifi Mac
        /// 2->Bluetooh Mac
        /// 3->GUID
        /// </summary>
        [XmlIgnore]
        public int                           PreferredActivationID  =   0;

        public string                        AutoImportFileName;

        public string                        AutoImportCloudId;

        public int                           AutoImportInterval;
        
        [XmlIgnore]
        public string MacWifi                   { get; set; }

        [XmlIgnore]
        public List<string> MacWifiList         { get; set; }

        [XmlIgnore]
        public List<string> MacBluetoothList    { get; set; }

        [XmlIgnore]
        public string MacBluetooth              { get; set; }

        [XmlIgnore]
        public string GUID                      { get; set; }

        [XmlIgnore]
        public bool isLicensed                  { get; set; }

        [XmlIgnore]
        public string LicKey                    { get; set; }

        [XmlIgnore]
        public string RenewKey                  { get; set; }

        [XmlIgnore]
        public string LicTill                   { get; set; }

        [XmlIgnore]
        public string licensedTo                { get; set; }

        [XmlIgnore]
        public KeyInfo LicKeyInfo               { get; set; }

        //Method to get or create GUID
        private void GetGUID_PrefferedID()
        {
             RegistryKey Key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\42gears\\SureLock");
             if (Key != null)
             {
                 //Gets the stored GUID.
                 object guid    = Key.GetValue("GUID");
                 object actId   = Key.GetValue("PreferredActivationID");
                 Key.Close();

                 //If GUID string is blank or does not exit
                 if (guid == null || guid.ToString() == "")
                 {
                     RegistryKey SureLockKey = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock", true);

                     //Generate GUID once and store in registry
                     GUID = GenerateGUID();
                     SureLockKey.SetValue("GUID", GUID);
                     SureLockKey.Close();
                 }
                 else
                 {
                     //Retrieve GUID from registry
                     GUID = guid.ToString();
                 }

                 //If preferred id is empty
                 if (actId == null || actId.ToString() == "")
                 {

                     //Set it to 0->None
                     RegistryKey SureLockKey = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock", true);
                     SureLockKey.SetValue("PreferredActivationID", 0);
                     SureLockKey.Close();
                 }
                 else
                 {
                     try
                     {
                         PreferredActivationID  =   Int32.Parse(actId.ToString());
                     }
                     catch (Exception exp)
                     {
                     }
                 }
             }
        }

        //Method to generate GUID
        private string GenerateGUID()
        {
            Guid DeviceGUID = Guid.NewGuid();
            return DeviceGUID.ToString();
        }

        public void GetOSInfo()
        {
            try
            {

                ProcessorArchitecture = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
                if (Environment.Is64BitOperatingSystem)
                {
                    OsArchitecture = "64-bit";
                }
                else
                {
                    OsArchitecture = "32-bit";
                }

                OsName             = Environment.OSVersion.ToString();                
            }
            catch (Exception exp)
            {
                OsName = "";
            }
        }

        public static void SavePreferredID(int id)
        {
            RegistryKey SureLockKey = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock", true);
            if (SureLockKey != null)
            {
                SureLockKey.SetValue("PreferredActivationID", id);
                SureLockKey.Close();
            }
        }

        private void GetDeviceInformation()
        {

            MacWifi             = "Not Available";
            MacBluetooth        = "Not Available";

            var adapters        = NetworkInterface.GetAllNetworkInterfaces().Where(x => x.NetworkInterfaceType != NetworkInterfaceType.Loopback && x.NetworkInterfaceType != NetworkInterfaceType.Tunnel);
            var wifiAdapters    = adapters.Where(x => x.NetworkInterfaceType == NetworkInterfaceType.Wireless80211).ToList();
            var btAdapters      = adapters.Except(wifiAdapters).ToList();

            MacWifiList         = GetMacAddress(wifiAdapters);
            MacBluetoothList    = GetMacAddress(btAdapters);

            if(MacWifiList.Count != 0)
            {
                MacWifi         = MacWifiList.First();
            }

            if (MacBluetoothList.Count != 0)
            {
                MacBluetooth    = MacBluetoothList.First();
            }

            GetGUID_PrefferedID();

        }


        public static List<string> GetMacAddress(List<NetworkInterface> interfaces)
        {
            List<string> macAddresses = new List<string>();

            interfaces.ForEach(x =>
            {
                try
                {
                    var macAddress = string.Join("-", x.GetPhysicalAddress().GetAddressBytes().Select(b => b.ToString("X2")));
                    macAddresses.Add(macAddress);
                }
                catch (Exception)
                {
                }
            });

            return macAddresses;
        }

        /// <summary>
        /// Method to set licensed wallpaper if device is licensed only if trial wallpaper is set OR revert to trial version if not licensed
        /// </summary>
        public void CheckWallpaper()
        {
            string wallpaperPath        = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (this.isLicensed)
            {
                if(this.ssModel != null && (string.IsNullOrWhiteSpace(this.ssModel.Wallpaper) || this.ssModel.Wallpaper.EndsWith("surelock_logo_trial1(Win8).png")))
                {
                    ssModel.Wallpaper   = wallpaperPath + "\\Images\\surelock_logo(Win8).png";
                }
            }
            else
            {
                ssModel.Wallpaper       = wallpaperPath + "\\Images\\surelock_logo_trial1(Win8).png";
                ssModel.wallSize        = WallpaperSize.None;
            }
        }

        public RootModel()
        {

            GetOSInfo();
            GetDeviceInformation();
            isDeviceLicensed();
            IsDeviceLicensedOffline();

            allowedAppsModel                = new AllowedApplicationsModel();
            allowedWebsitesModel            = new AllowedWebsitesModel();
            ssModel                         = new SureLockSettingsModel();
            browserSettings                 = new BrowserSettingsModel();
            keyboardFiltering               = new KeyboardFiltering();
            mouseFiltering                  = new MouseFiltering();
            singleAppModel                  = new SingleApplicationModeModel();
            importSuccess                   = false;
            ExportActivationCode            = true;

            if (AutoImportFileName == null)
            {
                AutoImportFileName          = @"C:\Users\SureLock.settings";
            }

        }

        public RootModel(bool bEnable)
        {
            GetOSInfo();
            GetDeviceInformation();
            isDeviceLicensed();
            IsDeviceLicensedOffline();

            allowedAppsModel                                = new AllowedApplicationsModel();
            allowedWebsitesModel                            = new AllowedWebsitesModel();
            ssModel                                         = new SureLockSettingsModel();
            browserSettings                                 = new BrowserSettingsModel();
            ssModel.TimeoutSettingsModel.StartAt            = "08:00 AM";
            ssModel.TimeoutSettingsModel.EndAt              = "10:00 PM";
            ssModel.TimeoutSettingsModel.powerButtonAction  = 1;
            ssModel.TimeoutSettingsModel.sleepButtonAction  = 1;
            ssModel.TimeoutSettingsModel.lidCloseAction     = 1;

            SetDaysofWeek(ssModel);
            keyboardFiltering                               = new KeyboardFiltering();
            mouseFiltering                                  = new MouseFiltering();
            singleAppModel                                  = new SingleApplicationModeModel();
            importSuccess                                   = false;
            ExportActivationCode                            = true;

            if (AutoImportFileName == null)
            {
                AutoImportFileName                          = @"C:\Users\SureLock.settings";
            }

            CheckWallpaper();
            ssModel.EnableTaskbar                           = bEnable;
            AutoImportCloudId                               = "";
            AutoImportSource                                = 0;
            
            //Default is 10 minutes
            AutoImportInterval                              = 10;     
        }

        public void SetDaysofWeek(SureLockSettingsModel settingsModel)
        {
            settingsModel.TimeoutSettingsModel.daysofWeek.Sunday    = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Monday    = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Tuesday   = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Wednesday = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Thursday  = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Friday    = true;
            settingsModel.TimeoutSettingsModel.daysofWeek.Saturday  = true;
        }

        private void IsDeviceLicensedOffline()
        {
            //First preference for Online activation
            //Check only if device is not licensed
            if(!isLicensed)
            {
                if(File.Exists(ApplicationConstants.OfflineLicenseFilePath))
                {
                    string licFile              = File.ReadAllText(ApplicationConstants.OfflineLicenseFilePath);

                    if(!string.IsNullOrWhiteSpace(licFile))
                    {
                        try
                        {
                            var SureLockKey     = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock",true);
                            if(SureLockKey != null)
                            {
                                LicKey              = licFile;

                                ValidateLicense(licenseValid =>
                                {
                                    if (licenseValid)
                                    {
                                        SureLockKey.SetValue("Offline", 1, RegistryValueKind.DWord);
                                        SureLockKey.SetValue("LicKey", LicKey);
                                    }
                                });

                                SureLockKey.Close();
                            }
                        }
                        catch (Exception exp)
                        {
                        }
                    }
                }
            }
        }

        private void ValidateLicense(Action<bool> action)
        {
            //Mac Wi-Fi
            for (int i = 0 ; i < MacWifiList.Count ; i++)
            {
                var macWifi     = MacWifiList[i];

                if(IsLicenseValid(macWifi, DeviceIDType.WIFI_MAC))
                {
                    MacWifi     = macWifi;
                    break;
                }
            }

            //Mac Bluetooth
            if (!isLicensed)
            {
                for (int i = 0 ; i < MacBluetoothList.Count ; i++)
                {
                    var macBT           = MacBluetoothList[i];

                    if(IsLicenseValid(macBT, DeviceIDType.BT_MAC))
                    {
                        MacBluetooth    = macBT;
                        break;
                    }
                }
            }

            //GUID
            if (!isLicensed)
            {
                IsLicenseValid(GUID, DeviceIDType.GUID);
            }

            action.Invoke(isLicensed);
        }

        private bool IsLicenseValid(string deviceID, DeviceIDType deviceIDType)
        {
            //string macWifi                      = string.Empty;
            //string macBluetooth                 = string.Empty;
            //string guid                         = string.Empty;

            //switch (deviceIDType)
            //{
            //    case DeviceIDType.WIFI_MAC:
            //        macWifi                     = deviceID;
            //        break;

            //    case DeviceIDType.BT_MAC:
            //        macBluetooth                = deviceID;
            //        break;

            //    case DeviceIDType.GUID:
            //        guid                        = deviceID;
            //        break;
            //}

            //KeyInfo keyInfo                     = null;
            //bool isKeyExpired                   = false;
            //bool isValid                        = Verifier.IsLicenseValid(LicKey, RenewKey, macWifi, macBluetooth, guid, out isKeyExpired, out keyInfo);

            ////Grace period check
            //if (isKeyExpired)
            //{
            //    var surelockInfo                = SureLockInfo.GetInfo();

            //    //If SureLock in grace period, check license validity
            //    if(surelockInfo.IsGracePeriod)
            //    {
            //        isValid                     = Verifier.IsLicenseValidInGracePeriod(keyInfo);
            //    }
            //    else
            //    {
            //        isValid                     = true;

            //        //Check activation server
            //        //If activation server running, mark grace period on and save.
            //        //Recheck license
            //        Utility.Utils.ActivationServerRunning(serverRunning =>
            //        {
            //            //Mark SureLock in grace period if we are able to ping 
            //            //Activation server atleast once during one activated session
            //            if (serverRunning)
            //            {
            //                surelockInfo                = SureLockInfo.GetInfo();
            //                surelockInfo.IsGracePeriod  = true;
            //                SureLockInfo.Save(surelockInfo);
            //                RecheckLicense?.Invoke(this, null);
            //            }
            //        });
            //    }
            //}

            //LicKeyInfo                  = keyInfo;

            //if (isValid)
            //{
            //    isLicensed              = true;
            //    licensedTo              = LicKeyInfo.licensedTo;
            //    SetExpiryDate(LicKeyInfo.expiryDate);
            //}
            //else
            //{
            //    isLicensed              = false;
            //}

            //return isValid;

            return true;
        }

        public void isDeviceLicensed()
        {
            try
            {
                RegistryKey SureLockKey = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock");
                object licenseKey       = SureLockKey.GetValue("LicKey");
                object renewKey         = SureLockKey.GetValue("RenewKey");
                string activationCode   = SureLockKey.GetValue("ActivationCode").ToString();

                if(renewKey != null)
                {
                    RenewKey            = renewKey.ToString();
                }

                if (licenseKey != null && licenseKey.ToString() != "")
                {
                    LicKey              = licenseKey.ToString();

                    ValidateLicense(licenseValid =>
                    {
                        if(licenseValid)
                        {
                            try
                            {
                                if (!string.IsNullOrWhiteSpace(activationCode))
                                {
                                    activationCode = MD5.DecryptString(activationCode, ApplicationConstants.PASSPHRASE);
                                    ActivationCode = activationCode;
                                }
                            }
                            catch (Exception exp)
                            {
                            }
                        }
                    });
                }
                SureLockKey.Close();
            }
            catch (Exception exp)
            {
                var SureLockKey = Registry.LocalMachine.CreateSubKey(@"Software\42Gears\SureLock");
                SureLockKey.SetValue("ActivationCode", "");
                SureLockKey.SetValue("LicKey","");
                SureLockKey.Close();
            }
        }

        /// <summary>
        /// Method to Set the date in About page
        /// </summary>
        /// <param name="shortDate"></param>
        public void SetExpiryDate(ShortDate shortDate)
        {
            string expiryDate = "";

            //Set the month
            switch (shortDate.Month)
            {
                case 1:
                    expiryDate += "Jan ";
                    break;

                case 2:
                    expiryDate += "Feb ";
                    break;

                case 3:
                    expiryDate += "Mar ";
                    break;

                case 4:
                    expiryDate += "Apr ";
                    break;

                case 5:
                    expiryDate += "May ";
                    break;

                case 6:
                    expiryDate += "Jun ";
                    break;

                case 7:
                    expiryDate += "Jul ";
                    break;

                case 8:
                    expiryDate += "Aug ";
                    break;

                case 9:
                    expiryDate += "Sep ";
                    break;

                case 10:
                    expiryDate += "Oct ";
                    break;

                case 11:
                    expiryDate += "Nov ";
                    break;

                case 12:
                    expiryDate += "Dec ";
                    break;
            }

            expiryDate += shortDate.Day + ",";
            expiryDate += (2000 + shortDate.Year);
            LicTill     = expiryDate;
        }

        public void Refresh()
        {
            NeedRefresh?.Invoke(this, null);
        }
    }
}
