﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Utility.Model;

namespace SureLockWin8.Model
{
    [Serializable]
    public class AllowedWebsitesModel
    {
        public ObservableCollection<WebsiteSettingsModel> allowedWebsites;

        public AllowedWebsitesModel()
        {
            allowedWebsites = new ObservableCollection<WebsiteSettingsModel>();
        }
    }
}
