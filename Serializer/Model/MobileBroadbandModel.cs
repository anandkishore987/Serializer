﻿using SureLockWin8.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SureLockWin8.Model
{
    public class MobileBroadbandModel : ViewModelBase
    {
        public MobileBroadbandModel(string _SSID, bool _isConnected , bool _isLocked = false)
        {
            SSID = _SSID;
            isConnected = _isConnected;
            isLocked = _isLocked;
        }

        public string SSID { get; set; }
		
        public bool isLocked { get; set; }


        bool _isConnected;
        public bool isConnected
        {
            get
            {
                return _isConnected;
            }
            set
            {
                _isConnected = value;
                OnPropertyChanged("Status");
                OnPropertyChanged("isConnected");
            }
        }

        public string ButtonContent
        {
            get
            {
                if (isLocked)
                {
                    return "Unlock";
                }
                else if (isConnected)
                {
                    return "Disconnect";
                }
                else
                {
                    return "Connect";
                }
            }
        }
		
        public string Status
        {
            get
            {
                if (isConnected)
                {
                    return "Connected";
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
