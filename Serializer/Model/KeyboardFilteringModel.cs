﻿using System;
using System.Windows.Input;
using System.Xml.Serialization;
using Utility.Common;
using Utility.Enum;
using Utility.Model;

namespace SureLockWin8.Model
{
    [XmlRootAttribute("Keyboard_Combination", Namespace = "", IsNullable = true)]
    [Serializable]
    public class KeyboardFilteringModel : ModelBase
    {
        public KeyboardType Type;

        public string Key;

        [XmlElement("ScanCode")]
        public ushort scanCode;

        [XmlElement("CustomCode")]
        public bool isCustomCode;

        public bool Ctrl;

        public bool Alt;

        public bool Shift;

        public bool Win;

        public int index;

        private bool KeySelected;

        [XmlIgnore]
        public string KeyCodeProp
        {
            get { return Key; }
            set
            {
                Key = value;
            }
        }

        [XmlIgnore]
        public bool CtrlProp
        {
            get { return Ctrl; }
            set
            {
                Ctrl = value;
            }
        }

        [XmlIgnore]
        public bool AltProp
        {
            get { return Alt; }
            set
            {
                Alt = value;
            }
        }

        [XmlIgnore]
        public bool ShiftProp
        {
            get { return Shift; }
            set
            {
                Shift = value;
            }
        }

        [XmlIgnore]
        public bool WinProp
        {
            get { return Win; }
            set
            {
                Win = value;
            }
        }

        [XmlIgnore]
        public int SelectedType
        {
            get
            {
                return (int)Type;
            }
            set
            {
                Type = (KeyboardType)value;
            }
        }

        bool _driverInstalled;

        [XmlIgnore]
        public bool DriverInstalled
        {
            get
            {
                return _driverInstalled;
            }
            set
            {
                _driverInstalled = value;
                OnPropertyChanged("DriverInstalled");
            }
        }

        public event EventHandler Click;

        //RelayCommand _click;
        //public ICommand ClickCommand
        //{
        //    get
        //    {
        //        if (_click == null)
        //            _click = new RelayCommand(param => OnClick());
        //        return _click;
        //    }
        //}

        private void OnClick()
        {
            if (Click != null)
            {
                Click(this, null);
            }
        }

        [XmlIgnore]
        public bool KeySelectedProp
        {
            get { return KeySelected; }
            set
            {
                KeySelected = value;
                OnPropertyChanged("KeySelectedProp");
                OnPropertyChanged("Background");
            }
        }

        [XmlIgnore]
        public string Background
        {
            get
            {
                if (KeySelectedProp)
                {
                    return ApplicationConstants.AliceBlue;
                }
                else
                {
                    return ApplicationConstants.White;
                }
            }
        }

        public KeyboardFilteringModel()
        {
            _driverInstalled = true;
            InitKeyboardType();
        }

        private void InitKeyboardType()
        {
            Type            = DriverInstalled ? KeyboardType.Physical : KeyboardType.Virtual;
        }
    }
}
