﻿namespace Utility.License
{
    public class RenewInfo
    {
        public bool isVerified;
        public bool isValidKey;
        public int errorCode;
        public ShortDate expiryDate;
    }
}
