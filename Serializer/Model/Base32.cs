﻿using System;

namespace Utility.License
{
    public class Base32
    {
        private static readonly byte[] decodeTable = {
            (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255,
            (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255,
            (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)63,
            (byte)0,  (byte)1,  (byte)2,  (byte)3,  (byte)4,  (byte)5,  (byte)6,  (byte)7,  (byte)8,  (byte)9,  (byte)255, (byte)255, (byte)255, (byte)255, (byte)255, (byte)255,
            (byte)255, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };

        private static readonly byte[] encodeTable = {
            (byte)'0', (byte)'1', (byte)'2', (byte)'3', (byte)'4', (byte)'5', (byte)'6', (byte)'7', (byte)'8', (byte)'9', 
            (byte)'A', (byte)'B', (byte)'C', (byte)'D', (byte)'E', (byte)'F', (byte)'G', (byte)'H', (byte)'I', (byte)'J',
            (byte)'K', (byte)'L', (byte)'M', (byte)'N', (byte)'O', (byte)'P', (byte)'Q', (byte)'R', (byte)'S', (byte)'T',
            (byte)'U', (byte)'V' };
        
        private static readonly int MASK_5BITS                      = 0x1f;
        private readonly byte PAD                                   = (byte)'=';
        private readonly int encodeSize                             = 8;
        private readonly int decodeSize                             = 7;
        private readonly int lineLength                             = 0;
        private readonly byte[] lineSeparator                       = null;
        private static readonly int BITS_PER_ENCODED_BYTE           = 5;
        private static readonly int BYTES_PER_ENCODED_BLOCK         = 8;
        private static readonly int BYTES_PER_UNENCODED_BLOCK       = 5;
        private static readonly int DEFAULT_BUFFER_RESIZE_FACTOR    = 2;
        private static readonly int DEFAULT_BUFFER_SIZE             = 8192;
        private static readonly int MASK_8BITS                      = 0xff;
        private long bitWorkArea;
        private bool eof;
        private int modulus;
        private byte[] buffer;
        private int pos;
        private int readPos;
        private int currentLinePos;

        private void reset()
        {
            buffer = null;
            pos = 0;
            readPos = 0;
            currentLinePos = 0;
            modulus = 0;
            eof = false;
        }

        public byte[] encode(byte[] pArray)
        {
            reset();
            if (pArray == null || pArray.Length == 0)
            {
                return pArray;
            }
            encode(pArray, 0, pArray.Length);
            encode(pArray, 0, -1);
            byte[] buf = new byte[pos - readPos];
            readResults(buf, 0, buf.Length);
            return buf;
        }

        public byte[] decode(byte[] pArray)
        {
            reset();
            if (pArray == null || pArray.Length == 0)
            {
                return pArray;
            }
            decode(pArray, 0, pArray.Length);
            decode(pArray, 0, -1);
            byte[] result = new byte[pos];
            readResults(result, 0, result.Length);
            return result;
        }

        private int available()
        {
            return buffer != null ? pos - readPos : 0;
        }

        private int readResults(byte[] b, int bPos, int bAvail)
        {
            if (buffer != null)
            {
                int len = Math.Min(available(), bAvail);
                Array.Copy(buffer, readPos, b, bPos, len);
                readPos += len;
                if (readPos >= pos)
                {
                    buffer = null;
                }
                return len;
            }
            return eof ? -1 : 0;
        }

        private void encode(byte[] input, int inPos, int inAvail)
        {
            if (eof)
            {
                return;
            }
            if (inAvail < 0)
            {
                eof = true;
                if (0 == modulus && lineLength == 0)
                {
                    return;
                }
                ensureBufferSize(encodeSize);
                int savedPos = pos;
                switch (modulus)
                {
                    case 1:
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 3) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea << 2) & MASK_5BITS];
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        break;

                    case 2:
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 11) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 6) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 1) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea << 4) & MASK_5BITS];
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        break;
                    case 3:
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 19) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 14) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 9) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 4) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea << 1) & MASK_5BITS];
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        buffer[pos++] = PAD;
                        break;
                    case 4:
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 27) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 22) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 17) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 12) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 7) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 2) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea << 3) & MASK_5BITS];
                        buffer[pos++] = PAD;
                        break;
                }
                currentLinePos += pos - savedPos;
                if (lineLength > 0 && currentLinePos > 0)
                {
                    Array.Copy(lineSeparator, 0, buffer, pos, lineSeparator.Length);
                    pos += lineSeparator.Length;
                }
            }
            else
            {
                for (int i = 0; i < inAvail; i++)
                {
                    ensureBufferSize(encodeSize);
                    modulus = (modulus + 1) % BYTES_PER_UNENCODED_BLOCK;
                    int b = input[inPos++];
                    if (b < 0)
                    {
                        b += 256;
                    }
                    bitWorkArea = (bitWorkArea << 8) + b;
                    if (0 == modulus)
                    {
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 35) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 30) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 25) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 20) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 15) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 10) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)(bitWorkArea >> 5) & MASK_5BITS];
                        buffer[pos++] = encodeTable[(int)bitWorkArea & MASK_5BITS];
                        currentLinePos += BYTES_PER_ENCODED_BLOCK;
                        if (lineLength > 0 && lineLength <= currentLinePos)
                        {
                            Array.Copy(lineSeparator, 0, buffer, pos, lineSeparator.Length);
                            pos += lineSeparator.Length;
                            currentLinePos = 0;
                        }
                    }
                }
            }
        }

        private void decode(byte[] input, int inPos, int inAvail)
        {
            if (eof)
            {
                return;
            }
            if (inAvail < 0)
            {
                eof = true;
            }
            for (int i = 0; i < inAvail; i++)
            {
                byte b = input[inPos++];
                if (b == PAD)
                {
                    eof = true;
                    break;
                }
                else
                {
                    ensureBufferSize(decodeSize);
                    if (b >= 0 && b < decodeTable.Length)
                    {
                        int result = decodeTable[b];
                        if (result >= 0 && result < 255)
                        {
                            modulus = (modulus + 1) % BYTES_PER_ENCODED_BLOCK;
                            bitWorkArea = (bitWorkArea << BITS_PER_ENCODED_BYTE) + result;
                            if (modulus == 0)
                            {
                                buffer[pos++] = (byte)((bitWorkArea >> 32) & MASK_8BITS);
                                buffer[pos++] = (byte)((bitWorkArea >> 24) & MASK_8BITS);
                                buffer[pos++] = (byte)((bitWorkArea >> 16) & MASK_8BITS);
                                buffer[pos++] = (byte)((bitWorkArea >> 8) & MASK_8BITS);
                                buffer[pos++] = (byte)(bitWorkArea & MASK_8BITS);
                            }
                        }
                    }
                }
            }

            if (eof && modulus >= 2)
            {
                ensureBufferSize(decodeSize);

                switch (modulus)
                {
                    case 2:
                        buffer[pos++] = (byte)((bitWorkArea >> 2) & MASK_8BITS);
                        break;
                    case 3:
                        buffer[pos++] = (byte)((bitWorkArea >> 7) & MASK_8BITS);
                        break;
                    case 4:
                        bitWorkArea = bitWorkArea >> 4;
                        buffer[pos++] = (byte)((bitWorkArea >> 8) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea) & MASK_8BITS);
                        break;
                    case 5:
                        bitWorkArea = bitWorkArea >> 1;
                        buffer[pos++] = (byte)((bitWorkArea >> 16) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea >> 8) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea) & MASK_8BITS);
                        break;
                    case 6:
                        bitWorkArea = bitWorkArea >> 6;
                        buffer[pos++] = (byte)((bitWorkArea >> 16) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea >> 8) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea) & MASK_8BITS);
                        break;
                    case 7:
                        bitWorkArea = bitWorkArea >> 3;
                        buffer[pos++] = (byte)((bitWorkArea >> 24) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea >> 16) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea >> 8) & MASK_8BITS);
                        buffer[pos++] = (byte)((bitWorkArea) & MASK_8BITS);
                        break;
                }
            }
        }

        private int getDefaultBufferSize()
        {
            return DEFAULT_BUFFER_SIZE;
        }

        private void resizeBuffer()
        {
            if (buffer == null)
            {
                buffer = new byte[getDefaultBufferSize()];
                pos = 0;
                readPos = 0;
            }
            else
            {
                byte[] b = new byte[buffer.Length * DEFAULT_BUFFER_RESIZE_FACTOR];
                Array.Copy(buffer, 0, b, 0, buffer.Length);
                buffer = b;
            }
        }

        private void ensureBufferSize(int size)
        {
            if ((buffer == null) || (buffer.Length < pos + size))
            {
                resizeBuffer();
            }
        }
    }
}
