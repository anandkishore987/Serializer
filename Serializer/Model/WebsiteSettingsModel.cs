﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using Utility.Common;
using Utility.Enum;

namespace Utility.Model
{
    public class WebsiteSettingsModel : ModelBase
    {
        int _itemPostion    = -1;
        int _iconSize       = 50;
        double _fontSize    = 12;
        string _fontFamily  = "Tahoma";
        string _color       = "Black";

        public WebsiteSettingsModel()
        {
            AllowSubDomains = true;
            URL             = "www.";
        }

        [XmlAttribute("Type")]
        public URLType Type             { get; set; }

        [XmlAttribute("URL")]
        public string URL               { get; set; }

        [XmlAttribute("Name")]
        public string Name              { get; set; }

        [XmlAttribute("AllowSubDomains")]
        public bool AllowSubDomains     { get; set; }

        [XmlAttribute("AllowOnlyThis")]
        public bool AllowOnlyThis       { get; set; }

        [XmlAttribute("StartingURL")]
        public bool StartingURL         { get; set; }

        [XmlAttribute("HideURL")]
        public bool HideURL             { get; set; }

        [XmlAttribute("ErrorRedirection")]
        public bool ErrorRedirection    { get; set; }

        [XmlAttribute("Icon")]
        public IconType Icon            { get; set; }

        [XmlAttribute("IconPath")]
        public string IconPath          { get; set; }

        [XmlAttribute("IsIdleTimeoutWebsite")]
        public bool IsIdleTimeoutWebsite { get; set; }

        [XmlIgnore]
        public bool isDummy
        {
            get { return false; }
        }

        [XmlIgnore]
        public int IconSize
        {
            get { return _iconSize; }
            set
            {
                _iconSize = value;
            }
        }

        [XmlIgnore]
        public double TotalWidth
        {
            get
            {
                if (IconSize == 50)
                {
                    return 60;
                }
                else
                {
                    return (IconSize * 60) / 50;
                }
            }
        }

        [XmlIgnore]
        public double TotalHeight
        {
            get
            {
                if (IconSize == 50)
                {
                    return 85;
                }
                else
                {
                    return (IconSize * 85) / 50;
                }
            }
        }

        [XmlIgnore]
        public double ImageHeight_Width
        {
            get { return 0.83 * TotalWidth; }
        }

        [XmlIgnore]
        public double TextHeight
        {
            get { return 0.42 * TotalHeight; }
        }

        [XmlIgnore]
        public string FontFamily
        {
            get { return _fontFamily; }
            set
            {
                _fontFamily = value;
            }
        }

        [XmlIgnore]
        public double FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
            }
        }

        //private ImageSource _iconProp;

        //[XmlIgnore]
        //public ImageSource IconProp
        //{
        //    get
        //    {
        //        if(_iconProp == null)
        //        {
        //             return new BitmapImage(new Uri("pack://application:,,,/SureLock;component/Images/globe_greyscale.png", UriKind.RelativeOrAbsolute));
        //        }
        //        else
        //        {
        //            return _iconProp;
        //        }
        //    }
        //    set
        //    {
        //        _iconProp = value;
        //        OnPropertyChanged("IconProp");
        //    }
        //}

        [XmlIgnore]
        public string LabelProp
        {
            get { return Name; }
            set { Name = value; }
        }

        [XmlAttribute("Position")]
        public int Position
        {
            get { return _itemPostion; }
            set
            {
                _itemPostion = value;
            }
        }

        private bool _selectedWebsite = false;

        [XmlIgnore]
        public bool SelectedWebsiteProp
        {
            get { return _selectedWebsite; }
            set 
            { 
                _selectedWebsite = value;
                OnPropertyChanged("SelectedWebsiteProp");
                OnPropertyChanged("Background");
            }
        }

        public event EventHandler Click;

        //RelayCommand _click;
        //public ICommand ClickCommand
        //{
        //    get
        //    {
        //        if (_click == null)
        //            _click = new RelayCommand(param => this.OnClick());
        //        return _click;
        //    }
        //}

        private void OnClick()
        {
            if (Click != null)
            {
                Click(this, null);
            }
        }

        [XmlIgnore]
        public string Background
        {
            get 
            { 
                if(SelectedWebsiteProp)
                {
                    return ApplicationConstants.AliceBlue;
                }
                else
                {
                    return ApplicationConstants.White;
                }
            }
        }

        [XmlIgnore]
        public string FontColor
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }
    }
}
