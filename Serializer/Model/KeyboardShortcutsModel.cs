﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace SureLockWin8.Model
{
    public class KeyboardShortcutsModel
    {
        [XmlElement("KeyboardShortcut")]
        public ObservableCollection<KeyboardShortcutModel> KeyboardShortcuts;

        public KeyboardShortcutsModel()
        {
            KeyboardShortcuts = new ObservableCollection<KeyboardShortcutModel>();
        }
    }
}
