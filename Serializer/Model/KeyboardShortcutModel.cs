﻿using System;
using System.Windows.Input;
using System.Xml.Serialization;
using Utility.Common;
using Utility.Enum;
using Utility.Model;
using System.Linq;

namespace SureLockWin8.Model
{
    [Serializable]
    public class KeyboardShortcutModel : ModelBase
    {
        public KeyboardShortcutModel()
        {
            Application = -1;
        }

        private string _key;
        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
                OnPropertyChanged("Key");
            }
        }

        public ushort ScanCode                      { get; set; }

        public bool CustomCode                      { get; set; }

        public int Application                      { get; set; }

        private KeyboardModifiers _keyboardModifier;
        public KeyboardModifiers KeyboardModifier
        {
            get
            {
                return _keyboardModifier;
            }
            set
            {
                _keyboardModifier = value;
                OnPropertyChanged("KeyboardModifier");
            }
        }

        private bool KeySelected                    { get; set; }

        [XmlIgnore]
        public string Background
        {
            get
            {
                if (KeySelectedProp)
                {
                    return ApplicationConstants.AliceBlue;
                }
                else
                {
                    return ApplicationConstants.White;
                }
            }
        }

        public event EventHandler Click;

        //RelayCommand _click;
        //public ICommand ClickCommand
        //{
        //    get
        //    {
        //        if (_click == null)
        //            _click = new RelayCommand(param => this.OnClick());
        //        return _click;
        //    }
        //}

        private void OnClick()
        {
            if (Click != null)
            {
                Click(this, null);
            }
        }

        string _applicationProp;
        [XmlIgnore]
        public string ApplicationProp
        {
            get
            {
                return _applicationProp;
            }
            set
            {
                _applicationProp = value;
                OnPropertyChanged("ApplicationProp");
            }
        }

        [XmlIgnore]
        public bool KeySelectedProp
        {
            get { return KeySelected; }
            set
            {
                KeySelected = value;
                OnPropertyChanged("KeySelectedProp");
                OnPropertyChanged("Background");
            }
        }
    }
}
