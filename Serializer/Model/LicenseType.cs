﻿namespace Utility.Enum
{
    public enum LicenseType
    {
        PERPETUAL       = 0,
        SUBSCRIPTION    = 1
    }
}
