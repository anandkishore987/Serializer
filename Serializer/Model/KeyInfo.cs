﻿using System.Text;
using Utility.Enum;

namespace Utility.License
{
    public class KeyInfo
    {
        public string licensedTo;
        public bool isValidKey;
        public bool isVerified;
        public ShortDate expiryDate;
        public int nDevices;
        public int productCode;
        public DeviceIDType type;
        public LicenseType license_Type;
        public int salt;
        public float security;
        public int errorCode;
		public bool isGracePeriodCheckRequireConnectivity;

        public KeyInfo()
        {
            licensedTo                              = "NONE";
            isValidKey                              = false;
            isVerified                              = false;
            expiryDate                              = null;
            nDevices                                = 0;
            productCode                             = -1;
            salt                                    = 0;
            security                                = 0;
            errorCode                               = 0;
			license_Type                            = LicenseType.PERPETUAL;
			isGracePeriodCheckRequireConnectivity   = false;
        }        

        public override string ToString()
        {
            var sb = new StringBuilder();

            if (isValidKey)
            {
                sb.Append("This Key is Licensed to: " + licensedTo + "\n");
                sb.Append("This Key Expires on: " + expiryDate + "\n");
                sb.Append("This Key is valid for " + nDevices + " devices\n");
                sb.Append("Product: " + productCode + "\n");
                sb.Append("Device Key Type: " + type + "\n");
                sb.Append("Salt: " + salt + "\n");
                sb.Append("Security: " + security + "%\n");
                sb.Append("Verified: " + isVerified);
				sb.Append("GracePeriod Requires Connectivity: " + isGracePeriodCheckRequireConnectivity);
            }
            else
            {
                sb.Append("Invalid KEY\n");
                sb.Append("Error Code:" + errorCode);
            }

            return sb.ToString();
        }
    }
}
