﻿
namespace Utility.Enum
{
    public enum KeyboardModifiers
    {
        NONE,
        CTRL,
        ALT,
        SHIFT
    }
}
