﻿using System;
using System.Xml.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Utility.Model;
using System.Windows.Input;
using Utility.Common;

namespace SureLockWin8.Model
{
    [Serializable]
    public class ApplicationSettingsModel : ModelBase
    {
        int _iconSize       = 50;
        int _itemPostion    = -1;
        double _fontSize    = 12;
        string _fontFamily  = "Tahoma";
        string _color       = "Black";
        private bool _appSelected;
        private bool _isPinRunning;
        private bool _dummy;

        public ApplicationSettingsModel(bool _isdummy) : base()
        {
            _dummy      = _isdummy;
        }

        public ApplicationSettingsModel()
        {
            RunAsAdmin  = true;
        }               

        [XmlAttribute("Label")]
        public string Label                 { get; set; }

        [XmlAttribute("Parameter")]
        public string Parameter             { get; set; }

        [XmlAttribute("IsIdleTimeoutApp")]
        public bool IsIdleTimeoutApp        { get; set; }

        [XmlAttribute("RunAsAdmin")]
        public bool RunAsAdmin              { get; set; }

        [XmlAttribute("HideIconOnHomeScreen")]
        public bool HideIconOnHomeScreen    { get; set; }

        [XmlAttribute("LaunchAtStarup")]
        public bool LaunchAtStarup          { get; set; }

        [XmlAttribute("PinToTaskbar")]
        public bool PinToTaskbar            { get; set; }

        [XmlAttribute("Minimized")]
        public bool Minimized               { get; set; }

        [XmlAttribute("WorkingDirectory")]
        public string WorkingDirectory      { get; set; }

        [XmlAttribute("ExePath")]
        public string ExePath               { get; set; }

        [XmlIgnore]
        private string _iconPath = string.Empty;

        [XmlAttribute("IconPath")]
        public string IconPath
        {
            get
            {
                return _iconPath;
            }
            set
            {
                _iconPath = value;
            }
        }

        [XmlAttribute("Position")]
        public int Position
        {
            get { return _itemPostion; }
            set
            {
                _itemPostion = value;
            }
        }

        [XmlIgnore]
        public string LabelProp 
        {
            get { return Label; }
            set { Label = value; }
        }        

        [XmlIgnore]
        public bool isDummy
        {
            get { return _dummy; }
            set
            {
                _dummy = value;
            }
        }

        [XmlIgnore]
        public int IconSize
        {
            get { return _iconSize; }
            set
            {
                _iconSize = value;
            }
        }

        [XmlIgnore]
        public double TotalWidth
        {
            get 
            {
                if (IconSize == 50)
                {
                    return 60;
                }
                else
                {
                    return (IconSize*60)/50;
                }
            }
        }

        [XmlIgnore]
        public double TotalHeight
        {
            get
            {
                if (IconSize == 50)
                {
                    return 85;
                }
                else
                {
                    return (IconSize * 85) / 50;
                }
            }
        }        

        [XmlIgnore]
        public string FontColor
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }

        [XmlIgnore]
        public string FontFamily
        {
            get { return _fontFamily; }
            set
            {
                _fontFamily = value;
            }
        }

        [XmlIgnore]
        public double FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
            }
        }

        //[XmlIgnore]
        //public ImageSource IconProp
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(IconPath))
        //        {
        //            return new BitmapImage(new Uri(IconPath));
        //        }
        //        else
        //        {
        //            return Utils.Utils.IconFromFilePath(ExePath);
        //        }
        //    }
        //}

        [XmlIgnore]
        public string ExePathProp
        {
            get { return ExePath; }
            set { ExePath = value; }
        }

        [XmlIgnore]
        public string ParameterProp
        {
            get { return Parameter; }
            set { Parameter = value; }
        }

        [XmlIgnore]
        public double ImageHeight_Width
        {
            get { return 0.83 * TotalWidth; }
        }

        [XmlIgnore]
        public double TaskbarIconWidth
        {
            get
            {
                double result = UseSmallIcons ? ApplicationConstants.PinApplicationSmallWidth : ApplicationConstants.PinApplicationWidth;
                return result;
            }
        }

        [XmlIgnore]
        public bool UseSmallIcons { get; set; }

        [XmlIgnore]
        public double TextHeight
        {
            get { return 0.42 * TotalHeight; }
        }

        public override string ToString()
        {
            return ExePath;
        }

        [XmlIgnore]
        public bool AppSelectedProp
        {
            get { return _appSelected; }
            set 
            { 
                _appSelected = value;
                OnPropertyChanged("AppSelectedProp");
                OnPropertyChanged("Background");
            }
        }

        [XmlIgnore]
        public bool IsPinRunning
        {
            get
            {
                return _isPinRunning;
            }
            set
            {
                _isPinRunning = value;
                OnPropertyChanged("IsPinRunning");
            }
        }
        
        public event EventHandler Click;

        //RelayCommand _click;
        //public ICommand ClickCommand
        //{
        //    get
        //    {
        //        if (_click == null)
        //            _click = new RelayCommand(param => this.OnClick());
        //        return _click;
        //    }
        //}

        private void OnClick()
        {
            if(Click != null)
            {
                Click(this, null);
            }
        }

        [XmlIgnore]
        public string Background
        {
            get 
            {
                if (AppSelectedProp)
                {
                    return ApplicationConstants.AliceBlue;
                }
                else
                {
                    return ApplicationConstants.White; 
                }
            }
        }
    }
}
