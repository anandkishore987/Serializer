﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SureLockWin8.Model
{
    [XmlRootAttribute("MouseFilteringSettings", Namespace = "", IsNullable = true)]
    [Serializable]
    public class MouseFiltering
    {
        [XmlElement("RightMouseButton")]
        public bool rightMouseButton;

        [XmlElement("MouseWheelScroll")]
        public bool mouseWheelScroll;
    }
}
