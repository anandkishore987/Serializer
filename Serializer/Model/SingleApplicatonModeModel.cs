﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SureLockWin8.Model
{
    [XmlRootAttribute("SingleApplicationMode", Namespace = "", IsNullable = false)]
    [Serializable]
    public class SingleApplicationModeModel
    {

        [XmlElement("SingleAppMode")]
        public bool SingleAppMode;

        //[XmlElement("AppLaunchDelay")]
        //public long AppLaunchDelay;
    }
}
