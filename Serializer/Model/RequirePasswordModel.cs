﻿
namespace SureLockWin8.Model
{
    public class RequirePasswordModel
    {
        public uint DCValue { get; set; }

        public uint ACValue { get; set; }

        public RequirePasswordModel(uint acValue, uint dcValue)
        {
            ACValue = acValue;
            DCValue = dcValue;
        }
    }
}
