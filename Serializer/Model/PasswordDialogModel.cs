﻿using System.Xml.Serialization;
using System;

namespace SureLockWin8.Model
{
    [XmlRootAttribute("Password", Namespace = "", IsNullable = false)]
    [Serializable]
    class PasswordDialogModel
    {
       
        [XmlAttribute("TextBoxValue")]
        public string TextBoxValue = "";
    }

}
