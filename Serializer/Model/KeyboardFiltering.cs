﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace SureLockWin8.Model
{
    [XmlRootAttribute("KeyboardFilteringSettings", Namespace = "", IsNullable = true)]
    [Serializable]
    public class KeyboardFiltering
    {
        [XmlElement("KeyboardFiltering")]
        public bool enableKeyboardFiltering;

        [XmlElement("Keyboard_Combination")]
        public ObservableCollection<KeyboardFilteringModel> keyboardCombinations;

        [XmlElement("KeyboardShortcuts")]
        public KeyboardShortcutsModel Shortcuts;

        public KeyboardFiltering()
        {
            keyboardCombinations    = new ObservableCollection<KeyboardFilteringModel>();
            Shortcuts               = new KeyboardShortcutsModel();
        }
    }
}
