﻿using System;
using System.IO;
using System.Windows.Media;

namespace Utility.Common
{
    //http://stackoverflow.com/questions/4412915/how-to-deactivate-right-click-on-wpf-webbrowser-control/21699086#21699086
    //http://blog.codeinside.eu/2015/01/07/using-fontawesome-with-wpf/

    public class ApplicationConstants
    {
        public const string BUILD_DATE                          = "06/04/18";   
        public const string KeyVerifierVersion                  = "4";
        private const string LiveActivationServer               = "http://activate.42gears.com";
        public static readonly string ActivationServer          = File.Exists(ActivationFilePath) ? File.ReadAllText(ActivationFilePath) : LiveActivationServer;
        public static readonly string ActivationServerUrl       = ActivationServer + "/activatedevice.ashx";
        public static readonly string DeactivationServerUrl     = ActivationServer + "/deactivatedevice.ashx";
        public static readonly string CloudGetSettingsUrl       = ActivationServer + "/getsettings.ashx";
        public static readonly string CloudPutSettingsUrl       = ActivationServer + "/putsettings.ashx";        

        public static Brush GridBackgroundColor                 = (Brush)new BrushConverter().ConvertFrom("#f1f4f8");
        public static Brush HeaderBackgroundColor               = (Brush)new BrushConverter().ConvertFrom("#2c3e50");
        public static Brush BorderGrid                          = Brushes.White;

        public const string pipeName                            = "net.pipe://localhost/42gears/surelock";
        public const string apiPipeName                         = "net.pipe://localhost/42gears/surelockapi";

        public static readonly IntPtr HWND_NOTOPMOST            = new IntPtr(-2);
        public static readonly IntPtr HWND_TOP                  = new IntPtr(0);
        public static readonly IntPtr HWND_BOTTOM               = new IntPtr(1);
        public static readonly IntPtr HWND_TOPMOST              = new IntPtr(-1);
        public const int WM_SYSCOMMAND                          = 0x0112;
        public const int SC_MONITORPOWER                        = 0xF170;
        public const int WM_GETMINMAXINFO                       = 0x0024;
        public const uint WS_EX_TOOLWINDOW                      = 0x00000080;
        public const uint WS_EX_TOPMOST                         = 0x00000008;
        public const uint WS_EX_NOACTIVATE                      = 0x08000000;
        public const int WM_COPYDATA                            = 0x004A;
        public const int WM_PARENTNOTIFY                        = 0x210;
        public const int WM_DESTROY                             = 2;
        public const uint STGM_READ                             = 0;
        public const int MAX_PATH                               = 260;
        public const int    WM_KILLFOCUS                        = 0x0008;
        public const int    WM_KEYDOWN                          = 0x0100;
        public const int    WM_SYSKEYDOWN                       = 0x0104;
        public const int    WM_KEYUP                            = 0x0101;
        public const int    ALT                                 = 0xA4;
        public const int    EXTENDEDKEY                         = 0x1;
        public const int    KEYUP                               = 0x2;
        public const int    MONITOR_ON                          = -1;
        public const int    MONITOR_OFF                         = 2;
        public const int    MONITOR_STANBY                      = 1;
        public const int    SC_CLOSE                            = 0xF060;
        public const int    WM_POWERBROADCAST                   = 0x0218;
        public const int    HWND_BROADCAST                      = 0xffff;
        public const int    DEVICE_NOTIFY_WINDOW_HANDLE         = 0x00000000;
        public const int    PBT_POWERSETTINGCHANGE              = 0x8013;       
        public const int    SURELOCKWIN8                        = 0;
        public const int    LowLevelKeyboardHook                = 13;
        public const int    LowLevelMouseHook                   = 14;
        public const UInt32 SWP_NOSIZE                          = 0x0001;
        public const UInt32 SWP_NOMOVE                          = 0x0002;
        public const UInt32 SWP_SHOWWINDOW                      = 0x0040;
        public const UInt32 INFINITE                            = 0xFFFFFFFF;
        public const uint   SPI_SETWINARRANGING                 = 0x0083;
        public const uint   SPI_GETWINARRANGING                 = 0x0082;
        public const int    GWL_STYLE                           = -16;
        public const long   WS_MINIMIZEBOX                      = 0x00020000L;
        public const long   WS_MAXIMIZEBOX                      = 0x00010000L;
        public const long   WS_SIZEBOX                          = 0x00040000L;
        public const int 	TapDelay                  			= 666;
        public const int    FontSizeStart                       = 10;
        public const int    FontSizeEnd                         = 600;
        public const int PinApplicationWidth                    = 44;
        public const int PinApplicationSmallWidth               = 24;

        public const int  ExpiryCheckDuration                   = 6 * 60 * 60 * 1000;
        public const double trialMessageStartTimerDuration      = 60000;        //60 seconds
        public const int trialMessageEndTimerDuration           = 5000;         //5 seconds
        public const double passwordTimerDuration               = 30000;        //30 seconds
        
        public const string ACTCODE_ATTRIBUTE                   = "ActivationCode";
        public const string ACTCODE_PASSPHRASE                  = "42Gears Mobility Systems";
        public const string TOAST_WINDOW_TITLE                  = "42GearsToast";
        public const string START_MENU_PATH                     = @"C:\ProgramData\Microsoft\Windows\Start Menu";
        public const string TabTipPath                          = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
        public const string OSKPath                             = @"C:\Windows\System32\osk.exe";
        public const string OSK                                 = "osk.exe";
        public static string[] SYSTEM_APPS                      = { @"c:\windows\system32\notepad.exe",@"c:\program files\internet explorer\iexplore.exe" };
        public const string WinLogonKey                         = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon";
        public const string CurrentVersionKey                   = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion";
        public const string TaskbarBackgroundColor              = "#2D6FA2";
        public const string White                               = "White";
        public const int Win10TabTipVersion                     = 1703;
        public const string AliceBlue                           = "#F0F8FF";
        public const string DefaultPassword                     = "0000";
        public const string ButtonBlue                          = "#4a90e2";
        public const string DisableColor                        = "#e6e6e6";
        public const int TaskbarHeight                          = 50;
        public const int TaskbarHeight_Small                    = 30;

        public const string OfflineLicenseFilePath              = @"C:\42gears_surelock.lic";
        public const string ActivationFilePath                  = @"C:\42gears_surelock_activation.txt";
        public const string DefaultImportExportPath             = @"C:\Users\SureLock.settings";
        
        public const string Grey                                = "#E0E0E0";
        public const string GreyText                            = "#FF808080";
        public const string Black                               = "#000000";
        public const string AnalyticsTimeFormat                 = @"dd'/'MM'/'yy hh:mm:ss tt";
        public const string BuildDateFormat                     = "dd/MM/yy";
        public const string BuildDateFormatString               = "dd'/'MM'/'yy";
        public const string HexadecimalNul                      = "\0";

        public const string ActivationSuccess                   = "Activation Successful";
        public const string RenewSuccess                        = "License Renewed Successfully";
        public const string Invalid_Input                       = "Invalid value entered";
        public const string Configure_Taskbar_Color             = "Configure Taskbar Color";
        public const string RenewLicError                       = "Cannot Activate Product. Invalid Key. Error code: {0}";
        public const string ContextMenuEnableSummary            = "Context Menu should be enabled";
        public const string InvalidPassword_MDM                 = "SureLock: Invalid Password Received";
        public const string ApplySettings_MDM                   = "SureLock: Apply Settings Command Received";
        public const string RenewKeyEmpty                       = "Renew Key cannot be empty";
        public const string NoNotification                      = "No new notifications";
        public const string NewNotifications                    = "{0} new notifications";
        public const string NewNotification                     = "1 new notification";
        public const string NoItemsSelected                     = "No items selected";
        public const string SingleItemSelected                  = "{0} item selected";
        public const string MultipleItemsSelected               = "{0} items selected";
        public const string Invalid_App                         = "Invalid Application";
        public const string Invalid_URL                         = "Invalid URL";
        public const string Invalid_IconPath                    = "Invalid Icon Path";
        public const string Invalid_CloudId                     = "Invalid Cloud ID";
        public const string ImportSettingsTitle                 = "Import Settings from Cloud";
        public const string SubscriptionLicValidMessage         = "License valid till";
        public const string PerpetualLicValidMessage            = "Free upgrades available till";
        public const string Import_Success                      = "Imported successfully";
        public const string AllowedAppTrialMessage              = "Only two applications are allowed in the trial version.";
        public const string AllowedWebTrialMessage              = "Only two websites are allowed in the trial version.";
        public const string AllowedKeyTrialMessage              = "Only two combinations are allowed in the trial version.";
        public const string UnableImportMessage                 = "Unable to import the file.";
        public const string UnableImportCloud                   = "Unable to import settings file from cloud.";
        public const string UnableLoadSettingsMessage           = "Unable to load the settings file.";
        public const string SureLockTrialVersion                = "SureLock Trial Version";
        public const string Mandatory                           = "Mandatory fields cannot be left blank";
        public const string analyticsSummary                    = "Tap to configure SureLock Analytics";
        public const string analyticsWatchdogSummary            = "Enable Watchdog service to use this feature";
        public const string wifiManagerErrorMessage             = "Unable to connect to this network.";
        public const string IdleTimeoutAppDefaultSummary        = "Go to SureLock homescreen";
        public const string NavigateURLEnableSummary            = "Display URL instead of page title in the title bar";
        public const string NavigateURLDisableSummary           = "Show Title Bar should be enabled";
        public const string ActivateSummary                     = "Press to activate device license online";
        public const string ActivateSummaryActivated            = "Product Activated";
        public const string ActivateText                        = "Activate License";
        public const string RenewText                           = "Renew";
        public const string ActivationTitle                     = "Enter activation code";
        public const string RenewKeyTitle                       = "Enter renew key";
        public const string ScheduleSuspendSummary              = "Specify a time interval during which Prevent Suspend should be effective";
        public const string FeatureNotPresent                   = "This feature is not supported on this device";
        public const string InternetConnectivityErrorMessage    = "Please make sure device has internet connectivity.";
        public const string UnRegisteredText                    = "Unregistered version for evaluation use only";
        public const string trialError                          = "This feature is not available in the trial version.";
        public const string exportActivationCodeSummary         = "Include encrypted Activation Code in Settings file";
        public const string resetSettingsWarningMessage         = "This will reset all settings including allowed application/website list.";
        public const string resetConfirmationMessage            = "Do you want to continue?";
        public const string applySettingsMessage                = "Applying Settings";
        public const string InvalidSettings                     = "Invalid Settings";
        public const string autoImportMessage                   = "Auto Import In Progress";
        public const string waitMessage                         = "Please wait...";
        public const string autoImportTitle                     = "Auto Import";
        public const string daysofWeekConfigureMessage          = "Tap to configure days of the week";
        public const string singleAppModeWarning                = "IMPORTANT: To exit Single Application Mode, press Windows/Home Button 10 times.";
        public const string singleAppModeError                  = "This option is available only when you have set one allowed application or website.";
        public const string addWebsiteMessage                   = "Please add a URL.";
        public const string selectWebsiteMessage                = "Please select a URL.";
        public const string enterURLMessage                     = "Please enter a URL.";
        public const string AssociatedAppMessage                = "This file has an associated application. Would you like to add {0} to the allowed application list ? ";
        public const string installKeyboardDriverMessage        = "To install the keyboard driver, the device needs to be restarted";
        public const string uninstallKeyboardDriverMessage      = "To uninstall the keyboard driver, the device needs to be restarted";
        public const string exportCloudMessage                  = "Exporting your device settings";
        public const string importCloudMessage                  = "Importing your device settings";
        public const string exportCloudTitle                    = "Export to Cloud";
        public const string importCloudTitle                    = "Import to Cloud";
        public const string newCloudIdConfirmationMessage       = "Settings were successfully exported to cloud. Please note this ID for future reference.";
        public const string tapToConfigureCloudMessage          = "Tap to configure Auto Import Cloud ID";
        public const string DeleteApplication_Many              = "Do you want to delete the selected items?";
        public const string DeleteApplication_Single            = "Do you want to delete the selected item?";
        public const string AddApplication                      = "Please add an application.";
        public const string SelectApplication                   = "Please select an application.";
        public const string Confirmation                        = "Confirmation";
        public const string SelectOneApplication                = "Please select only one Application.";
        public const string SelectOneWebsiteMessage             = "Please select only one URL.";
        public const string ApplicationExists                   = "Application already exists.";
        public const string WebsiteExists                       = "URL already exists.";
        public const string ActivationErrorMsg                  = "Unable to Activate device.";
        public const string ShutdownMessage                     = "Do you want to shutdown the device?";
        public const string LogoffMessage                       = "Do you want to Logoff the device?";
        public const string EnterPasscode                       = "Please enter the passcode";
        public const string PasswordChanged                     = "Password changed sucessfully";
        public const string PasscodeMismatch                    = "Passcode entered did not match";
        public const string NumberOfTapsLimit                   = "Please enter value between 4 and 60";
        public const string NumberOfTabsLimit                   = "Please enter value between 1 and 8";
        public const string NumberOfTabsSummary                 = "Maximum number of tabs allowed is {0}";
        public const string EnableTabBrowsing                   = "Allow Tabbed Browsing should be enabled";
        public const string FontSizeLimit                       = "Please enter value between 10 and 600";
        public const string IconSizeLimit                       = "Please enter value between 12 and {0}";
        public const string ScreensaverTimeoutSummary           = "Force Screensaver after {0} of inactivity";
        public const string InvalidValue                        = "Screensaver Timeout should be minimum 10 seconds";
        public const string ScreensaverTimeoutTitle             = "Screensaver Timeout";
        public const string IdleTimeoutTitle                    = "Idle Timeout";
        public const string ValidImageFile                      = "Please enter a valid image file";
        public const string EmptyPath                           = "File path cannot be empty";
        public const string ScreensaverSummary                  = "Tap to configure screensaver settings";
        public const string ScreensaverError                    = "Please select a type of screensaver";
        public const string DisablePreventSuspendMode           = "Prevent Suspend Mode should be disabled";
        public const string FontErrorMessage                    = "Please enter a valid value";
        public const string PinErrorMessage                     = "Please enter a pin";
        public const string SimBlockedMessage                   = "Your SIM cannot be accessed. Please call your service provider to obtain your PUK code.";
        public const string Simlocked                           = "SIM Card Locked";
        public const string licensedText                        = "This product is licensed to: ";
        public const string TabCountLimitMessage                = "You cannot open more than {0} {1}";
        public const string SETTINGS_FILE_NOTFOUND              = "Settings file doesn't exist.";
        public const string INVALID_SETTINGS                    = "Invalid Settings.";
        public const string Errow_Redirection_Message           = "Invalid URL. Redirecting to Error Page.";
        public const string Enable_Tabbed_Browsing              = "Please Allow Tabbed Browsing.";
        public const string MissingFiles                        = "Please install SureLock";
        public const string Message_Reboot                      = "To start SureLock Lockdown, the device needs to be rebooted.";
        public const string Message_NoReboot                    = "It is recommended to reboot the computer.";
        public const string Confirmation_Reboot                 = "Do you wish to continue?";
        public const string Confirmation_NoReboot               = "Reboot Now?";
        public const string DownloadSuccess                     = "File Download has completed. Do you want to open the file?";
        public const string DownloadComplete                    = "Download Complete";
        public const string ScreenSaverError                    = "Screensaver timeout cannot be more than idle timeout ({0})";
        public const string IdleTimeOutError                    = "Idle timeout cannot be lesser than screensaver/display timeout ({0})";
        public const string DisabledScreenSaverSummary          = "Screensaver should be enabled";
        public const string ScreenSaverSummary                  = "Check to use system wallpaper as screensaver";
        public const string UseSystemWallpaperSummary           = "Use System Wallpaper should be disabled";
        public const string ScreenSaverConfigSummmary           = "Tap to configure screensaver media or url";
        public const string DisabledScheduledPreventSummary     = "Schedule Prevent Suspend Mode should be enabled";
        public const string EnabledScheduledPreventSummary      = "Schedule Prevent Suspend should be disabled";
        public const string AC_PowerSummary                     = "Keep screen on only when AC Power plugged in";
        public const string EnabledAC_PowerSummary              = "AC Power Prevent Suspend should be disabled";
        public const string DisabledPreventSuspendMode          = "Prevent Suspend Mode should be enabled";
        public const string DisabledSureLockTaskBar             = "SureLock Taskbar should be enabled";
        public const string NotificationSummary                 = "Enable/Disable Notifications in Taskbar";
        public const string ConfigureTaskbarSummary             = "Tap to configure SureLock Taskbar Color";
        public const string ConfigureTouchKeyboardSummary       = "Tap to configure Touch keyboard";
        public const string RebootSummary                       = "Enable/Disable Reboot in Taskbar";
        public const string LogOffSummary                       = "Enable/Disable Log Off in Taskbar";
        public const string ShutDownSummary                     = "Enable/Disable Shutdown in Taskbar";
        public const string AutoHideSummary                     = "Check to auto-hide the SureLock Taskbar";
        public const string UseSmallTaskBarSummary              = "Change how buttons appear on the taskbar";
        public const string NetworkSettingSummary               = "Configure Airplane Mode, Wi-Fi, Cellular Network and Mobile Hotspot settings";
        public const string DisabledIdleTimeoutSummary          = "Idle Timeout should be enabled";
        public const string CloseApplicationSummary             = "Close open applications on Idle Timeout";
        public const int Injected                               = 16;
        public const int ExtentedInjected                       = 17;
        public const int AltPressed                             = 48;

        public const string licensedIconPath                    = "..\\Images/info_green1.png";
        public const string unRegisteredIconPath                = "..\\Images/info_red.png";

        //Browser Agents
        public const string IE                                  = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)";
        public const string Desktop		                        = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24";
        public const string I_phone                             = "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3";
        public const string Firefox                             = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0";
        public const string Safari                              = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1";
        public const string I_pad                               = "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10";
        public const string Opera                               = "Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02";
        public const string Opera_mini                          = "Opera/9.80 (J2ME/MIDP; Opera Mini/9.80 (J2ME/23.377; U; en) Presto/2.5.25 Version/10.54";
        public const string InvalidCustomAgent                  = "Browser agent cannot be blank";

        public const string SureLockTaskbarName                 = "42Taskbar";
        public const string OskModule                           = "osk.exe";
        public static string[] WhiteListWindows                 = { "Program Manager", SureLockTaskbarName, "Application Information", "SureLock", "autoImportLoadingView","WifiCenterView","VolumeControlView","42GearsToast", "On-Screen Keyboard" };
        public static string[] WhiteListModules                 = { OskModule };

        public static string[] BlackListApps                    = { "shellexperiencehost.exe","propertypageslauncher.exe","dcshelper.exe", OskModule };
        public static string[] AllowedModules                   = { "surelock.exe", "surefox.exe", "nix agent.exe"};
        public static string[] FileExtensions                   = { ".lnk", ".exe" };
        public static string[] FileAssociationApp               = { "openwith.exe","rundll32.exe" };
        public static string[] AppsListExceptions               = { "C:\\WINDOWS\\Camera\\Camera.exe" };

        public const string TaskbarTemplate                     = "TaskbarTemplate";
        public const string TaskbarTemplateSmall                = "TaskbarTemplateSmall";
        public const string WideIconTemplate                    = "WideIconTemplate";
        public const string WideIconTemplateSmall               = "WideIconTemplateSmall";
        public const string PinApplicationTemplate              = "PinApplicationTemplate";

        public const string FaviconUrl                          = "http://www.google.com/s2/favicons?domain={0}";
        
        public static Guid  PBUTTONACTION                       = new Guid(0x7648efa3, 0xdd9c, 0x4e3e, 0xb5, 0x66, 0x50, 0xf9, 0x29, 0x38, 0x62, 0x80);
        public static Guid  GUID_LIDCLOSE_ACTION                = new Guid(0x5CA83367, 0x6E45, 0x459F, 0xA2, 0x7B, 0x47, 0x6B, 0x1D, 0x01, 0xC9, 0x36);
        public static Guid  GUID_SLEEPBUTTON_ACTION             = new Guid(0x96996BC0, 0xAD50, 0x47EC, 0x92, 0x3B, 0x6F, 0x41, 0x87, 0x4D, 0xD9, 0xEB);
        public static Guid  GUID_SYSTEM_BUTTON_SUBGROUP         = new Guid("4f971e89-eebd-4455-a8de-9e59040e7347");
        public static Guid  GUID_BATTERY_PERCENTAGE_REMAINING   = new Guid("A7AD8041-B45A-4CAE-87A3-EECBB468A9E1");
        public static Guid  SLEEPGUID                           = new Guid("238c9fa8-0aad-41ed-83f4-97be242c8f20");
        public static Guid  WAKETIMERGUID                       = new Guid("bd3b718a-0680-4d9d-8ab2-e1d2b4ac806d");
        public static Guid  SUB_NONEGUID                        = new Guid("fea3413e-7e05-4911-9a71-700331f1c294");
        public static Guid  CONSOLE_LOCKGUID                    = new Guid("0e796bdb-100d-47d6-a2d5-f7d2daa51f51");

        public const string FontFamily                          =  "Segoe UI";
        
        public const string FileAssociationRegistryPath         = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\{0}\\UserChoice";
        public const string FileExtRegPath                      = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts";
        public const string ResetPasswordRegistryKey            = "ResetPwd";
        public const string ImageFileFilter                     = "Image Files (.png)|*.png|Image Files (.jpeg)|*jpeg |Image Files (.jpg)|*jpg";
        public const string IconImageFilter                     = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)|*.jpg; *.jpeg; *.png; *.bmp";
        
        public const string ActivationResponseTitle             = "Activation";
        public const string ToastView                           = "42GearsToast";
        public const string AnalyticsTableName                  = "SureLockAnalytics";
        public const string CloudTableName                      = "CloudAccess";
        public const string DatabaseSourceName                  = "SureLock.sqlite";
        public const string SettingsFileName                    = "SureLock.settings";
        public const string SureLockPasswordFile                = "SureLock_Password.txt";
        public const string SureLock_Window                     = "SureLock";
        public const string WifiCenterView                      = "WifiCenterView";
        public const string PASSPHRASE                          = "42Gears";
        public const string Error                               = "Error";
        public const string TabItemTitle                        = "42Gears SureFox";
        public const string Tabs                                = "tabs";
        public const string Tab                                 = "tab";

        public const string ACCESS_DENIED                       = "Access Denied for {0}";
        public const string CANCEL_NAV_URI                      = "res://ieframe.dll/navcancl.htm";
        public const string IEFRAME_URI                         = "res://ieframe.dll";
        public const string AboutBlank                          = "about:blank";
        public const string JAVASCRIPTCALL                      = "javascript:";
        public static string[] Protocols                        = { "http://","https://","file:///"};
        public static string[] NtpServers                       = { "time.windows.com", "time1.google.com", "pool.ntp.org" };
        public const string SureFox                             = "SureFox.exe";
        public const string BROWSER_EMULATION_KEY               = SureFox;
        public const string SureLock_Exe                        = "SureLock.exe";
        public const string IconSize                            = "Icon Size";
        public const string BrowserAgent                        = "Browser Agent";
        public const string ZoomLevel                           = "Zoom Level";
        public const string ZoomLevelSummary                    = "Set default zoom level";
        public const string EnableZoomSummary                   = "Enable zoom to use this feature";
        public const string SetCustomIconSize                   = "Set Custom Icon Size";
        public const string SetCustomZoomLevel                  = "Set Custom Zoom Level";
        public const string ZoomLevelLimit                      = "Please enter value between 10 and 1000";
        public const string Add                                 = "Add";
        public const string Save                                = "Save";
        public const string FileExists                          = "File already exists.";
        public const string addMediaMessage                     = "Please add a Media.";
        public const string selectMediaMessage                  = "Please select a Media.";
        public const string enterMediaMessage                   = "Please enter a Media.";
        public const string SelectOneMediaMessage               = "Please select only one Media.";
        public const string SelectValidMedia                    = "Please select a valid media file.";
        public const string ShufflePlaylistSummary              = "Check to play playlist in random order.";
        public const string MediaFileFilter                     = "Media Files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png ,*.mp4, *.wmv, *.3gp, *.m4v, *.mkv, *.mov, *.avi)|*.jpg;*.jpeg;*.jpe;*.jfif;*.png;*.mp4;*.wmv;*.3gp;*.m4v;*.mkv;*.mov;*.avi";
        public const string TabTipWindowClassName               = "IPTIP_Main_Window";
        public const string HotSpotSuccess                      = "Hotspot created successfully";
        public const string HotSpotFailed                       = "Hotspot creation failed";
        public const string Success                             = "Success";
        public const string PasswordLength                      = "Password length should be at least 8 characters";

        //Registry Key
        public const string InternetExplorerKey                 = @"SOFTWARE\Microsoft\Internet Explorer\Main";
        public const string FormSuggest_Passwords               = "FormSuggest Passwords";
        public const string FormSuggest_PW_Ask                  = "FormSuggest PW Ask";
        public const string AutoFillYes                         = "yes";
        public const string AutoFillNo                          = "no";
        public const string HotspotErrorMessage                 = "Unable to configure hotspot, please try again later..";
        public const string MandatoryFields                     = "Please provide network name and password to create hotspot";

        //Html Element types
        public const string Type                                = "type";
        public const string Text                                = "text";
        public const string Password                            = "password";
        public const string Email                               = "email";
        public const string Iframe                              = "iframe";
        public const string Input                               = "INPUT";
        public const string Search                              = "search";
        public const string Tel                                 = "tel";
        public const string Url                                 = "url";

    }
}
