﻿using System;
using System.Xml.Serialization;

namespace Utility.Model
{
    [XmlRoot(IsNullable = true)]
    [Serializable]
    public class DisplaySettingsModel
    {
        public DisplaySettingsModel()
        {
            TitleBar               = true;
            LoadProgress           = true;
            NavigationURL          = true;
            ToastMessage           = true;
            FullScreen             = true;
            Toolbar                = false;
            ContextMenu            = true;
            ContextMenuSettings    = new ContextMenuSettingsModel();
            EnableZoom             = true;
            EnableTabBrowsing      = false;
            NumberOfTabs           = 8;
            DefaultZoomLevel       = 100;
        }

        public bool ContextMenu                             { get; set; }

        public bool TitleBar                                { get; set; }

        public bool LoadProgress                            { get; set; }

        public bool NavigationURL                           { get; set; }

        public bool ToastMessage                            { get; set; }

        public bool SecurityWarning                         { get; set; }

        public bool FullScreen                              { get; set; }

        public bool Toolbar                                 { get; set; }

        public bool EnableZoom                              { get; set; }

        public int DefaultZoomLevel                         { get; set; }

        public bool EnableTabBrowsing                       { get; set; }

        public int NumberOfTabs                             { get; set; }

        public ContextMenuSettingsModel ContextMenuSettings { get; set; }
    }
}
