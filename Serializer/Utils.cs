﻿using Microsoft.Win32;
using SureLockWin8.Common;
using SureLockWin8.Model;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Utility;
using Utility.Common;
using Utility.Interfaces;
using Utility.Logger;
using Utility.Model;

namespace SureLockWin8.Utils
{
    static class Utils
    {
        /// <summary>
        /// Method to verify the SureLock password with the password sent by Nix
        /// </summary>
        /// <param name="surelockPassword"></param>
        /// <param name="encryptedPassword"></param>
        /// <returns></returns>
        public static bool VerifyPassword(string surelockPassword,string encryptedPassword)
        {
            bool retVal         = false;
            SHA256 sha256       = new SHA256Managed();
            var bytes           = Encoding.UTF8.GetBytes(surelockPassword);
            var encryptedBytes  = sha256.ComputeHash(bytes);
            var newPassword     = Convert.ToBase64String(encryptedBytes);
            retVal              = newPassword.Equals(encryptedPassword);
            return retVal;
        }

        public static double GetDPIHeightFactor()
        {
            double retVal                   = 1;

            try
            {
                if (Application.Current != null && Application.Current.MainWindow != null)
	            {
		            using (var source = new HwndSource(new HwndSourceParameters()))
		            {
			            var matrix          = source.CompositionTarget.TransformToDevice;
			            var dpiWidthFactor  = matrix.M11;
			            var dpiHeightFactor = matrix.M22;
                        retVal              = dpiHeightFactor;

                    }
	            }
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            return retVal;
        }

        public static void SetWorkArea(double height)
        {
            var newMetrics          = new Win32.MinimizedMetrics();
            newMetrics.cbSize       = (uint)Marshal.SizeOf(newMetrics);

            Win32.SystemParametersInfo(Win32.SPI.SPI_GETWORKAREA, newMetrics.cbSize, ref newMetrics, 0);

            //Reducing the height of the work area with height of taskbar
            newMetrics.iVertGap     = (int)height;
            Win32.SystemParametersInfo(Win32.SPI.SPI_SETWORKAREA, newMetrics.cbSize, ref newMetrics, Win32.SPIF.SPIF_UPDATEINIFILE);
            Win32.SystemParametersInfo(Win32.SPI.SPI_GETWORKAREA, newMetrics.cbSize, ref newMetrics, 0);
        }

        public static string GetExportCloudSettings(RootModel baseModel, string CloudId, bool shouldExportActCode)
        {
            string settingsXml                  = ObjectXMLSerializer<RootModel>.Save(baseModel, null, SerializedFormat.String, shouldExportActCode);
            string ExportXml                    = GetExportCloudXml(baseModel,CloudId.Trim(),settingsXml);;
            return ExportXml;
        }

        private static string GetExportCloudXml(RootModel baseModel,string CloudID, string settingsXml)
        {
            string xmlOutput = null;
            try
            {
                License viewModel               = new License(baseModel);
                XmlWriterSettings wSettings     = new XmlWriterSettings();
                wSettings.Indent                = true;
                wSettings.OmitXmlDeclaration    = true;
                MemoryStream ms                 = new MemoryStream();
                viewModel.GetDeviceInformation();
                XmlWriter xw                    = XmlWriter.Create(ms, wSettings);                 // Write Declaration
                xw.WriteStartDocument();
                xw.WriteStartElement("Request");
                xw.WriteElementString("CloudID", CloudID);                                        //Enter cloud id here
                xw.WriteStartElement("SettingsXML");
                xw.WriteCData(settingsXml);
                xw.WriteEndElement();
                xw.WriteElementString("OS", viewModel.OS);
                xw.WriteElementString("Model", viewModel.Model);
                if (baseModel.isLicensed)
                {
                    xw.WriteElementString("ActivationCode", baseModel.ActivationCode);
                }
                else
                {
                    xw.WriteElementString("ActivationCode", string.Empty);
                }
                xw.WriteElementString("IMEI", string.Empty);
                xw.WriteElementString("IMSI", string.Empty);
                xw.WriteElementString("WIFI_MAC", baseModel.MacWifi);
                xw.WriteElementString("BT_MAC", baseModel.MacBluetooth);
                xw.WriteElementString("GUID", baseModel.GUID);
                xw.WriteElementString("Product", ApplicationConstants.SURELOCKWIN8.ToString());
                xw.WriteEndElement();
                xw.WriteEndDocument();
                xw.Flush();
                Byte[] buffer                   = new Byte[ms.Length];
                buffer                          = ms.ToArray();
                xmlOutput                       = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                buffer                          = null;
                ms.Flush();
                ms.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
            return xmlOutput;
        }

        public static string GetImportCloudXml(string CloudID,RootModel baseModel)
        {
            string xmlOutput                    = null;
            try
            {
                License viewModel               = new License(baseModel);
                XmlWriterSettings wSettings     = new XmlWriterSettings();
                wSettings.Indent                = true;
                wSettings.OmitXmlDeclaration    = true;
                MemoryStream ms                 = new MemoryStream();
                viewModel.GetDeviceInformation();
                XmlWriter xw                    = XmlWriter.Create(ms, wSettings);      // Write Declaration
                xw.WriteStartDocument();
                xw.WriteStartElement("Request");
                xw.WriteElementString("CloudID", CloudID);                      //Enter cloud id here
                xw.WriteElementString("OS", viewModel.OS);
                xw.WriteElementString("Model", viewModel.Model);

                if(baseModel.isLicensed)
                {
                    xw.WriteElementString("ActivationCode", baseModel.ActivationCode);
                }
                else
                {
                    xw.WriteElementString("ActivationCode", string.Empty);
                }

                xw.WriteElementString("IMEI", string.Empty);
                xw.WriteElementString("IMSI", string.Empty);
                xw.WriteElementString("WIFI_MAC", baseModel.MacWifi);
                xw.WriteElementString("BT_MAC", baseModel.MacBluetooth);
                xw.WriteElementString("GUID", baseModel.GUID);
                xw.WriteElementString("Product", ApplicationConstants.SURELOCKWIN8.ToString());
                xw.WriteEndElement();
                xw.WriteEndDocument();
                xw.Flush();
                Byte[] buffer                   = new Byte[ms.Length];
                buffer                          = ms.ToArray();
                xmlOutput                       = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                buffer                          = null;
                ms.Flush();
                ms.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
            return xmlOutput;
        }

        /// <summary>
        /// Synchronous method to get the settings file from the cloud
        /// </summary>
        /// <param name="CloudId"></param>
        /// <returns></returns>
        public static string GetSettingsFile(string CloudId,RootModel baseModel)
        {
            string result                       = null;
            try
            {
                HTTP httpObject                 = new HTTP();
                string importXml                = GetImportCloudXml(CloudId, baseModel);
                string serverResponse           = httpObject.HttpPost(ApplicationConstants.CloudGetSettingsUrl, importXml);
                RootModel newRootModelObject    = new RootModel();
                XDocument xmlDoc                = XDocument.Parse(serverResponse);

                foreach (var Result in xmlDoc.Descendants("Result"))
                {
                    //Import Success
                    if (Result.Value.ToString().Equals("True"))
                    {
                        foreach (var SettingsXML in xmlDoc.Descendants("SettingsXML"))
                        {
                            result              = SettingsXML.Value.ToString();
                        }
                    }
                }

                //Check for backward compatibility
                result                          = Utility.Utils.BackwardCompatibilitySettings(result);
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            return result;
        }

        public static string GetShortcutTarget(string filename)
        {
            string result           = null;

            try
            {
                var link            = new ShellLink();
                var file            = link as IPersistFile;

                if(file != null)
                {
                    file.Load(filename, ApplicationConstants.STGM_READ);
                    var sb          = new StringBuilder(ApplicationConstants.MAX_PATH);
                    var data        = new Win32.WIN32_FIND_DATAW();
                    var linkW       = link as IShellLinkW;

                    if(linkW != null)
                    {
                        linkW.GetPath(sb, sb.Capacity, out data, 0);
                        result      = sb.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            return result;
        }

        /// <summary>
        /// Method to get application name from exe path
        /// </summary>
        /// <param name="exePath"></param>
        /// <returns></returns>
        public static string GetApplicationName(string exePath)
        {
            int index       = exePath.LastIndexOf("\\");
            string appName  = exePath.Substring(index + 1);
            index           = appName.LastIndexOf(".");

            if (index != -1)
            {
                appName     = appName.Remove(index);
                return appName;
            }
            return null;
        }

        public static string GetFileAssociation(string fileExt)
        {
            string retVal               = null;

            Win32.AssocStr assocStr     = Win32.AssocStr.Executable;
            uint pcchOut                = 0;
            Win32.AssocQueryString(Win32.AssocF.NoTruncate| Win32.AssocF.Init_IgnoreUnknown, assocStr, fileExt, null, null, ref pcchOut);

            if(pcchOut != 0)
            {
                StringBuilder pszOut    = new StringBuilder((int)pcchOut);
                Win32.AssocQueryString(Win32.AssocF.NoTruncate | Win32.AssocF.Init_IgnoreUnknown, assocStr, fileExt, null, pszOut, ref pcchOut);
                retVal                  = pszOut.ToString();
            }

            return retVal;
        }       

        public static bool FileAssociationPresent(string fileExt)
        {
            bool retVal                 = false;

            if (!string.IsNullOrWhiteSpace(fileExt))
            {
                string fileAssociation  = GetFileAssociation(fileExt);

                if (!string.IsNullOrWhiteSpace(fileAssociation) && !ApplicationConstants.FileAssociationApp.Any(x => Contains(fileAssociation, x, StringComparison.InvariantCultureIgnoreCase)))
                {
                    retVal              = true;
                }
            }

            if (!retVal)
            {
                Logger.LogMessage(string.Format("No file association found for {0}", fileExt));
            }
            return retVal;
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

        public static bool IsLandscape
        {
            get { return System.Windows.SystemParameters.PrimaryScreenHeight < System.Windows.SystemParameters.PrimaryScreenWidth; }
        }

        public static ApplicationSettingsModel GetSingleAppModeApplication(RootModel rm)
        {
            ApplicationSettingsModel asm = null;
            for (int i = 0; i < rm.allowedAppsModel.allowedapps.Count; i++)
            {
                if (!rm.allowedAppsModel.allowedapps[i].HideIconOnHomeScreen)
                {
                    asm = rm.allowedAppsModel.allowedapps[i];
                    break;
                }
            }
            return asm;
        }

        public static WebsiteSettingsModel GetSingleAppModeWebsite(RootModel rm)
        {
            WebsiteSettingsModel wsm    = null;
            for (int i = 0; i < rm.allowedWebsitesModel.allowedWebsites.Count; i++)
            {
                if (!rm.allowedWebsitesModel.allowedWebsites[i].HideURL)
                {
                    wsm                 = rm.allowedWebsitesModel.allowedWebsites[i];
                    break;
                }
            }
            return wsm;
        }

        /// <summary>
        /// Method to check the Allowed applications and Allowed websites if Single App mode is possible
        /// </summary>
        /// <param name="rm"></param>
        /// <returns></returns>
        public static bool isSingleAppMode(RootModel rm)
        {
            int allowedAppsCount    = GetAllowedAppsCount(rm);
            int allowedWebCount     = GetAllowedWebsiteCount(rm);

            bool retVal             = (allowedAppsCount == 1 && allowedWebCount == 0) || 
                                      (allowedWebCount == 1 && allowedAppsCount == 0);
            return retVal;
        }

        private static int GetAllowedAppsCount(RootModel rm)
        {
            int count = 0;
            if(rm != null && rm.allowedAppsModel != null && rm.allowedAppsModel.allowedapps != null)
            {
                rm.allowedAppsModel.allowedapps.ToList().ForEach(x =>
                {
                    if(!x.HideIconOnHomeScreen)
                    {
                        count++;
                    }
                });
            }
            return count;
        }

        private static int GetAllowedWebsiteCount(RootModel rm)
        {
            int count = 0;
            if (rm != null && rm.allowedWebsitesModel != null && rm.allowedWebsitesModel.allowedWebsites != null)
            {
                rm.allowedWebsitesModel.allowedWebsites.ToList().ForEach(x =>
                {
                    if (!x.HideURL)
                    {
                        count++;
                    }
                });
            }
            return count;
        }

        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name) ? Application.Current.Windows.OfType<T>().Any() : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }

        public static void SwitchToSureLock()
        {
            IntPtr surelockWindow = Win32.FindWindow(null, "SureLock");
            if (surelockWindow != IntPtr.Zero)
            {
                Win32.SwitchToThisWindow(surelockWindow, true);
            }
        }

        public static string ConvertStringToHex(string decNumber)
        {
            string retVal       = string.Empty;

            try
            {
                long result     = int.Parse(decNumber);
                string asHex    = result.ToString("x");
                retVal          = "0x" + asHex;
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            return retVal;
        }

        public static decimal ParseHexString(string hexNumber)
        {
            hexNumber   = Regex.Replace(hexNumber, "x", string.Empty, RegexOptions.IgnoreCase);
            long result = long.Parse(hexNumber, NumberStyles.HexNumber);
            return result;
        }

        //Gets the icon from exe path
        public static ImageSource IconFromFilePath(string filePath)
        {
            try
            {
                FileToImageIconConverter some = new FileToImageIconConverter(filePath);
                ImageSource imgSource = some.Icon;
                return imgSource;
            }
            catch (Exception)
            {
                return null;
            }
        }

        //saves surelock settings to an xml file(.settings)
        internal static void Save(RootModel baseModel,string filename="",bool exportActivationCode=false)
        {
            string settingsPath                 = Utils.GetSettingsPath(filename);
            baseModel.ssModel.ProductVersion    = Version.version;
            string xml                          = ObjectXMLSerializer<RootModel>.Save(baseModel, null, SerializedFormat.String, exportActivationCode);
            File.WriteAllText(settingsPath, xml);
        }

        //Gets settings file from the exe path
        internal static string GetSettingsPath(string filename="")
        {
            if (filename != "")
                return filename; 
            else
                return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SureLock.settings"; 
        }

        public static bool IsWindows8()
        {
            System.Version win8Version = new System.Version(6, 2, 9200, 0);
            if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version >= win8Version)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void LogOff()
        {
            Win32.ExitWindowsEx(Win32.ExitWindows.LogOff, Win32.ShutdownReason.MajorOther | Win32.ShutdownReason.MinorOther);
        }

        public static string CalculateTimeoutSummary(int timeout)
        {
            TimeSpan span = TimeSpan.FromSeconds(timeout);
            string TimeoutInterval = "";
            if (span.Hours != 0)
            {
                TimeoutInterval = (int)span.TotalMinutes + " minutes ";
            }
            else
            {
                if (span.Minutes == 1)
                {
                    TimeoutInterval = (int)span.Minutes + " minute ";
                }
                else if (span.Minutes != 0)
                {
                    TimeoutInterval = (int)span.Minutes + " minutes ";
                }
            }
            if (span.Seconds != 0)
            {
                if (span.Seconds == 1)
                {
                    if (span.Minutes != 0)
                    {
                        TimeoutInterval += "and " + span.Seconds + " second";
                    }
                    else
                    {
                        TimeoutInterval += span.Seconds + " second";
                    }
                }
                else if (span.Seconds != 0)
                {
                    if (span.Minutes != 0)
                    {
                        TimeoutInterval += "and " + span.Seconds + " seconds";
                    }
                    else
                    {
                        TimeoutInterval += span.Seconds + " seconds";
                    }
                }
            }
            return TimeoutInterval.TrimEnd();
        }

        public static string[] GetAvailableMouseSchemes()
        {
            try
            {
                using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Control Panel\Cursors\Schemes", RegistryKeyPermissionCheck.ReadSubTree))
                {
                    //if we couldn't open the registry, tell the user and exit
                    if (regKey != null)
                    {
                        string[] values = regKey.GetValueNames();
                        string[] replacedValues = new string[values.Length];

                        for (int i = 0; i < values.Length; i++)
                        {
                            string newValue = values[i];
                            if (values[i].Equals("Windows Aero"))
                            {
                                newValue = values[i].Replace("Windows Aero", "Windows Default");
                            }
                            else if (values[i].Equals("Windows Aero L"))
                            {
                                newValue = values[i].Replace("Windows Aero L", "Windows Default (large)");
                            }
                            else if (values[i].Equals("Windows Aero XL)"))
                            {
                                newValue = values[i].Replace("Windows Aero XL)", "Windows Default (extra large)");
                            }
                            replacedValues[i] = newValue;
                        }
                        return replacedValues;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }
            return null;
        }

        public static string GetModuleName(string name)
        {
            string retVal           = null;

            try
            {
                if (!string.IsNullOrWhiteSpace(name))
                {
                    //Get the target application for shortcuts
                    if (name.EndsWith(".lnk"))
                    {
                        name        = GetShortcutTarget(name);
                    }

                    var nameParts   = name.Split('\\');
                    retVal          = nameParts.GetValue(nameParts.Length - 1).ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            return retVal;
        }

        public static string[] GetMouseSchemePath(string cursor)
        {
            string[] cursorPaths = null;
            try
            {
                using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Control Panel\Cursors\Schemes", RegistryKeyPermissionCheck.ReadSubTree))
                {
                    //if we couldn't open the registry, tell the user and exit
                    if (regKey != null)
                    {
                        string newValue = cursor;
                        if (cursor.Equals("Windows Default"))
                        {
                            newValue = cursor.Replace("Windows Default", "Windows Aero");
                        }
                        else if (cursor.Equals("Windows Default (large)"))
                        {
                            newValue = cursor.Replace("Windows Default (large)", "Windows Aero L");
                        }
                        else if (cursor.Equals("Windows Default (extra large)"))
                        {
                            newValue = cursor.Replace("Windows Default (extra large)", "Windows Aero XL)");
                        }
                        object value = regKey.GetValue(newValue, "");

                        //the paths are seperated by commas, split up the string
                        if(value != null)
                        {
                            cursorPaths = value.ToString().Split(new char[] { ',' });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }
            return cursorPaths;
        }

        public static ObservableCollection<string> GetColors()
        {
            Type colorType      = typeof(System.Drawing.Color);
            var propInfoList    = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);
            var fontColors      = new ObservableCollection<string>();

            for(int i = 0 ; i < propInfoList.Length ; i++)
            {
                var property    = propInfoList[i];

                if(property!= null && !string.IsNullOrWhiteSpace(property.Name))
                {
                    fontColors.Add(property.Name);
                }
            }

            return fontColors;
        }

        public static void SetMouseScheme(string cursor, string[] cursorPaths)
        {
            try
            {
                //this is the order than the paths appear in the comma seperated path
                string[] pathKeys = new string[] { "Arrow", "Help", "AppStarting", "Wait", "Crosshair", "IBeam", "NWPen", "No", "SizeNS", "SizeWE", "SizeNWSE", "SizeNESW", "SizeAll", "UpArrow", "Hand" };

                //open the registry and set the used cursor values to the new ones we found
                using (RegistryKey regKey = Registry.CurrentUser.CreateSubKey(@"Control Panel\Cursors", RegistryKeyPermissionCheck.ReadWriteSubTree))
                {
                    //if we couldn't open the registry, tell the user and exit
                    if (regKey != null)
                    {
                        //set each path
                        for (int i = 0; i < pathKeys.Length; i++)
                        {
                            var path = pathKeys[i];
                            var curPath = cursorPaths[i];
                            regKey.SetValue(path, curPath, RegistryValueKind.String);
                        }
                        regKey.SetValue("", cursor, RegistryValueKind.String);
                    }
                }
                uint SPI_SETCURSORS = 0x0057;
                uint value = 0;
                //tell windows we changed the cursor so it gets updated
                Win32.SystemParametersInfo(SPI_SETCURSORS, 0, ref value, 0);
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }
        }
    }

    //Class to get the icon from exe path
    #region FileToImageIconConverter
    public class FileToImageIconConverter
    {
        private string filePath;
        private ImageSource icon;

        public string FilePath { get { return filePath; } }

        public ImageSource Icon
        {
            get
            {
                if (icon == null && File.Exists(FilePath))
                {
                    using (System.Drawing.Icon sysicon = System.Drawing.Icon.ExtractAssociatedIcon(FilePath))
                    {
                        icon = Imaging.CreateBitmapSourceFromHIcon(
                                  sysicon.Handle,
                                  Int32Rect.Empty,
                                  System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                    }
                }

                return icon;
            }
        }

        public FileToImageIconConverter(string filePath)
        {
            this.filePath = filePath;
        }
    }
    #endregion
}
