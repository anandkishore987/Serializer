﻿using System;
using System.Linq;
using System.ComponentModel;
using SureLockWin8.Model;
using System.IO;
using System.Windows;
using Utility.Model;
using Utility.Common;
using System.Windows.Input;

namespace SureLockWin8.Interfaces
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class ViewModelBase : IChangeTracking, INotifyPropertyChanged, IDisposable
    {
        public static bool launchMainMenu;
        public static bool closeMainMenu;
        public static bool warningMessage;
        public delegate void CompletedEventHandler(object sender, bool result, string reason, RootModel rm = null);
        public delegate void WlanNotificationEventHandler(Wlan.WlanNotificationData notifyData);
        public delegate void WlanConnectionNotificationEventHandler(Wlan.WlanNotificationData notifyData, Wlan.WlanConnectionNotificationData connNotifyData);
        public delegate void WlanReasonNotificationEventHandler(Wlan.WlanNotificationData notifyData, Wlan.WlanReasonCode reasonCode);
        public delegate void MyEventHandler(RootModel rm);
        public delegate void RefreshActionEventHandler(int action);
        public delegate void ShowMessageEvent(string message, string title);
        public delegate void ActivationCodeEventHandler(string activationCode);
        internal RootModel baseModel;        

        /// <summary>
        /// Method to check if imported settings is valid
        /// </summary>
        /// <param name="importedRootModel"></param>
        /// <returns></returns>
        public bool IsSettingsValid(RootModel importedRootModel)
        {

            RootModel comparisonRootModel   = new RootModel();
            bool retVal                     = false;

            if (baseModel.isLicensed)
            {
                retVal                      = true;
            }
            else
            {

                //Allowed app count should be <= 2
                if ((importedRootModel.allowedAppsModel.allowedapps.Count <= 2)

                    &&

                    //Allowed website count should be <= 2
                    (importedRootModel.allowedWebsitesModel.allowedWebsites.Count <= 2)

                    &&

                    //Wallpaper should be trial wallpaper
                    (string.Equals(importedRootModel.ssModel.Wallpaper, comparisonRootModel.ssModel.Wallpaper)

                    &&

                    //Wallpaper size should be same as trial size
                    (importedRootModel.ssModel.wallSize == comparisonRootModel.ssModel.wallSize)

                    &&

                    //Password should be default password 0000
                    (string.Equals(importedRootModel.ssModel.Password, comparisonRootModel.ssModel.Password))))
                {
                    retVal                  = true;
                }
            }

            return retVal;
        }

        public void CloseWindow(EventHandler<EventArgs> RequestClose)
        {
            if (RequestClose != null)
            {
                RequestClose(this, null);
            }
        }

        #region Properties

        internal bool ShouldExportActCode
        {
            get
            {
                bool retVal = baseModel != null && baseModel.isLicensed && baseModel.ExportActivationCode && !string.IsNullOrWhiteSpace(baseModel.ActivationCode);
                return retVal;
            }
        }

        internal int AllowedAppsCount
        {
            get
            {
                if (baseModel.isLicensed || baseModel.allowedAppsModel.allowedapps.Count < 2)
                {
                    return baseModel.allowedAppsModel.allowedapps.Count;
                }
                else
                {
                    return 2;
                }
            }
        }

        internal int AllowedWebCount
        {
            get
            {
                if (baseModel.isLicensed || baseModel.allowedWebsitesModel.allowedWebsites.Count < 2)
                {
                    return baseModel.allowedWebsitesModel.allowedWebsites.Count;
                }
                else
                {
                    return 2;
                }
            }
        }

        public bool CanAddApplication
        {
            get { return baseModel.allowedAppsModel.allowedapps.Count < 2 || baseModel.isLicensed; }
        }

        public bool CanAddWebsite
        {
            get { return baseModel.allowedWebsitesModel.allowedWebsites.Count < 2 || baseModel.isLicensed; }
        }

        public bool SingleAppMode
        {
            get 
            {   
                return baseModel.singleAppModel.SingleAppMode && Utils.Utils.isSingleAppMode(baseModel) 
                    
                && 

                ((SingleAppModeApplication != null && Utils.Utils.GetApplicationName(SingleAppModeApplication.ExePath) != null && CheckAssociatedApps())
                
                ||

                SingleAppModeWebsite != null);
            }
        }

        public WebsiteSettingsModel SingleAppModeWebsite
        {
            get
            {
                return Utils.Utils.GetSingleAppModeWebsite(baseModel);
            }
        }

        public ApplicationSettingsModel SingleAppModeApplication
        {
            get
            {
                return Utils.Utils.GetSingleAppModeApplication(baseModel);
            }
        }

        #endregion

        #region Methods

        public void ShowTrialError(bool isWebsite = false)
        {
            if(isWebsite)
            {
                //MessageBox.Show(ApplicationConstants.AllowedWebTrialMessage, ApplicationConstants.SureLockTrialVersion, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                //MessageBox.Show(ApplicationConstants.AllowedAppTrialMessage, ApplicationConstants.SureLockTrialVersion, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CheckAssociatedApps()
        {
            bool retVal = false;

            if (SingleAppModeApplication != null && !string.IsNullOrWhiteSpace(SingleAppModeApplication.ExePath))
            {
                string extension            = Path.GetExtension(SingleAppModeApplication.ExePath);

                if (!string.IsNullOrWhiteSpace(extension) && File.Exists(SingleAppModeApplication.ExePath.Trim()))
                {
                    if (extension.Equals(".exe", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }

                    string targetApplication = null;

                    //Shortcut files
                    if (extension.Equals(".lnk", StringComparison.InvariantCultureIgnoreCase))
                    {
                        targetApplication   = Utils.Utils.GetShortcutTarget(SingleAppModeApplication.ExePath);

                        if (!string.IsNullOrWhiteSpace(targetApplication))
                        {
                            extension       = Path.GetExtension(targetApplication);
                        }
                    }

                    bool fileAssociation    = Utils.Utils.FileAssociationPresent(extension);
                    string associatedApp    = Utils.Utils.GetFileAssociation(extension);
                    string application      = null;

                    //Get the application
                    if (!string.IsNullOrWhiteSpace(associatedApp) && associatedApp.EndsWith(".exe", StringComparison.InvariantCultureIgnoreCase))
                    {
                        application         = associatedApp;
                    }
                    else if(!string.IsNullOrWhiteSpace(targetApplication))
                    {
                        application         = targetApplication;
                    }

                    //If application present, check if its present in Allowed Apps
                    if(!string.IsNullOrWhiteSpace(application))
                    {
                        retVal              = IsAssociatedAppPresent(application, fileAssociation);
                    }
                }
            }

            return retVal;
        }

        private bool IsAssociatedAppPresent(string application, bool fileAssociation)
        {
            bool retVal = false;

            if (!string.IsNullOrWhiteSpace(application))
            {
                bool appAllowed     = baseModel.allowedAppsModel.allowedapps.Any(x => x.ExePath.Equals(application, StringComparison.InvariantCultureIgnoreCase));
                retVal              = appAllowed && fileAssociation;
            }

            return retVal;
        }

        #endregion

        #region Constructor

        protected ViewModelBase()
        {

        }

        #endregion // Constructor

        protected bool SetProperty<T>(string name, ref T oldValue, T newValue) where T : System.IComparable<T>
        {
            if (oldValue == null || oldValue.CompareTo(newValue) != 0)
            {
                oldValue = newValue;
                if (PropertyChanged != null)
                    PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
                IsChanged = true;
                return true;
            }
            return false;
        }

        //======================================================== 
        //  IChangeTracking Implementation 
        //======================================================== 
        #region IsChanged
        /// <summary> 
        /// Gets the object's changed status. 
        /// </summary> 
        /// <value> 
        /// <see langword="true"/> if the object’s content has changed since the last call to <see cref="AcceptChanges()"/>; otherwise, <see langword="false"/>.  
        /// The initial value is <see langword="false"/>. 
        /// </value> 
        public bool IsChanged
        {
            get
            {
                lock (_notifyingObjectIsChangedSyncRoot)
                {
                    return _notifyingObjectIsChanged;
                }
            }

            protected set
            {
                lock (_notifyingObjectIsChangedSyncRoot)
                {
                    if (!Boolean.Equals(_notifyingObjectIsChanged, value))
                    {
                        _notifyingObjectIsChanged = value;

                        this.OnPropertyChanged("IsChanged");
                    }
                }
            }
        }
        private bool _notifyingObjectIsChanged;
        private readonly object _notifyingObjectIsChangedSyncRoot = new Object();

        #endregion

        #region AcceptChanges()
        /// <summary> 
        /// Resets the object’s state to unchanged by accepting the modifications. 
        /// </summary> 
        public void AcceptChanges()
        {
            this.IsChanged = false;
        }
        #endregion

        //======================================================== 
        //  INotifyPropertyChanged Implementation 
        //======================================================== 
        #region PropertyChanged
        /// <summary> 
        /// Occurs when a property value changes. 
        /// </summary> 
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region OnPropertyChanged(PropertyChangedEventArgs e)
        /// <summary> 
        /// Raises the <see cref="INotifyPropertyChanged.PropertyChanged"/> event. 
        /// </summary> 
        /// <param name="e">A <see cref="PropertyChangedEventArgs"/> that provides data for the event.</param> 
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region OnPropertyChanged(string propertyName)
        /// <summary> 
        /// Raises the <see cref="INotifyPropertyChanged.PropertyChanged"/> event for the specified <paramref name="propertyName"/>. 
        /// </summary> 
        /// <param name="propertyName">The <see cref="MemberInfo.Name"/> of the property whose value has changed.</param> 
        protected void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region OnPropertyChanged(params string[] propertyNames)
        /// <summary> 
        /// Raises the <see cref="INotifyPropertyChanged.PropertyChanged"/> event for the specified <paramref name="propertyNames"/>. 
        /// </summary> 
        /// <param name="propertyNames">An <see cref="Array"/> of <see cref="String"/> objects that contains the names of the properties whose values have changed.</param> 
        /// <exception cref="ArgumentNullException">The <paramref name="propertyNames"/> is a <see langword="null"/> reference (Nothing in Visual Basic).</exception> 
        protected void OnPropertyChanged(params string[] propertyNames)
        {
            if (propertyNames == null)
            {
                throw new ArgumentNullException("propertyNames");
            }

            foreach (var propertyName in propertyNames)
            {
                this.OnPropertyChanged(propertyName);
            }
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();
        }

        /// <summary>
        /// Child classes can override this method to perform 
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose()
        {
        }

        #endregion // IDisposable Members

        //RelayCommand _launchKeyboard;
        //public ICommand LaunchKeyboard
        //{
        //    get
        //    {
        //        if (_launchKeyboard == null)
        //            _launchKeyboard = new RelayCommand(param => OnLaunchKeyboard());

        //        return _launchKeyboard;
        //    }
        //}

        private void OnLaunchKeyboard()
        {
            //Taskbar.LaunchKeyboard(baseModel);
        }
    }
}

