﻿using Microsoft.Win32;
using SureLockWin8.Common;
using SureLockWin8.Model;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Utility;
using Utility.Common;
using Utility.Interfaces;
using Utility.Model;

namespace SureLockWin8.Utils
{
    static class Utils
    {
        public static string GetImportCloudXml(string CloudID, RootModel baseModel)
        {
            string xmlOutput = null;
            try
            {
                License viewModel = new License(baseModel);
                XmlWriterSettings wSettings = new XmlWriterSettings();
                wSettings.Indent = true;
                wSettings.OmitXmlDeclaration = true;
                MemoryStream ms = new MemoryStream();
                viewModel.GetDeviceInformation();
                XmlWriter xw = XmlWriter.Create(ms, wSettings);      // Write Declaration
                xw.WriteStartDocument();
                xw.WriteStartElement("Request");
                xw.WriteElementString("CloudID", CloudID);                      //Enter cloud id here
                xw.WriteElementString("OS", viewModel.OS);
                xw.WriteElementString("Model", viewModel.Model);

                if (baseModel.isLicensed)
                {
                    xw.WriteElementString("ActivationCode", baseModel.ActivationCode);
                }
                else
                {
                    xw.WriteElementString("ActivationCode", string.Empty);
                }

                xw.WriteElementString("IMEI", string.Empty);
                xw.WriteElementString("IMSI", string.Empty);
                xw.WriteElementString("WIFI_MAC", baseModel.MacWifi);
                xw.WriteElementString("BT_MAC", baseModel.MacBluetooth);
                xw.WriteElementString("GUID", baseModel.GUID);
                xw.WriteElementString("Product", ApplicationConstants.SURELOCKWIN8.ToString());
                xw.WriteEndElement();
                xw.WriteEndDocument();
                xw.Flush();
                Byte[] buffer = new Byte[ms.Length];
                buffer = ms.ToArray();
                xmlOutput = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                buffer = null;
                ms.Flush();
                ms.Close();
            }
            catch (Exception )
            {
            }
            return xmlOutput;
        }

        public static string GetShortcutTarget(string filename)
        {
            string result = null;

            try
            {
                var link = new ShellLink();
                var file = link as IPersistFile;

                if (file != null)
                {
                    file.Load(filename, ApplicationConstants.STGM_READ);
                    var sb = new StringBuilder(ApplicationConstants.MAX_PATH);
                    var data = new Win32.WIN32_FIND_DATAW();
                    var linkW = link as IShellLinkW;

                    if (linkW != null)
                    {
                        linkW.GetPath(sb, sb.Capacity, out data, 0);
                        result = sb.ToString();
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static string GetApplicationName(string exePath)
        {
            int index = exePath.LastIndexOf("\\");
            string appName = exePath.Substring(index + 1);
            index = appName.LastIndexOf(".");

            if (index != -1)
            {
                appName = appName.Remove(index);
                return appName;
            }
            return null;
        }

        public static string GetFileAssociation(string fileExt)
        {
            string retVal = null;

            Win32.AssocStr assocStr = Win32.AssocStr.Executable;
            uint pcchOut = 0;
            Win32.AssocQueryString(Win32.AssocF.NoTruncate | Win32.AssocF.Init_IgnoreUnknown, assocStr, fileExt, null, null, ref pcchOut);

            if (pcchOut != 0)
            {
                StringBuilder pszOut = new StringBuilder((int)pcchOut);
                Win32.AssocQueryString(Win32.AssocF.NoTruncate | Win32.AssocF.Init_IgnoreUnknown, assocStr, fileExt, null, pszOut, ref pcchOut);
                retVal = pszOut.ToString();
            }

            return retVal;
        }

        public static bool FileAssociationPresent(string fileExt)
        {
            bool retVal = false;

            if (!string.IsNullOrWhiteSpace(fileExt))
            {
                string fileAssociation = GetFileAssociation(fileExt);

                if (!string.IsNullOrWhiteSpace(fileAssociation) && !ApplicationConstants.FileAssociationApp.Any(x => Contains(fileAssociation, x, StringComparison.InvariantCultureIgnoreCase)))
                {
                    retVal = true;
                }
            }

            if (!retVal)
            {
            }
            return retVal;
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

        public static ApplicationSettingsModel GetSingleAppModeApplication(RootModel rm)
        {
            ApplicationSettingsModel asm = null;
            for (int i = 0; i < rm.allowedAppsModel.allowedapps.Count; i++)
            {
                if (!rm.allowedAppsModel.allowedapps[i].HideIconOnHomeScreen)
                {
                    asm = rm.allowedAppsModel.allowedapps[i];
                    break;
                }
            }
            return asm;
        }

        public static WebsiteSettingsModel GetSingleAppModeWebsite(RootModel rm)
        {
            WebsiteSettingsModel wsm = null;
            for (int i = 0; i < rm.allowedWebsitesModel.allowedWebsites.Count; i++)
            {
                if (!rm.allowedWebsitesModel.allowedWebsites[i].HideURL)
                {
                    wsm = rm.allowedWebsitesModel.allowedWebsites[i];
                    break;
                }
            }
            return wsm;
        }

        /// <summary>
        /// Method to check the Allowed applications and Allowed websites if Single App mode is possible
        /// </summary>
        /// <param name="rm"></param>
        /// <returns></returns>
        public static bool isSingleAppMode(RootModel rm)
        {
            int allowedAppsCount = GetAllowedAppsCount(rm);
            int allowedWebCount = GetAllowedWebsiteCount(rm);

            bool retVal = (allowedAppsCount == 1 && allowedWebCount == 0) ||
                                      (allowedWebCount == 1 && allowedAppsCount == 0);
            return retVal;
        }

        private static int GetAllowedAppsCount(RootModel rm)
        {
            int count = 0;
            if (rm != null && rm.allowedAppsModel != null && rm.allowedAppsModel.allowedapps != null)
            {
                rm.allowedAppsModel.allowedapps.ToList().ForEach(x =>
                {
                    if (!x.HideIconOnHomeScreen)
                    {
                        count++;
                    }
                });
            }
            return count;
        }

        private static int GetAllowedWebsiteCount(RootModel rm)
        {
            int count = 0;
            if (rm != null && rm.allowedWebsitesModel != null && rm.allowedWebsitesModel.allowedWebsites != null)
            {
                rm.allowedWebsitesModel.allowedWebsites.ToList().ForEach(x =>
                {
                    if (!x.HideURL)
                    {
                        count++;
                    }
                });
            }
            return count;
        }

        //Gets the icon from exe path
        //public static ImageSource IconFromFilePath(string filePath)
        //{
        //    try
        //    {
        //        FileToImageIconConverter some = new FileToImageIconConverter(filePath);
        //        ImageSource imgSource = some.Icon;
        //        return imgSource;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //Class to get the icon from exe path
    }
}
