﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Utility.Common
{
    public class Win32
    {
        #region Delegates

        public delegate IntPtr LowLevelProc(int nCode, IntPtr wParam, IntPtr lParam);
        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        #endregion

        #region Struct & Enums       

        /// <summary>
        /// Passed to <see cref="GetTokenInformation"/> to specify what
        /// information about the token to return.
        /// </summary>
        public enum TokenInformationClass
        {
            TokenUser = 1,
            TokenGroups,
            TokenPrivileges,
            TokenOwner,
            TokenPrimaryGroup,
            TokenDefaultDacl,
            TokenSource,
            TokenType,
            TokenImpersonationLevel,
            TokenStatistics,
            TokenRestrictedSids,
            TokenSessionId,
            TokenGroupsAndPrivileges,
            TokenSessionReference,
            TokenSandBoxInert,
            TokenAuditPolicy,
            TokenOrigin,
            TokenElevationType,
            TokenLinkedToken,
            TokenElevation,
            TokenHasRestrictions,
            TokenAccessInformation,
            TokenVirtualizationAllowed,
            TokenVirtualizationEnabled,
            TokenIntegrityLevel,
            TokenUiAccess,
            TokenMandatoryPolicy,
            TokenLogonSid,
            MaxTokenInfoClass
        }
        /// <summary>
        /// The elevation type for a user token.
        /// </summary>
        public enum TokenElevationType
        {
            TokenElevationTypeDefault = 1,
            TokenElevationTypeFull,
            TokenElevationTypeLimited
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SHELLTRAYDATA
        {
            public int dwHz;
            public NIM dwMessage;
            public NOTIFYICONDATA nid;
        }

        public enum WtsInfoClass
        {
            WTSInitialProgram,
            WTSApplicationName,
            WTSWorkingDirectory,
            WTSOEMId,
            WTSSessionId,
            WTSUserName,
            WTSWinStationName,
            WTSDomainName,
            WTSConnectState,
            WTSClientBuildNumber,
            WTSClientName,
            WTSClientDirectory,
            WTSClientProductId,
            WTSClientHardwareId,
            WTSClientAddress,
            WTSClientDisplay,
            WTSClientProtocolType,
            WTSIdleTime,
            WTSLogonTime,
            WTSIncomingBytes,
            WTSOutgoingBytes,
            WTSIncomingFrames,
            WTSOutgoingFrames,
            WTSClientInfo,
            WTSSessionInfo,
        }

        [Flags]
        public enum NIF : uint
        {
            /// <summary>
            /// The message ID is set.
            /// </summary>
            NIF_MESSAGE = 0x01,

            /// <summary>
            /// The notification icon is set.
            /// </summary>
            NIF_ICON = 0x02,

            /// <summary>
            /// The tooltip is set.
            /// </summary>
            NIF_TIP = 0x04,

            /// <summary>
            /// State information (<see cref="NIS"/>) is set. This
            /// applies to both <see cref="NOTIFYICONDATA.dwState"/> and
            /// <see cref="NOTIFYICONDATA.dwStateMask"/>.
            /// </summary>
            NIF_STATE = 0x08,

            /// <summary>
            /// The ballon ToolTip is set. Accordingly, the following
            /// members are set: <see cref="NOTIFYICONDATA.BalloonText"/>,
            /// <see cref="NOTIFYICONDATA.szInfoTitle"/>, <see cref="NOTIFYICONDATA.dwInfoFlags"/>,
            /// and <see cref="NOTIFYICONDATA.uTimeout"/>.
            /// </summary>
            NIF_INFO = 0x10,

            /// <summary>
            /// public identifier is set. Reserved, thus commented out.
            /// </summary>
            NIF_GUID = 0x20,

            /// <summary>
            /// Windows Vista (Shell32.dll version 6.0.6) and later. If the ToolTip
            /// cannot be displayed immediately, discard it.<br/>
            /// Use this flag for ToolTips that represent real-time information which
            /// would be meaningless or misleading if displayed at a later time.
            /// For example, a message that states "Your telephone is ringing."<br/>
            /// This modifies and must be combined with the <see cref="NIF_INFO"/> flag.
            /// </summary>
            NIF_REALTIME = 0x40,

            /// <summary>
            /// Windows Vista (Shell32.dll version 6.0.6) and later.
            /// Use the standard ToolTip. Normally, when uVersion is set
            /// to NOTIFYICON_VERSION_4, the standard ToolTip is replaced
            /// by the application-drawn pop-up user interface (UI).
            /// If the application wants to show the standard tooltip
            /// in that case, regardless of whether the on-hover UI is showing,
            /// it can specify NIF_SHOWTIP to indicate the standard tooltip
            /// should still be shown.<br/>
            /// Note that the NIF_SHOWTIP flag is effective until the next call
            /// to Shell_NotifyIcon.
            /// </summary>
            NIF_SHOWTIP     = 0x80
        }

        [Flags]
        public enum NIS : uint
        {
            /// <summary>
            /// The icon is visible.
            /// </summary>
            Visible         = 0x00,
            /// <summary>
            /// Hide the icon.
            /// </summary>
            NIS_HIDDEN      = 0x01,

            /// <summary>
            /// The icon is shared - currently not supported, thus commented out.
            /// </summary>
            NIS_SHAREDICON  = 0x02
        }

        // this is a 64-bit structure, when running in 64-bit mode, but should be 32!!!
        /// <summary>
        /// A struct that is submitted in order to configure
        /// the taskbar icon. Provides various members that
        /// can be configured partially, according to the
        /// values of the <see cref="NIF"/>
        /// that were defined.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct NOTIFYICONDATA
        {
            /// <summary>
            /// Size of this structure, in bytes.
            /// </summary>
            public int cbSize;

            /// <summary>
            /// Handle to the window that receives notification messages associated with an icon in the
            /// taskbar status area. The Shell uses hWnd and uID to identify which icon to operate on
            /// when Shell_NotifyIcon is invoked.
            /// </summary>
            public UInt32 hwnd;

            /// <summary>
            /// Application-defined identifier of the taskbar icon. The Shell uses hWnd and uID to identify
            /// which icon to operate on when Shell_NotifyIcon is invoked. You can have multiple icons
            /// associated with a single hWnd by assigning each a different uID. This feature, however
            /// is currently not used.
            /// </summary>
            public uint uID;

            /// <summary>
            /// Flags that indicate which of the other members contain valid data. This member can be
            /// a combination of the NIF_XXX constants.
            /// </summary>
            public NIF uFlags;

            /// <summary>
            /// Application-defined message identifier. The system uses this identifier to send
            /// notifications to the window identified in hWnd.
            /// </summary>
            public uint uCallbackMessage;

            /// <summary>
            /// A handle to the icon that should be displayed. Just
            /// <see cref="Icon.Handle"/>.
            /// </summary>
            //public IntPtr hIcon;
            public uint hIcon;

            /// <summary>
            /// String with the text for a standard ToolTip. It can have a maximum of 64 characters including
            /// the terminating NULL. For Version 5.0 and later, szTip can have a maximum of
            /// 128 characters, including the terminating NULL.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szTip;

            /// <summary>
            /// State of the icon. Remember to also set the <see cref="dwStateMask"/>.
            /// </summary>
            public NIS dwState;

            /// <summary>
            /// A value that specifies which bits of the state member are retrieved or modified.
            /// For example, setting this member to <see cref="NIS.NIS_HIDDEN"/>
            /// causes only the item's hidden
            /// state to be retrieved.
            /// </summary>
            public NIS dwStateMask;

            /// <summary>
            /// String with the text for a balloon ToolTip. It can have a maximum of 255 characters.
            /// To remove the ToolTip, set the NIF_INFO flag in uFlags and set szInfo to an empty string.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szInfo;

            /// <summary>
            /// Mainly used to set the version when <see cref="WinApi.Shell_NotifyIcon"/> is invoked
            /// with <see cref="NotifyCommand.SetVersion"/>. However, for legacy operations,
            /// the same member is also used to set timouts for balloon ToolTips.
            /// </summary>
            public uint uTimeout;

            /// <summary>
            /// String containing a title for a balloon ToolTip. This title appears in boldface
            /// above the text. It can have a maximum of 63 characters.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szInfoTitle;

            /// <summary>
            /// Adds an icon to a balloon ToolTip, which is placed to the left of the title. If the
            /// <see cref="szInfoTitle"/> member is zero-length, the icon is not shown.
            /// </summary>
            public uint dwInfoFlags;

            /// <summary>
            /// Windows XP (Shell32.dll version 6.0) and later.<br/>
            /// - Windows 7 and later: A registered GUID that identifies the icon.
            /// 	This value overrides uID and is the recommended method of identifying the icon.<br/>
            /// - Windows XP through Windows Vista: Reserved.
            /// </summary>
            public Guid guidItem;

            /// <summary>
            /// Windows Vista (Shell32.dll version 6.0.6) and later. The handle of a customized
            /// balloon icon provided by the application that should be used independently
            /// of the tray icon. If this member is non-NULL and the <see cref="Interop.BalloonFlags.User"/>
            /// flag is set, this icon is used as the balloon icon.<br/>
            /// If this member is NULL, the legacy behavior is carried out.
            /// </summary>
            public IntPtr hBalloonIcon;
        }

        public enum NIM
        {
            NIM_ADD         = 0,
            NIM_MODIFY      = 1,
            NIM_DELETE      = 2,
            NIM_SETFOCUS    = 3,
            NIM_SETVERSION  = 4
        }

        [Flags]
        public enum ExitWindows : uint
        {
            LogOff      = 0x00,
            ShutDown    = 0x01,
            Reboot      = 0x02,
            PowerOff    = 0x08,
            RestartApps = 0x40,
            Force       = 0x04,
            ForceIfHung = 0x10,
        }

        [Flags]
        public enum ShutdownReason : uint
        {
            MajorApplication            = 0x00040000,
            MajorHardware               = 0x00010000,
            MajorLegacyApi              = 0x00070000,
            MajorOperatingSystem        = 0x00020000,
            MajorOther                  = 0x00000000,
            MajorPower                  = 0x00060000,
            MajorSoftware               = 0x00030000,
            MajorSystem                 = 0x00050000,
            MinorBlueScreen             = 0x0000000F,
            MinorCordUnplugged          = 0x0000000b,
            MinorDisk                   = 0x00000007,
            MinorEnvironment            = 0x0000000c,
            MinorHardwareDriver         = 0x0000000d,
            MinorHotfix                 = 0x00000011,
            MinorHung                   = 0x00000005,
            MinorInstallation           = 0x00000002,
            MinorMaintenance            = 0x00000001,
            MinorMMC                    = 0x00000019,
            MinorNetworkConnectivity    = 0x00000014,
            MinorNetworkCard            = 0x00000009,
            MinorOther                  = 0x00000000,
            MinorOtherDriver            = 0x0000000e,
            MinorPowerSupply            = 0x0000000a,
            MinorProcessor              = 0x00000008,
            MinorReconfig               = 0x00000004,
            MinorSecurity               = 0x00000013,
            MinorSecurityFix            = 0x00000012,
            MinorSecurityFixUninstall   = 0x00000018,
            MinorServicePack            = 0x00000010,
            MinorServicePackUninstall   = 0x00000016,
            MinorTermSrv                = 0x00000020,
            MinorUnstable               = 0x00000006,
            MinorUpgrade                = 0x00000003,
            MinorWMI                    = 0x00000015,
            FlagUserDefined             = 0x40000000,
            FlagPlanned                 = 0x80000000
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct WIN32_FIND_DATAW
        {
            public uint dwFileAttributes;
            public long ftCreationTime;
            public long ftLastAccessTime;
            public long ftLastWriteTime;
            public uint nFileSizeHigh;
            public uint nFileSizeLow;
            public uint dwReserved0;
            public uint dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
            public string cAlternateFileName;
        }

        [Flags()]
        public enum SLGP_FLAGS
        {
            /// <summary>Retrieves the standard short (8.3 format) file name</summary>
            SLGP_SHORTPATH = 0x1,
            /// <summary>Retrieves the Universal Naming Convention (UNC) path name of the file</summary>
            SLGP_UNCPRIORITY = 0x2,
            /// <summary>Retrieves the raw path name. A raw path is something that might not exist and may include environment variables that need to be expanded</summary>
            SLGP_RAWPATH = 0x4
        }

        [Flags()]
        public enum SLR_FLAGS
        {
            /// <summary>
            /// Do not display a dialog box if the link cannot be resolved. When SLR_NO_UI is set,
            /// the high-order word of fFlags can be set to a time-out value that specifies the
            /// maximum amount of time to be spent resolving the link. The function returns if the
            /// link cannot be resolved within the time-out duration. If the high-order word is set
            /// to zero, the time-out duration will be set to the default value of 3,000 milliseconds
            /// (3 seconds). To specify a value, set the high word of fFlags to the desired time-out
            /// duration, in milliseconds.
            /// </summary>
            SLR_NO_UI = 0x1,
            /// <summary>Obsolete and no longer used</summary>
            SLR_ANY_MATCH = 0x2,
            /// <summary>If the link object has changed, update its path and list of identifiers.
            /// If SLR_UPDATE is set, you do not need to call IPersistFile::IsDirty to determine
            /// whether or not the link object has changed.</summary>
            SLR_UPDATE = 0x4,
            /// <summary>Do not update the link information</summary>
            SLR_NOUPDATE = 0x8,
            /// <summary>Do not execute the search heuristics</summary>
            SLR_NOSEARCH = 0x10,
            /// <summary>Do not use distributed link tracking</summary>
            SLR_NOTRACK = 0x20,
            /// <summary>Disable distributed link tracking. By default, distributed link tracking tracks
            /// removable media across multiple devices based on the volume name. It also uses the
            /// Universal Naming Convention (UNC) path to track remote file systems whose drive letter
            /// has changed. Setting SLR_NOLINKINFO disables both types of tracking.</summary>
            SLR_NOLINKINFO = 0x40,
            /// <summary>Call the Microsoft Windows Installer</summary>
            SLR_INVOKE_MSI = 0x80
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DOCHOSTUIINFO
        {
            public int cbSize;
            public int dwFlags;
            public int dwDoubleClick;
            public IntPtr dwReserved1;
            public IntPtr dwReserved2;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct COMRECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public struct RECT
        {
            /// <summary> Win32 </summary>
            public int left;

            /// <summary> Win32 </summary>
            public int top;

            /// <summary> Win32 </summary>
            public int right;

            /// <summary> Win32 </summary>
            public int bottom;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MONITORINFO
        {
            /// <summary>
            /// </summary>            
            public int cbSize       = Marshal.SizeOf(typeof(MONITORINFO));

            /// <summary>
            /// </summary>            
            public RECT rcMonitor   = new RECT();

            /// <summary>
            /// </summary>            
            public RECT rcWork      = new RECT();

            /// <summary>
            /// </summary>            
            public int dwFlags = 0;
        }

        public enum FAMILY : uint
        {
            /// <summary>
            /// IPv4
            /// </summary>
            AF_INET = 2,
            /// <summary>
            /// IPv6
            /// </summary>
            AF_INET6 = 23,
            /// <summary>
            /// Unpecified. Includes both IPv4 and IPv4
            /// </summary>
            AF_UNSPEC = 0
        }
        public enum FLAGS : uint
        {
            GAA_FLAG_SKIP_UNICAST = 0x0001,
            GAA_FLAG_SKIP_ANYCAST = 0x0002,
            GAA_FLAG_SKIP_MULTICAST = 0x0004,
            GAA_FLAG_SKIP_DNS_SERVER = 0x0008,
            GAA_FLAG_INCLUDE_PREFIX = 0x0010,
            GAA_FLAG_SKIP_FRIENDLY_NAME = 0x0020
        }

        public enum ERROR : uint
        {
            ERROR_SUCCESS = 0,
            ERROR_NO_DATA = 232,
            ERROR_BUFFER_OVERFLOW = 111,
            ERROR_INVALID_PARAMETER = 87
        }

        public enum IF_OPER_STATUS : uint
        {
            IfOperStatusUp = 1,
            IfOperStatusDown,
            IfOperStatusTesting,
            IfOperStatusUnknown,
            IfOperStatusDormant,
            IfOperStatusNotPresent,
            IfOperStatusLowerLayerDown,
        }

        public enum IF_TYPE : uint
        {
            IF_TYPE_OTHER = 1,   // None of the below
            IF_TYPE_REGULAR_1822 = 2,
            IF_TYPE_HDH_1822 = 3,
            IF_TYPE_DDN_X25 = 4,
            IF_TYPE_RFC877_X25 = 5,
            IF_TYPE_ETHERNET_CSMACD = 6,
            IF_TYPE_IS088023_CSMACD = 7,
            IF_TYPE_ISO88024_TOKENBUS = 8,
            IF_TYPE_ISO88025_TOKENRING = 9,
            IF_TYPE_ISO88026_MAN = 10,
            IF_TYPE_STARLAN = 11,
            IF_TYPE_PROTEON_10MBIT = 12,
            IF_TYPE_PROTEON_80MBIT = 13,
            IF_TYPE_HYPERCHANNEL = 14,
            IF_TYPE_FDDI = 15,
            IF_TYPE_LAP_B = 16,
            IF_TYPE_SDLC = 17,
            IF_TYPE_DS1 = 18,  // DS1-MIB
            IF_TYPE_E1 = 19,  // Obsolete; see DS1-MIB
            IF_TYPE_BASIC_ISDN = 20,
            IF_TYPE_PRIMARY_ISDN = 21,
            IF_TYPE_PROP_POINT2POINT_SERIAL = 22,  // proprietary serial
            IF_TYPE_PPP = 23,
            IF_TYPE_SOFTWARE_LOOPBACK = 24,
            IF_TYPE_EON = 25,  // CLNP over IP
            IF_TYPE_ETHERNET_3MBIT = 26,
            IF_TYPE_NSIP = 27,  // XNS over IP
            IF_TYPE_SLIP = 28,  // Generic Slip
            IF_TYPE_ULTRA = 29,  // ULTRA Technologies
            IF_TYPE_DS3 = 30,  // DS3-MIB
            IF_TYPE_SIP = 31,  // SMDS, coffee
            IF_TYPE_FRAMERELAY = 32,  // DTE only
            IF_TYPE_RS232 = 33,
            IF_TYPE_PARA = 34,  // Parallel port
            IF_TYPE_ARCNET = 35,
            IF_TYPE_ARCNET_PLUS = 36,
            IF_TYPE_ATM = 37,  // ATM cells
            IF_TYPE_MIO_X25 = 38,
            IF_TYPE_SONET = 39,  // SONET or SDH
            IF_TYPE_X25_PLE = 40,
            IF_TYPE_ISO88022_LLC = 41,
            IF_TYPE_LOCALTALK = 42,
            IF_TYPE_SMDS_DXI = 43,
            IF_TYPE_FRAMERELAY_SERVICE = 44,  // FRNETSERV-MIB
            IF_TYPE_V35 = 45,
            IF_TYPE_HSSI = 46,
            IF_TYPE_HIPPI = 47,
            IF_TYPE_MODEM = 48,  // Generic Modem
            IF_TYPE_AAL5 = 49,  // AAL5 over ATM
            IF_TYPE_SONET_PATH = 50,
            IF_TYPE_SONET_VT = 51,
            IF_TYPE_SMDS_ICIP = 52,  // SMDS InterCarrier Interface
            IF_TYPE_PROP_VIRTUAL = 53,  // Proprietary virtual/internal
            IF_TYPE_PROP_MULTIPLEXOR = 54,  // Proprietary multiplexing
            IF_TYPE_IEEE80212 = 55,  // 100BaseVG
            IF_TYPE_FIBRECHANNEL = 56,
            IF_TYPE_HIPPIINTERFACE = 57,
            IF_TYPE_FRAMERELAY_INTERCONNECT = 58,  // Obsolete, use 32 or 44
            IF_TYPE_AFLANE_8023 = 59,  // ATM Emulated LAN for 802.3
            IF_TYPE_AFLANE_8025 = 60,  // ATM Emulated LAN for 802.5
            IF_TYPE_CCTEMUL = 61,  // ATM Emulated circuit
            IF_TYPE_FASTETHER = 62,  // Fast Ethernet (100BaseT)
            IF_TYPE_ISDN = 63,  // ISDN and X.25
            IF_TYPE_V11 = 64,  // CCITT V.11/X.21
            IF_TYPE_V36 = 65,  // CCITT V.36
            IF_TYPE_G703_64K = 66,  // CCITT G703 at 64Kbps
            IF_TYPE_G703_2MB = 67,  // Obsolete; see DS1-MIB
            IF_TYPE_QLLC = 68,  // SNA QLLC
            IF_TYPE_FASTETHER_FX = 69,  // Fast Ethernet (100BaseFX)
            IF_TYPE_CHANNEL = 70,
            IF_TYPE_IEEE80211 = 71,  // Radio spread spectrum
            IF_TYPE_IBM370PARCHAN = 72,  // IBM System 360/370 OEMI Channel
            IF_TYPE_ESCON = 73,  // IBM Enterprise Systems Connection
            IF_TYPE_DLSW = 74,  // Data Link Switching
            IF_TYPE_ISDN_S = 75,  // ISDN S/T interface
            IF_TYPE_ISDN_U = 76,  // ISDN U interface
            IF_TYPE_LAP_D = 77,  // Link Access Protocol D
            IF_TYPE_IPSWITCH = 78,  // IP Switching Objects
            IF_TYPE_RSRB = 79,  // Remote Source Route Bridging
            IF_TYPE_ATM_LOGICAL = 80,  // ATM Logical Port
            IF_TYPE_DS0 = 81,  // Digital Signal Level 0
            IF_TYPE_DS0_BUNDLE = 82,  // Group of ds0s on the same ds1
            IF_TYPE_BSC = 83,  // Bisynchronous Protocol
            IF_TYPE_ASYNC = 84,  // Asynchronous Protocol
            IF_TYPE_CNR = 85,  // Combat Net Radio
            IF_TYPE_ISO88025R_DTR = 86,  // ISO 802.5r DTR
            IF_TYPE_EPLRS = 87,  // Ext Pos Loc Report Sys
            IF_TYPE_ARAP = 88,  // Appletalk Remote Access Protocol
            IF_TYPE_PROP_CNLS = 89,  // Proprietary Connectionless Proto
            IF_TYPE_HOSTPAD = 90,  // CCITT-ITU X.29 PAD Protocol
            IF_TYPE_TERMPAD = 91,  // CCITT-ITU X.3 PAD Facility
            IF_TYPE_FRAMERELAY_MPI = 92,  // Multiproto Interconnect over FR
            IF_TYPE_X213 = 93,  // CCITT-ITU X213
            IF_TYPE_ADSL = 94,  // Asymmetric Digital Subscrbr Loop
            IF_TYPE_RADSL = 95,  // Rate-Adapt Digital Subscrbr Loop
            IF_TYPE_SDSL = 96,  // Symmetric Digital Subscriber Loop
            IF_TYPE_VDSL = 97,  // Very H-Speed Digital Subscrb Loop
            IF_TYPE_ISO88025_CRFPRINT = 98,  // ISO 802.5 CRFP
            IF_TYPE_MYRINET = 99,  // Myricom Myrinet
            IF_TYPE_VOICE_EM = 100,  // Voice recEive and transMit
            IF_TYPE_VOICE_FXO = 101,  // Voice Foreign Exchange Office
            IF_TYPE_VOICE_FXS = 102,  // Voice Foreign Exchange Station
            IF_TYPE_VOICE_ENCAP = 103,  // Voice encapsulation
            IF_TYPE_VOICE_OVERIP = 104,  // Voice over IP encapsulation
            IF_TYPE_ATM_DXI = 105,  // ATM DXI
            IF_TYPE_ATM_FUNI = 106,  // ATM FUNI
            IF_TYPE_ATM_IMA = 107,  // ATM IMA
            IF_TYPE_PPPMULTILINKBUNDLE = 108,  // PPP Multilink Bundle
            IF_TYPE_IPOVER_CDLC = 109,  // IBM ipOverCdlc
            IF_TYPE_IPOVER_CLAW = 110,  // IBM Common Link Access to Workstn
            IF_TYPE_STACKTOSTACK = 111,  // IBM stackToStack
            IF_TYPE_VIRTUALIPADDRESS = 112,  // IBM VIPA
            IF_TYPE_MPC = 113,  // IBM multi-proto channel support
            IF_TYPE_IPOVER_ATM = 114,  // IBM ipOverAtm
            IF_TYPE_ISO88025_FIBER = 115,  // ISO 802.5j Fiber Token Ring
            IF_TYPE_TDLC = 116,  // IBM twinaxial data link control
            IF_TYPE_GIGABITETHERNET = 117,
            IF_TYPE_HDLC = 118,
            IF_TYPE_LAP_F = 119,
            IF_TYPE_V37 = 120,
            IF_TYPE_X25_MLP = 121,  // Multi-Link Protocol
            IF_TYPE_X25_HUNTGROUP = 122,  // X.25 Hunt Group
            IF_TYPE_TRANSPHDLC = 123,
            IF_TYPE_INTERLEAVE = 124,  // Interleave channel
            IF_TYPE_FAST = 125,  // Fast channel
            IF_TYPE_IP = 126,  // IP (for APPN HPR in IP networks)
            IF_TYPE_DOCSCABLE_MACLAYER = 127,  // CATV Mac Layer
            IF_TYPE_DOCSCABLE_DOWNSTREAM = 128,  // CATV Downstream interface
            IF_TYPE_DOCSCABLE_UPSTREAM = 129,  // CATV Upstream interface
            IF_TYPE_A12MPPSWITCH = 130,  // Avalon Parallel Processor
            IF_TYPE_TUNNEL = 131,  // Encapsulation interface
            IF_TYPE_COFFEE = 132,  // Coffee pot
            IF_TYPE_CES = 133,  // Circuit Emulation Service
            IF_TYPE_ATM_SUBINTERFACE = 134,  // ATM Sub Interface
            IF_TYPE_L2_VLAN = 135,  // Layer 2 Virtual LAN using 802.1Q
            IF_TYPE_L3_IPVLAN = 136,  // Layer 3 Virtual LAN using IP
            IF_TYPE_L3_IPXVLAN = 137,  // Layer 3 Virtual LAN using IPX
            IF_TYPE_DIGITALPOWERLINE = 138,  // IP over Power Lines
            IF_TYPE_MEDIAMAILOVERIP = 139,  // Multimedia Mail over IP
            IF_TYPE_DTM = 140,  // Dynamic syncronous Transfer Mode
            IF_TYPE_DCN = 141,  // Data Communications Network
            IF_TYPE_IPFORWARD = 142,  // IP Forwarding Interface
            IF_TYPE_MSDSL = 143,  // Multi-rate Symmetric DSL
            IF_TYPE_IEEE1394 = 144,  // IEEE1394 High Perf Serial Bus
            IF_TYPE_RECEIVE_ONLY = 145 // TV adapter type
        }
        public enum IP_SUFFIX_ORIGIN : uint
        {
            /// IpSuffixOriginOther -> 0
            IpSuffixOriginOther = 0,
            IpSuffixOriginManual,
            IpSuffixOriginWellKnown,
            IpSuffixOriginDhcp,
            IpSuffixOriginLinkLayerAddress,
            IpSuffixOriginRandom,
        }
        public enum IP_PREFIX_ORIGIN : uint
        {
            /// IpPrefixOriginOther -> 0
            IpPrefixOriginOther = 0,
            IpPrefixOriginManual,
            IpPrefixOriginWellKnown,
            IpPrefixOriginDhcp,
            IpPrefixOriginRouterAdvertisement,
        }
        public enum IP_DAD_STATE : uint
        {
            /// IpDadStateInvalid -> 0
            IpDadStateInvalid = 0,
            IpDadStateTentative,
            IpDadStateDuplicate,
            IpDadStateDeprecated,
            IpDadStatePreferred,
        }

        public enum NET_IF_CONNECTION_TYPE : uint
        {
            NET_IF_CONNECTION_DEDICATED = 1,
            NET_IF_CONNECTION_PASSIVE = 2,
            NET_IF_CONNECTION_DEMAND = 3,
            NET_IF_CONNECTION_MAXIMUM = 4
        }

        public enum TUNNEL_TYPE : uint
        {
            TUNNEL_TYPE_NONE = 0,
            TUNNEL_TYPE_OTHER = 1,
            TUNNEL_TYPE_DIRECT = 2,
            TUNNEL_TYPE_6TO4 = 11,
            TUNNEL_TYPE_ISATAP = 13,
            TUNNEL_TYPE_TEREDO = 14,
            TUNNEL_TYPE_IPHTTPS = 15
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GUID
        {
            uint Data1;
            ushort Data2;
            ushort Data3;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            byte[] Data4;
        }

        private const int MAX_ADAPTER_ADDRESS_LENGTH = 8;
        private const int MAX_ADAPTER_NAME_LENGTH = 256;
        private const int MAX_DHCPV6_DUID_LENGTH = 130;

        [StructLayout(LayoutKind.Sequential)]
        public struct SOCKADDR
        {
            /// u_short->unsigned short
            public ushort sa_family;

            /// char[14]
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
            public byte[] sa_data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SOCKET_ADDRESS
        {
            public IntPtr lpSockAddr;
            public int iSockaddrLength;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct IP_ADAPTER_UNICAST_ADDRESS
        {
            public UInt64 Alignment;
            public IntPtr Next;
            public SOCKET_ADDRESS Address;
            public IP_PREFIX_ORIGIN PrefixOrigin;
            public IP_SUFFIX_ORIGIN SuffixOrigin;
            public IP_DAD_STATE DadState;
            public uint ValidLifetime;
            public uint PreferredLifetime;
            public uint LeaseLifetime;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct IP_ADAPTER_ADDRESSES
        {
            public UInt64 Alignment;
            public IntPtr Next;
            public IntPtr AdapterName;
            public IntPtr FirstUnicastAddress;
            public IntPtr FirstAnycastAddress;
            public IntPtr FirstMulticastAddress;
            public IntPtr FirstDnsServerAddress;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string DnsSuffix;
            [System.Runtime.InteropServices.MarshalAs(UnmanagedType.LPWStr)]
            public string Description;
            [System.Runtime.InteropServices.MarshalAs(UnmanagedType.LPWStr)]
            public string FriendlyName;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_ADAPTER_ADDRESS_LENGTH)]
            public byte[] PhysicalAddress;
            public uint PhysicalAddressLength;
            public uint Flags;
            public uint Mtu;
            public IF_TYPE IfType;
            public IF_OPER_STATUS OperStatus;
            uint Ipv6IfIndex;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public uint[] ZoneIndices;
            public IntPtr FirstPrefix;

            // Items added for Vista
            // May need to be removed on Windows versions below Vista to work properly (?)
            public UInt64 TrasmitLinkSpeed;
            public UInt64 ReceiveLinkSpeed;
            public IntPtr FirstWinsServerAddress;
            public IntPtr FirstGatewayAddress;
            public uint Ipv4Metric;
            public uint Ipv6Metric;
            public UInt64 Luid;
            public SOCKET_ADDRESS Dhcpv4Server;
            public uint CompartmentId;
            public GUID NetworkGuid;
            public NET_IF_CONNECTION_TYPE ConnectionType;
            public TUNNEL_TYPE TunnelType;
            public SOCKET_ADDRESS Dhcpv6Server;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_DHCPV6_DUID_LENGTH)]
            public byte[] Dhcpv6ClientDuid;
            public uint Dhcpv6ClientDuidLength;
            public uint Dhcpv6Iaid;
            public uint FirstDnsSuffix;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_VOLUME
        {
            public int dbcv_size;
            public int dbcv_devicetype;
            public int dbcv_reserved;
            public int dbcv_unitmask;
        }

        [Flags]
        public enum AssocF : uint
        {
	        None                    = 0,
	        Init_NoRemapCLSID       = 0x1,
	        Init_ByExeName          = 0x2,
	        Open_ByExeName          = 0x2,
	        Init_DefaultToStar      = 0x4,
	        Init_DefaultToFolder    = 0x8,
	        NoUserSettings          = 0x10,
	        NoTruncate              = 0x20,
	        Verify                  = 0x40,
	        RemapRunDll             = 0x80,
	        NoFixUps                = 0x100,
	        IgnoreBaseClass         = 0x200,
	        Init_IgnoreUnknown      = 0x400,
	        Init_FixedProgId        = 0x800,
	        IsProtocol              = 0x1000,
	        InitForFile             = 0x2000,
        }

        public enum AssocStr
        {
            Command                 = 1,
            Executable,
            FriendlyDocName,
            FriendlyAppName,
            NoOpen,
            ShellNewValue,
            DDECommand,
            DDEIfExec,
            DDEApplication,
            DDETopic
        }

        public enum MessageFilterInfo : uint
        {
            None = 0, AlreadyAllowed = 1, AlreadyDisAllowed = 2, AllowedHigher = 3
        };

        public enum ChangeWindowMessageFilterExAction : uint
        {
            Reset = 0, Allow = 1, DisAllow = 2
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct CHANGEFILTERSTRUCT
        {
            public uint size;
            public MessageFilterInfo info;
        }

        /// <summary>
        /// Window Styles.
        /// The following styles can be specified wherever a window style is required. After the control has been created, these styles cannot be modified, except as noted.
        /// </summary>
        [Flags()]
        public enum WindowStyles : uint
        {
            /// <summary>The window has a thin-line border.</summary>
            WS_BORDER = 0x800000,

            /// <summary>The window has a title bar (includes the WS_BORDER style).</summary>
            WS_CAPTION = 0xc00000,

            /// <summary>The window is a child window. A window with this style cannot have a menu bar. This style cannot be used with the WS_POPUP style.</summary>
            WS_CHILD = 0x40000000,

            /// <summary>Excludes the area occupied by child windows when drawing occurs within the parent window. This style is used when creating the parent window.</summary>
            WS_CLIPCHILDREN = 0x2000000,

            /// <summary>
            /// Clips child windows relative to each other; that is, when a particular child window receives a WM_PAINT message, the WS_CLIPSIBLINGS style clips all other overlapping child windows out of the region of the child window to be updated.
            /// If WS_CLIPSIBLINGS is not specified and child windows overlap, it is possible, when drawing within the client area of a child window, to draw within the client area of a neighboring child window.
            /// </summary>
            WS_CLIPSIBLINGS = 0x4000000,

            /// <summary>The window is initially disabled. A disabled window cannot receive input from the user. To change this after a window has been created, use the EnableWindow function.</summary>
            WS_DISABLED = 0x8000000,

            /// <summary>The window has a border of a style typically used with dialog boxes. A window with this style cannot have a title bar.</summary>
            WS_DLGFRAME = 0x400000,

            /// <summary>
            /// The window is the first control of a group of controls. The group consists of this first control and all controls defined after it, up to the next control with the WS_GROUP style.
            /// The first control in each group usually has the WS_TABSTOP style so that the user can move from group to group. The user can subsequently change the keyboard focus from one control in the group to the next control in the group by using the direction keys.
            /// You can turn this style on and off to change dialog box navigation. To change this style after a window has been created, use the SetWindowLong function.
            /// </summary>
            WS_GROUP = 0x20000,

            /// <summary>The window has a horizontal scroll bar.</summary>
            WS_HSCROLL = 0x100000,

            /// <summary>The window is initially maximized.</summary> 
            WS_MAXIMIZE = 0x1000000,

            /// <summary>The window has a maximize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.</summary> 
            WS_MAXIMIZEBOX = 0x10000,

            /// <summary>The window is initially minimized.</summary>
            WS_MINIMIZE = 0x20000000,

            /// <summary>The window has a minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.</summary>
            WS_MINIMIZEBOX = 0x20000,

            /// <summary>The window is an overlapped window. An overlapped window has a title bar and a border.</summary>
            WS_OVERLAPPED = 0x0,

            /// <summary>The window is an overlapped window.</summary>
            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_SIZEFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,

            /// <summary>The window is a pop-up window. This style cannot be used with the WS_CHILD style.</summary>
            WS_POPUP = 0x80000000u,

            /// <summary>The window is a pop-up window. The WS_CAPTION and WS_POPUPWINDOW styles must be combined to make the window menu visible.</summary>
            WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,

            /// <summary>The window has a sizing border.</summary>
            WS_SIZEFRAME = 0x40000,

            /// <summary>The window has a window menu on its title bar. The WS_CAPTION style must also be specified.</summary>
            WS_SYSMENU = 0x80000,

            /// <summary>
            /// The window is a control that can receive the keyboard focus when the user presses the TAB key.
            /// Pressing the TAB key changes the keyboard focus to the next control with the WS_TABSTOP style.  
            /// You can turn this style on and off to change dialog box navigation. To change this style after a window has been created, use the SetWindowLong function.
            /// For user-created windows and modeless dialogs to work with tab stops, alter the message loop to call the IsDialogMessage function.
            /// </summary>
            WS_TABSTOP = 0x10000,

            /// <summary>The window is initially visible. This style can be turned on and off by using the ShowWindow or SetWindowPos function.</summary>
            WS_VISIBLE = 0x10000000,

            /// <summary>The window has a vertical scroll bar.</summary>
            WS_VSCROLL = 0x200000
        }


        [Flags]
        public enum SendMessageTimeoutFlags : uint
        {
            SMTO_NORMAL             = 0x0,
            SMTO_BLOCK              = 0x1,
            SMTO_ABORTIFHUNG        = 0x2,
            SMTO_NOTIMEOUTIFNOTHUNG = 0x8,
            SMTO_ERRORONEXIT        = 0x20
        }

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public struct WNDCLASS
        {
            public uint style;
            public IntPtr lpfnWndProc;
            public int cbClsExtra;
            public int cbWndExtra;
            public IntPtr hInstance;
            public IntPtr hIcon;
            public IntPtr hCursor;
            public IntPtr hbrBackground;

            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpszMenuName;

            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpszClassName;
        }

        public struct COPYDATASTRUCT1
        {
            public IntPtr dwData;

            public int cbData;

            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        /// The COPYDATASTRUCT structure contains data to be passed to another 
        /// application by the WM_COPYDATA message. 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;       // Specifies data to be passed
            public int cbData;          // Specifies the data size in bytes
            public IntPtr lpData;       // Pointer to data to be passed
        }

        public enum ShellEvents
        {
            HSHELL_WINDOWCREATED            = 1,
            HSHELL_WINDOWDESTROYED          = 2,
            HSHELL_ACTIVATESHELLWINDOW      = 3,
            HSHELL_WINDOWACTIVATED          = 4,
            HSHELL_GETMINRECT               = 5,
            HSHELL_REDRAW                   = 6,
            HSHELL_TASKMAN                  = 7,
            HSHELL_LANGUAGE                 = 8,
            HSHELL_SYSMENU                  = 9,
            HSHELL_ENDTASK                  = 10,
            HSHELL_ACCESSIBILITYSTATE       = 11,
            HSHELL_APPCOMMAND               = 12,
            HSHELL_WINDOWREPLACED           = 13,
            HSHELL_WINDOWREPLACING          = 14,
            HSHELL_HIGHBIT                  = 0x8000,
            HSHELL_FLASH                    = (HSHELL_REDRAW | HSHELL_HIGHBIT),
            HSHELL_RUDEAPPACTIVATED         = (HSHELL_WINDOWACTIVATED | HSHELL_HIGHBIT)
        }

        public enum GetWindow_Cmd : uint
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MinimizedMetrics
        {
            public uint cbSize;
            public int iWidth;
            public int iHorzGap;
            public int iVertGap;
            public MinimizedMetricsArrangement iArrange;
        }

        [Flags]
        public enum SPIF
        {
            None = 0x00,
            /// <summary>Writes the new system-wide parameter setting to the user profile.</summary>
            SPIF_UPDATEINIFILE = 0x01,
            /// <summary>Broadcasts the WM_SETTINGCHANGE message after updating the user profile.</summary>
            SPIF_SENDCHANGE = 0x02,
            /// <summary>Same as SPIF_SENDCHANGE.</summary>
            SPIF_SENDWININICHANGE = 0x02
        }

        [Flags]
        public enum MinimizedMetricsArrangement
        {
            BottomLeft  = 0,
            BottomRight = 1,
            TopLeft     = 2,
            TopRight    = 3,
            Left        = 0,
            Right       = 0,
            Up          = 4,
            Down        = 4,
            Hide        = 8
        }

        public enum SPI : uint
        {
            /// <summary>
            /// Sets the metrics associated with minimized windows. The pvParam parameter must point to a MINIMIZEDMETRICS structure 
            /// that contains the new parameters. Set the cbSize member of this structure and the uiParam parameter to sizeof(MINIMIZEDMETRICS).
            /// </summary>
            SPI_SETMINIMIZEDMETRICS = 0x002C,
            SPI_GETMINIMIZEDMETRICS = 0x002B,

            /// <summary>
            /// Retrieves the size of the work area on the primary display monitor. The work area is the portion of the screen not obscured
            /// by the system taskbar or by application desktop toolbars. The pvParam parameter must point to a RECT structure that receives 
            /// the coordinates of the work area, expressed in virtual screen coordinates. 
            /// To get the work area of a monitor other than the primary display monitor, call the GetMonitorInfo function.
            /// </summary>
            SPI_GETWORKAREA = 0x0030,

            /// expressed in virtual screen coordinates. In a system with multiple display monitors, the function sets the work area 
            /// of the monitor that contains the specified rectangle.
            /// </summary>
            SPI_SETWORKAREA = 0x002F,
        }

        public enum GetAncestorFlags
        {
            /// <summary>
            /// Retrieves the parent window. This does not include the owner, as it does with the GetParent function. 
            /// </summary>
            GetParent = 1,
            /// <summary>
            /// Retrieves the root window by walking the chain of parent windows.
            /// </summary>
            GetRoot = 2,
            /// <summary>
            /// Retrieves the owned root window by walking the chain of parent and owner windows returned by GetParent. 
            /// </summary>
            GetRootOwner = 3
        }

        public enum MouseMessages
        {
            WM_LBUTTONDOWN  = 0x0201,
            WM_LBUTTONUP    = 0x0202,
            WM_MOUSEMOVE    = 0x0200,
            WM_MOUSEWHEEL   = 0x020A,
            WM_RBUTTONDOWN  = 0x0204,
            WM_RBUTTONUP    = 0x0205
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;

            /// <summary>
            /// Construct a point of coordinates (x,y).
            /// </summary>
            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        }

        [Flags()]
        public enum ProcessAccess : int
        {
            AllAccess = CreateThread | DuplicateHandle | QueryInformation | SetInformation | Terminate | VMOperation | VMRead | VMWrite | Synchronize,
            CreateThread = 0x2,
            DuplicateHandle = 0x40,
            QueryInformation = 0x400,
            SetInformation = 0x200,
            Terminate = 0x1,
            VMOperation = 0x8,
            VMRead = 0x10,
            VMWrite = 0x20,
            Synchronize = 0x100000
        }

        [Flags]
        public enum DOCHOSTUIFLAG
        {
            DIALOG                              = 0x00000001,
            DISABLE_HELP_MENU                   = 0x00000002,
            NO3DBORDER                          = 0x00000004,
            SCROLL_NO                           = 0x00000008,
            DISABLE_SCRIPT_INACTIVE             = 0x00000010,
            OPENNEWWIN                          = 0x00000020,
            DISABLE_OFFSCREEN                   = 0x00000040,
            FLAT_SCROLLBAR                      = 0x00000080,
            DIV_BLOCKDEFAULT                    = 0x00000100,
            ACTIVATE_CLIENTHIT_ONLY             = 0x00000200,
            OVERRIDEBEHAVIORFACTORY             = 0x00000400,
            CODEPAGELINKEDFONTS                 = 0x00000800,
            URL_ENCODING_DISABLE_UTF8           = 0x00001000,
            URL_ENCODING_ENABLE_UTF8            = 0x00002000,
            ENABLE_FORMS_AUTOCOMPLETE           = 0x00004000,
            ENABLE_INPLACE_NAVIGATION           = 0x00010000,
            IME_ENABLE_RECONVERSION             = 0x00020000,
            THEME                               = 0x00040000,
            NOTHEME                             = 0x00080000,
            NOPICS                              = 0x00100000,
            NO3DOUTERBORDER                     = 0x00200000,
            DISABLE_EDIT_NS_FIXUP               = 0x00400000,
            LOCAL_MACHINE_ACCESS_CHECK          = 0x00800000,
            DISABLE_UNTRUSTEDPROTOCOL           = 0x01000000,
            HOST_NAVIGATES                      = 0x02000000,
            ENABLE_REDIRECT_NOTIFICATION        = 0x04000000,
            USE_WINDOWLESS_SELECTCONTROL        = 0x08000000,
            USE_WINDOWED_SELECTCONTROL          = 0x10000000,
            ENABLE_ACTIVEX_INACTIVATE_MODE      = 0x20000000,
            DPI_AWARE                           = 0x40000000
        }

        [Flags]
        public enum MouseEventFlags : uint
        {
            LEFTDOWN = 0x00000002,
            LEFTUP = 0x00000004,
            MIDDLEDOWN = 0x00000020,
            MIDDLEUP = 0x00000040,
            MOVE = 0x00000001,
            ABSOLUTE = 0x00008000,
            RIGHTDOWN = 0x00000008,
            RIGHTUP = 0x00000010,
            WHEEL = 0x00000800,
            XDOWN = 0x00000080,
            XUP = 0x00000100
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MSLLHOOKSTRUCT
        {
            public POINT pt;
            public uint mouseData;
            public uint flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [Flags]
        public enum SyncObjectAccess : uint
        {
            DELETE = 0x00010000,
            READ_CONTROL = 0x00020000,
            WRITE_DAC = 0x00040000,
            WRITE_OWNER = 0x00080000,
            SYNCHRONIZE = 0x00100000,
            EVENT_ALL_ACCESS = 0x001F0003,
            EVENT_MODIFY_STATE = 0x00000002,
            MUTEX_ALL_ACCESS = 0x001F0001,
            MUTEX_MODIFY_STATE = 0x00000001,
            SEMAPHORE_ALL_ACCESS = 0x001F0003,
            SEMAPHORE_MODIFY_STATE = 0x00000002,
            TIMER_ALL_ACCESS = 0x001F0003,
            TIMER_MODIFY_STATE = 0x00000002,
            TIMER_QUERY_STATE = 0x00000001
        }

        public struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KeyboardFilterStruct
        {
            public ushort scanCode;			//scan code
            public ushort ctrlKey;			//1->Indicates ctrl key pressed    , 0->Indicates ctrl key not pressed
            public ushort altKey;			//1->Indicates alt key pressed     , 0->Indicates alt key not pressed
            public ushort shiftKey;			//1->Indicates shift key pressed   , 0->Indicates shift key not pressed
            public ushort windowsKey;		//1->Indicates windows key pressed , 0->Indicates windows key not pressed
            public Boolean allow;
        }

        // This structure is sent when the PBT_POWERSETTINGSCHANGE message is sent.
        // It describes the power setting that has changed and contains data about the change
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct POWERBROADCAST_SETTING
        {
            public Guid PowerSetting;
            public uint DataLength;
            public byte Data;
        }

        public enum WindowLongFlags : int
        {
            GWL_EXSTYLE = -20,
            GWLP_HINSTANCE = -6,
            GWLP_HWNDPARENT = -8,
            GWL_ID = -12,
            GWL_STYLE = -16,
            GWL_USERDATA = -21,
            GWL_WNDPROC = -4,
            DWLP_USER = 0x8,
            DWLP_MSGRESULT = 0x0,
            DWLP_DLGPROC = 0x4
        }

        [Flags()]
        public enum SetWindowPosFlags : uint
        {
            /// <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
            /// the system posts the request to the thread that owns the window. This prevents the calling thread from 
            /// blocking its execution while other threads process the request.</summary>
            /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
            AsynchronousWindowPosition = 0x4000,
            /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
            /// <remarks>SWP_DEFERERASE</remarks>
            DeferErase = 0x2000,
            /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
            /// <remarks>SWP_DRAWFRAME</remarks>
            DrawFrame = 0x0020,
            /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to 
            /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE 
            /// is sent only when the window's size is being changed.</summary>
            /// <remarks>SWP_FRAMECHANGED</remarks>
            FrameChanged = 0x0020,
            /// <summary>Hides the window.</summary>
            /// <remarks>SWP_HIDEWINDOW</remarks>
            HideWindow = 0x0080,
            /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the 
            /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
            /// parameter).</summary>
            /// <remarks>SWP_NOACTIVATE</remarks>
            DoNotActivate = 0x0010,
            /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
            /// contents of the client area are saved and copied back into the client area after the window is sized or 
            /// repositioned.</summary>
            /// <remarks>SWP_NOCOPYBITS</remarks>
            DoNotCopyBits = 0x0100,
            /// <summary>Retains the current position (ignores X and Y parameters).</summary>
            /// <remarks>SWP_NOMOVE</remarks>
            IgnoreMove = 0x0002,
            /// <summary>Does not change the owner window's position in the Z order.</summary>
            /// <remarks>SWP_NOOWNERZORDER</remarks>
            DoNotChangeOwnerZOrder = 0x0200,
            /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to 
            /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
            /// window uncovered as a result of the window being moved. When this flag is set, the application must 
            /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
            /// <remarks>SWP_NOREDRAW</remarks>
            DoNotRedraw = 0x0008,
            /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
            /// <remarks>SWP_NOREPOSITION</remarks>
            DoNotReposition = 0x0200,
            /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
            /// <remarks>SWP_NOSENDCHANGING</remarks>
            DoNotSendChangingEvent = 0x0400,
            /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
            /// <remarks>SWP_NOSIZE</remarks>
            IgnoreResize = 0x0001,
            /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
            /// <remarks>SWP_NOZORDER</remarks>
            IgnoreZOrder = 0x0004,
            /// <summary>Displays the window.</summary>
            /// <remarks>SWP_SHOWWINDOW</remarks>
            ShowWindow = 0x0040, //ShowWindow,DoNotSendChangingEvent,DoNotCopyBits
        }

        public enum ShowWindowCommands : int
        {
            /// <summary>
            /// Hides the window and activates another window.
            /// </summary>
            Hide = 0,
            /// <summary>
            /// Activates and displays a window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position.
            /// An application should specify this flag when displaying the window 
            /// for the first time.
            /// </summary>
            Normal = 1,
            /// <summary>
            /// Activates the window and displays it as a minimized window.
            /// </summary>
            ShowMinimized = 2,
            /// <summary>
            /// Maximizes the specified window.
            /// </summary>
            Maximize = 3, // is this the right value?
            /// <summary>
            /// Activates the window and displays it as a maximized window.
            /// </summary>       
            ShowMaximized = 3,
            /// <summary>
            /// Displays a window in its most recent size and position. This value 
            /// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except 
            /// the window is not activated.
            /// </summary>
            ShowNoActivate = 4,
            /// <summary>
            /// Activates the window and displays it in its current size and position. 
            /// </summary>
            Show = 5,
            /// <summary>
            /// Minimizes the specified window and activates the next top-level 
            /// window in the Z order.
            /// </summary>
            Minimize = 6,
            /// <summary>
            /// Displays the window as a minimized window. This value is similar to
            /// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowMinNoActive = 7,
            /// <summary>
            /// Displays the window in its current size and position. This value is 
            /// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowNA = 8,
            /// <summary>
            /// Activates and displays the window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position. 
            /// An application should specify this flag when restoring a minimized window.
            /// </summary>
            Restore = 9,
            /// <summary>
            /// Sets the show state based on the SW_* value specified in the 
            /// STARTUPINFO structure passed to the CreateProcess function by the 
            /// program that started the application.
            /// </summary>
            ShowDefault = 10,
            /// <summary>
            ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
            /// that owns the window is not responding. This flag should only be 
            /// used when minimizing windows from a different thread.
            /// </summary>
            ForceMinimize = 11
        }

        public enum AppComandCode : int
        {
            VOLUME_MUTE = 0x80000,
            VOLUME_UP = 0xA0000,
            VOLUME_DOWN = 0x90000,
        }

        [Flags]
        public enum WindowStylesEx : uint
        {
            /// <summary>Specifies a window that accepts drag-drop files.</summary>
            WS_EX_ACCEPTFILES = 0x00000010,

            /// <summary>Forces a top-level window onto the taskbar when the window is visible.</summary>
            WS_EX_APPWINDOW = 0x00040000,

            /// <summary>Specifies a window that has a border with a sunken edge.</summary>
            WS_EX_CLIENTEDGE = 0x00000200,

            /// <summary>
            /// Specifies a window that paints all descendants in bottom-to-top painting order using double-buffering.
            /// This cannot be used if the window has a class style of either CS_OWNDC or CS_CLASSDC. This style is not supported in Windows 2000.
            /// </summary>
            /// <remarks>
            /// With WS_EX_COMPOSITED set, all descendants of a window get bottom-to-top painting order using double-buffering.
            /// Bottom-to-top painting order allows a descendent window to have translucency (alpha) and transparency (color-key) effects,
            /// but only if the descendent window also has the WS_EX_TRANSPARENT bit set.
            /// Double-buffering allows the window and its descendents to be painted without flicker.
            /// </remarks>
            WS_EX_COMPOSITED = 0x02000000,

            /// <summary>
            /// Specifies a window that includes a question mark in the title bar. When the user clicks the question mark,
            /// the cursor changes to a question mark with a pointer. If the user then clicks a child window, the child receives a WM_HELP message.
            /// The child window should pass the message to the parent window procedure, which should call the WinHelp function using the HELP_WM_HELP command.
            /// The Help application displays a pop-up window that typically contains help for the child window.
            /// WS_EX_CONTEXTHELP cannot be used with the WS_MAXIMIZEBOX or WS_MINIMIZEBOX styles.
            /// </summary>
            WS_EX_CONTEXTHELP = 0x00000400,

            /// <summary>
            /// Specifies a window which contains child windows that should take part in dialog box navigation.
            /// If this style is specified, the dialog manager recurses into children of this window when performing navigation operations
            /// such as handling the TAB key, an arrow key, or a keyboard mnemonic.
            /// </summary>
            WS_EX_CONTROLPARENT = 0x00010000,

            /// <summary>Specifies a window that has a double border.</summary>
            WS_EX_DLGMODALFRAME = 0x00000001,

            /// <summary>
            /// Specifies a window that is a layered window.
            /// This cannot be used for child windows or if the window has a class style of either CS_OWNDC or CS_CLASSDC.
            /// </summary>
            WS_EX_LAYERED = 0x00080000,

            /// <summary>
            /// Specifies a window with the horizontal origin on the right edge. Increasing horizontal values advance to the left.
            /// The shell language must support reading-order alignment for this to take effect.
            /// </summary>
            WS_EX_LAYOUTRTL = 0x00400000,

            /// <summary>Specifies a window that has generic left-aligned properties. This is the default.</summary>
            WS_EX_LEFT = 0x00000000,

            /// <summary>
            /// Specifies a window with the vertical scroll bar (if present) to the left of the client area.
            /// The shell language must support reading-order alignment for this to take effect.
            /// </summary>
            WS_EX_LEFTSCROLLBAR = 0x00004000,

            /// <summary>
            /// Specifies a window that displays text using left-to-right reading-order properties. This is the default.
            /// </summary>
            WS_EX_LTRREADING = 0x00000000,

            /// <summary>
            /// Specifies a multiple-document interface (MDI) child window.
            /// </summary>
            WS_EX_MDICHILD = 0x00000040,

            /// <summary>
            /// Specifies a top-level window created with this style does not become the foreground window when the user clicks it.
            /// The system does not bring this window to the foreground when the user minimizes or closes the foreground window.
            /// The window does not appear on the taskbar by default. To force the window to appear on the taskbar, use the WS_EX_APPWINDOW style.
            /// To activate the window, use the SetActiveWindow or SetForegroundWindow function.
            /// </summary>
            WS_EX_NOACTIVATE = 0x08000000,

            /// <summary>
            /// Specifies a window which does not pass its window layout to its child windows.
            /// </summary>
            WS_EX_NOINHERITLAYOUT = 0x00100000,

            /// <summary>
            /// Specifies that a child window created with this style does not send the WM_PARENTNOTIFY message to its parent window when it is created or destroyed.
            /// </summary>
            WS_EX_NOPARENTNOTIFY = 0x00000004,

            /// <summary>Specifies an overlapped window.</summary>
            WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,

            /// <summary>Specifies a palette window, which is a modeless dialog box that presents an array of commands.</summary>
            WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST,

            /// <summary>
            /// Specifies a window that has generic "right-aligned" properties. This depends on the window class.
            /// The shell language must support reading-order alignment for this to take effect.
            /// Using the WS_EX_RIGHT style has the same effect as using the SS_RIGHT (static), ES_RIGHT (edit), and BS_RIGHT/BS_RIGHTBUTTON (button) control styles.
            /// </summary>
            WS_EX_RIGHT = 0x00001000,

            /// <summary>Specifies a window with the vertical scroll bar (if present) to the right of the client area. This is the default.</summary>
            WS_EX_RIGHTSCROLLBAR = 0x00000000,

            /// <summary>
            /// Specifies a window that displays text using right-to-left reading-order properties.
            /// The shell language must support reading-order alignment for this to take effect.
            /// </summary>
            WS_EX_RTLREADING = 0x00002000,

            /// <summary>Specifies a window with a three-dimensional border style intended to be used for items that do not accept user input.</summary>
            WS_EX_STATICEDGE = 0x00020000,

            /// <summary>
            /// Specifies a window that is intended to be used as a floating toolbar.
            /// A tool window has a title bar that is shorter than a normal title bar, and the window title is drawn using a smaller font.
            /// A tool window does not appear in the taskbar or in the dialog that appears when the user presses ALT+TAB.
            /// If a tool window has a system menu, its icon is not displayed on the title bar.
            /// However, you can display the system menu by right-clicking or by typing ALT+SPACE. 
            /// </summary>
            WS_EX_TOOLWINDOW = 0x00000080,

            /// <summary>
            /// Specifies a window that should be placed above all non-topmost windows and should stay above them, even when the window is deactivated.
            /// To add or remove this style, use the SetWindowPos function.
            /// </summary>
            WS_EX_TOPMOST = 0x00000008,

            /// <summary>
            /// Specifies a window that should not be painted until siblings beneath the window (that were created by the same thread) have been painted.
            /// The window appears transparent because the bits of underlying sibling windows have already been painted.
            /// To achieve transparency without these restrictions, use the SetWindowRgn function.
            /// </summary>
            WS_EX_TRANSPARENT = 0x00000020,

            /// <summary>Specifies a window that has a border with a raised edge.</summary>
            WS_EX_WINDOWEDGE = 0x00000100
        }

        public struct MSG
        {
            public IntPtr hwnd;
            public uint message;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public POINT pt;
        }

        #endregion

        #region P/Invoke Methods

        [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
        public static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
            string lpszUsername,
            string lpszDomain,
            string lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            out IntPtr phToken
            );


        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool GetTokenInformation(IntPtr tokenHandle, TokenInformationClass tokenInformationClass, IntPtr tokenInformation, int tokenInformationLength, out int returnLength);

        [DllImport("kernel32.dll")]
        public static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("Wtsapi32.dll")]
        public static extern bool WTSQuerySessionInformation(IntPtr hServer, int sessionId, WtsInfoClass wtsInfoClass, out System.IntPtr ppBuffer, out int pBytesReturned);

        [DllImport("WTSAPI32.DLL", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern void WTSFreeMemory(IntPtr pMemory);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ExitWindowsEx(ExitWindows uFlags, ShutdownReason dwReason);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        [DllImport("user32")]
        public static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        [DllImport("User32")]
        public static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);


        [DllImport("iphlpapi.dll")]
        private static extern ERROR GetAdaptersAddresses(uint Family, uint Flags, IntPtr Reserved, IntPtr pAdapterAddresses, ref uint pOutBufLen);

        [DllImport("Shlwapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint AssocQueryString(AssocF flags, AssocStr str, string pszAssoc, string pszExtra, [Out] StringBuilder pszOut, [In][Out] ref uint pcchOut);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool ChangeWindowMessageFilterEx(IntPtr hWnd, uint msg, ChangeWindowMessageFilterExAction action, ref CHANGEFILTERSTRUCT changeInfo);

        [DllImport("user32.dll")]
        public static extern bool AllowSetForegroundWindow(int dwProcessId);

        [DllImport("user32.dll")]
        public static extern ushort RegisterClass([In] ref WNDCLASS lpWndClass);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessageTimeout(
            IntPtr windowHandle,
            uint Msg,
            Int64 wParam,
            IntPtr lParam,
            SendMessageTimeoutFlags flags,
            uint timeout,
            out IntPtr result);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessageTimeout(
            IntPtr windowHandle,
            uint Msg,
            IntPtr wParam,
            IntPtr lParam,
            SendMessageTimeoutFlags flags,
            uint timeout,
            out IntPtr result);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindow(IntPtr hWnd);

        /// </returns>
        [DllImport("user32.dll")]
        public static extern bool UnregisterClass(string lpClassName, IntPtr hInstance);

        public static IntPtr SetWindowLongPtr(HandleRef hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
            {
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            }
            else
            {
                return new IntPtr(SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32()));
            }
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLong32(HandleRef hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(HandleRef hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr CreateWindowEx(
           WindowStylesEx dwExStyle,
           string lpClassName,
           string lpWindowName,
           WindowStyles dwStyle,
           int x,
           int y,
           int nWidth,
           int nHeight,
           IntPtr hWndParent,
           IntPtr hMenu,
           IntPtr hInstance,
           IntPtr lpParam);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        public static extern System.UInt16 RegisterClassW([System.Runtime.InteropServices.In] ref WNDCLASS lpWndClass);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        public static extern System.IntPtr DefWindowProcW(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool SendNotifyMessage(int hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern bool TranslateMessage([In] ref MSG lpMsg);

        [DllImport("user32.dll")]
        public static extern IntPtr DispatchMessage([In] ref MSG lpmsg);

        [DllImport("user32.dll")]
        public static extern sbyte GetMessage(out MSG lpMsg, IntPtr hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool DestroyIcon(IntPtr hIcon);

        [DllImport("user32.dll")]
        public static extern IntPtr CopyIcon(IntPtr hIcon);

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr SetWindowsHookEx(int idHook, LowLevelProc lpfn, IntPtr hMod, uint dwThreadId);

        //[DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //public static extern int GetWindowThreadProcessId(IntPtr hwnd, ref int lpdwProcessId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("kernel32.dll")]
        public static extern bool SetEvent(IntPtr hEvent);

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern IntPtr OpenEvent(uint dwDesiredAccess, bool bInheritHandle, string lpName);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(ProcessAccess dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("psapi", EntryPoint = "GetModuleFileNameExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern uint GetModuleFileNameEx(IntPtr hProcess, IntPtr hModule, [Out] StringBuilder lpBaseName, [In] [MarshalAs(UnmanagedType.U4)] int nSize);

        [DllImport("User32.dll")]
        public static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [DllImport("user32.dll")]
        public static extern void mouse_event(MouseEventFlags dwFlags, int dx, int dy, int dwData, UIntPtr dwExtraInfo);

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern long CloseHandle(IntPtr hObject);

        [DllImport(@"User32", SetLastError = true, EntryPoint = "RegisterPowerSettingNotification",
       CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr RegisterPowerSettingNotification(
            IntPtr hRecipient,
            ref Guid PowerSettingGuid,
            Int32 Flags);

        [DllImport(@"User32", EntryPoint = "UnregisterPowerSettingNotification",
         CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnregisterPowerSettingNotification(
            IntPtr handle);

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DestroyWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern uint RegisterWindowMessage(string Message);

        [DllImport("user32.dll")]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
        public static extern bool SendMessageStruct(IntPtr hWnd, int Msg, int wParam, ref COPYDATASTRUCT1 lParam);

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SetShellWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool RegisterShellHookWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll")]
        public static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, IntPtr lParam);

        [DllImport("user32.dll")]
        static public extern void SetTaskmanWindow(IntPtr hwnd);

        // For Windows Mobile, replace user32.dll with coredll.dll
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);

        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(IntPtr hWnd, ShowWindowCommands nCmdShow);

        /// <summary>The GetForegroundWindow function returns a handle to the foreground window.</summary>
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll")]
        public extern static int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("User32")]
        public static extern int RemoveMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("User32")]
        public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("User32")]
        public static extern int GetMenuItemCount(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool CloseWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SystemParametersInfo(SPI uiAction, uint uiParam, ref MinimizedMetrics pvParam, SPIF fWinIni);

        [DllImport("User32.dll", SetLastError = true)]
        public static extern bool SystemParametersInfo(uint iAction, uint iParameter, ref uint pParameter, uint iWinIni);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern void GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);

        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr GetAncestor(IntPtr hwnd, GetAncestorFlags flags);

        [DllImport("user32.dll")]
        public static extern IntPtr GetLastActivePopup(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("user32.dll")]
        public static extern IntPtr GetShellWindow();

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GetCurrentProcess();

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("Kernel32.dll")]
        public static extern bool AttachConsole(int processId);

        #endregion

        #region Custom Methods

        public static void SetToolWindow(IntPtr handle)
        {
            SetWindowLong(handle, (int)WindowLongFlags.GWL_EXSTYLE, (int)(ApplicationConstants.WS_EX_TOOLWINDOW | ApplicationConstants.WS_EX_TOPMOST | ApplicationConstants.WS_EX_NOACTIVATE));
        }

        public static void SetTopWindow(IntPtr handle)
        {
            SetWindowLong(handle, (int)WindowLongFlags.GWL_EXSTYLE, (int)(ApplicationConstants.WS_EX_TOPMOST | ApplicationConstants.WS_EX_NOACTIVATE));
        } 
		
        private static string GetAdapterName(IntPtr text)
        {
            try
            {
                string name     =  Marshal.PtrToStringAnsi(text);
                return name;
            }
            catch (Exception)
            {
            }
            return null;
        }

        public static bool ProcessExists(int id)
        {
            return Process.GetProcesses().Any(x => x.Id == id);
        }

        public static uint GetIdleTime()
        {
            LASTINPUTINFO lastInPut = new LASTINPUTINFO();
            lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            return ((uint)Environment.TickCount - lastInPut.dwTime);
        }

        //public static string ExePathFromHwnd(IntPtr hWnd)
        //{
        //    StringBuilder sb = new StringBuilder(MAX_PATH);
        //    int pid = 0;
        //    GetWindowThreadProcessId(hWnd, ref  pid);
        //    IntPtr hProc = OpenProcess(ProcessAccess.AllAccess, false, pid);
        //    if (hProc != IntPtr.Zero)
        //    {
        //        uint retVal = GetModuleFileNameEx(hProc, IntPtr.Zero, sb, MAX_PATH);
        //        CloseHandle(hProc);
        //    }
        //    return sb.ToString();
        //}

        public static void PowerOnMonitor()
        {
            mouse_event(MouseEventFlags.MOVE, 0, 1, 0, UIntPtr.Zero);
            mouse_event(MouseEventFlags.MOVE, 0, -1, 0, UIntPtr.Zero);
        }

        public static void PowerOffMonitor()
        {
            IntPtr sureLockHandle = Win32.FindWindow(null, "SureLock");
            if (sureLockHandle != IntPtr.Zero)
            {
                SendMessage(sureLockHandle, ApplicationConstants.WM_SYSCOMMAND, ApplicationConstants.SC_MONITORPOWER, ApplicationConstants.MONITOR_OFF);
            }
        }

        public static void SignalShellReadyEvent()
        {
            IntPtr hShellReadyEvent;
            hShellReadyEvent = OpenEvent((uint)SyncObjectAccess.EVENT_MODIFY_STATE, false, "ShellDesktopSwitchEvent");
            if (hShellReadyEvent != IntPtr.Zero)
            {
                SetEvent(hShellReadyEvent);
                CloseHandle(hShellReadyEvent);
            }
        }

        public static bool SetTopMostWindow(IntPtr hWnd)
        {
            return SetWindowPos(hWnd, ApplicationConstants.HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        }

        public static int GetLastError()
        {
            return Marshal.GetLastWin32Error();
        }

        #endregion

        #region Constants

        public const int URLMON_OPTION_USERAGENT    = 0x10000001;
        public const int WH_MOUSE_LL                = 14;
        public const int WM_DEVICECHANGE            = 0x0219;
        public const int WM_QUERYENDSESSION         = 0x11;
        public const uint GAA_FLAG_INCLUDE_ALL_INTERFACES 
                                                    = 0x0100;
        public const int DBT_DEVICEARRIVAL          = 0x8000;
        public const int DBT_DEVICEQUERYREMOVE      = 0x8001;
        public const int DBT_DEVICEREMOVECOMPLETE   = 0x8004;
        public const int DBT_DEVTYP_VOLUME          = 0x00000002;
        public const uint WM_COMMAND                = 0x0111;
        public const uint WM_CONTEXTMENU            = 0x007B;
        public static IntPtr SH_TRAY_DATA           = (IntPtr)1;
        public const int WM_COPYDATA                = 0x004A;
        public const uint WS_POPUP                  = 0x80000000;
        public const int GWL_USERDATA               = -21;
        public const string szTrayClass             = "Shell_TrayWnd";
        public const string szNotifyClass           = "TrayNotifyWnd";
        public const int HWND_BROADCAST             = 0xffff;
        public const int magicDWord                 = 0x49474541;
        public const UInt32 WS_EX_NOACTIVATE        = 0x08000000;
        public const UInt32 WS_EX_TOOLWINDOW        = 0x00000080;
        public const UInt32 WS_EX_TOPMOST           = 0x00000008;
        public const UInt32 WM_CLOSE                = 0x0010;
        public const UInt32 SWP_NOSIZE              = 0x0001;
        public const UInt32 SWP_NOMOVE              = 0x0002;
        public const UInt32 SWP_SHOWWINDOW          = 0x0040;
        public const UInt32 SWP_NOACTIVATE          = 0x0010;
        public const UInt32 TOPMOST_FLAGS           = SWP_NOMOVE | SWP_NOSIZE;
        public const int MF_BYPOSITION              = 0x400;
        private const Int32 MF_REMOVE               = 0x1000;
        public const int MAX_PATH                   = 1024;
        public const int GWL_STYLE                  = -16;
        public const int WS_SYSMENU                 = 0x80000;
        public const int GWL_EXSTYLE                = -20;
        public const uint WM_DISPLAYCHANGE          = 0x007E;
        public const uint WS_VISIBLE                = 0X94000000;
        #endregion
    }

    public class HWND
    {
        public static IntPtr
        NoTopMost   = new IntPtr(-2),
        TopMost     = new IntPtr(-1),
        Top         = new IntPtr(0),
        Bottom      = new IntPtr(1);
    }
}
