﻿using Microsoft.Win32;
using SureLockWin8.Interfaces;
using SureLockWin8.Model;
using System;
using System.IO;
using System.Management;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Utility.Common;
using Utility.License;

namespace SureLockWin8.Common
{
    public class License : ViewModelBase
    {       
        public delegate void ServerResponseEventHandler(string Title, string Message);
        public delegate void DeviceActivatedEventHandler(object sender,bool activated);

        public event DeviceActivatedEventHandler    Activated;
        public event ServerResponseEventHandler     ServerResponse;

        public License(RootModel rm)
        {
            baseModel       = rm;
        }

        public void GetDeviceInformation()
        {
            var query       = new SelectQuery("Win32_OperatingSystem");
            var searcher    = new ManagementObjectSearcher(query);
            try
            {
                foreach (ManagementObject envVar in searcher.Get())
                {
                    OS      = envVar["Caption"].ToString();
                }
                query       = new SelectQuery("Win32_ComputerSystem");
                searcher    = new ManagementObjectSearcher(query);
                foreach (ManagementObject envVar in searcher.Get())
                {
                    Model   = envVar["Model"].ToString();
                }
            }
            catch (Exception)
            {
            }
        }
        private void ShowServerResponse(string title,string message)
        {
            ServerResponse?.Invoke(title, message);
        }
        
        string _osName;
        public string OS
        {
            get { return _osName; }
            set
            {
                _osName = value;
                OnPropertyChanged("OS");
            }
        }

        string _model;
        public string Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged("Model");
            }
        }
    }
}
