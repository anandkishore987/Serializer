﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Utility.Enum;

namespace Utility.License
{
    public class KeyVerifier2
    {
        private static List<string> blackListedKeys = new List<string>();

        static KeyVerifier2()
        {
            blackListedKeys.Add("keys here");
        }

        public static KeyInfo verifyKey(string key, string deviceID, int productID)
        {
            if (key == null || blackListedKeys.Contains(key))
            {
                return null;
            }

            KeyInfo keyInfo = new KeyInfo();

            for (int nIndex = 0; nIndex < key.Length; nIndex++)
            {
                if (!char.IsUpper(key.ElementAt(nIndex)) && !char.IsDigit(key.ElementAt(nIndex)))
                {
                    keyInfo.errorCode = 1009;
                    return keyInfo;
                }
            }

            byte[] rawKey = validateChecksum(key);
            if (rawKey == null)
            {
                keyInfo.errorCode = 1013;
                return keyInfo;
            }
            else
            {
                byte[] validity = getSubByteArray(rawKey, 0, 5);
                byte[] trippleHash = getSubByteArray(rawKey, 5, rawKey.Length - 5);

                byte[] date = new byte[2];
                date[0] = validity[3];
                date[1] = validity[1];
                keyInfo.expiryDate = bytesToDate(date);
                if (keyInfo.expiryDate == null)
                {
                    keyInfo.errorCode = 1031;
                    return keyInfo;
                }
                int[] info = getProductInfo(validity[2]);
                keyInfo.productCode = info[0];

                if (keyInfo.productCode != productID)
                {
                    keyInfo.errorCode = 1033;
                    return keyInfo;
                }

                if (System.Enum.IsDefined(typeof(DeviceIDType), info[1]))
                {
                    keyInfo.type = (DeviceIDType)info[1];
                }
                else
                {
                    keyInfo.errorCode = 1039;
                    return keyInfo;
                }

                keyInfo.salt = trippleHash[trippleHash.Length - 1];
                int nameLength = trippleHash[trippleHash.Length - 2];
                byte[] name = getSubByteArray(trippleHash, trippleHash.Length - 2 - nameLength, nameLength);
                keyInfo.licensedTo = new UTF8Encoding().GetString(name);
                if (keyInfo.licensedTo.Length < 1)
                {
                    keyInfo.errorCode = 1049;
                    return keyInfo;
                }
                byte[] hashes = getSubByteArray(trippleHash, 0, trippleHash.Length - 2 - nameLength);
                if (hashes.Length % 3 == 0)
                {
                    int hashLength = hashes.Length / 3;
                    byte[] hash1 = getSubByteArray(hashes, 0, hashLength);
                    byte[] hash2 = getSubByteArray(hashes, hashLength, hashLength);
                    byte[] hash3 = getSubByteArray(hashes, hashLength * 2, hashLength);

                    byte[][] inputs = new byte[2][];
                    inputs[0] = new UTF8Encoding().GetBytes("'" + new UTF8Encoding().GetString(name) + "'");
                    inputs[1] = validity;
                    bool flag = verifyHahshes(hash1, hash2, hash3, inputs);

                    if (flag)
                    {
                        keyInfo.nDevices = hash3.Length - 2;

                        float eff1 = (float)count1s(hash1) / (hash1.Length * 8.0f);
                        float eff2 = (float)count1s(hash2) / (hash2.Length * 8.0f);
                        float eff3 = (float)count1s(hash3) / (hash3.Length * 8.0f);

                        keyInfo.security = (1 - eff1 * eff2 * eff3) * 100;
                        if (keyInfo.security < 99.8)
                        {
                            keyInfo.errorCode = 1051;
                            return keyInfo;
                        }
                        else
                        {
                            keyInfo.isValidKey = true;
                            if (deviceID != null)
                            {
                                byte[][] devID = new byte[1][];
                                devID[0] = new UTF8Encoding().GetBytes(deviceID);
                                flag = verifyHahshes(hash1, hash2, hash3, devID);
                                if (flag)
                                {
                                    keyInfo.isVerified = true;
                                }
                                else
                                {
                                    keyInfo.errorCode = 1061;
                                    keyInfo.isVerified = false;
                                }
                            }
                            else
                            {
                                keyInfo.errorCode = 1063;
                                keyInfo.isVerified = false;
                            }

                            return keyInfo;
                        }
                    }
                    else
                    {
                        keyInfo.errorCode = 1021;
                        return keyInfo;
                    }
                }
                else
                {
                    keyInfo.errorCode = 1019;
                    return keyInfo;
                }
            }
        }

        private static byte[] base32ToBytes(string base32string)
        {
            string str = base32string.Replace("L", "00").Replace("X", "000").Replace("R", "0000").Replace("F", "00000").Replace("Z", "F").Replace("Y", "L").Replace("W", "R");

            int lenMod = str.Length % 8;
            if (lenMod == 0 || lenMod == 2 || lenMod == 4 || lenMod == 5 || lenMod == 7)
            {
                int filler = base32string.Length % 8;
                string fillStr = "";
                if (filler == 6 || filler == 4 || filler == 3 || filler == 1)
                {

                    for (int i = 0; i < filler; i++)
                    {
                        fillStr += "=";
                    }
                }

                return new Base32().decode(new UTF8Encoding().GetBytes(str + fillStr));
            }
            return null;
        }

        private static ShortDate bytesToDate(byte[] shortDate)
        {
            ShortDate date = null;

            if (shortDate != null && shortDate.Length == 2)
            {
                int year    = (shortDate[0] >> 1) & 0x7F;
                int month   = (shortDate[1] & 0x0F);
                int day     = ((shortDate[1] >> 4) & 0x0F) | ((shortDate[0] << 4) & 0x10);
                date        = new ShortDate(day, month, year, false);
            }

            return date;
        }

        private static int count1s(byte b)
        {
            int count = 0;
            while (b != 0)
            {
                count += (b & 1);
                b = (byte)((b >> 1) & 0x7F);
            }
            return count;
        }

        private static int count1s(byte[] bytes)
        {
            int count = 0;
            foreach (byte b in bytes)
            {
                count += count1s(b);
            }
            return count;
        }

        public static int getHash(byte[] key, int algorithm, int sizeInBits)
        {
            byte[] checksum;
            switch (algorithm)
            {
                case 0:
                    checksum = new SHA512Managed().ComputeHash(key);
                    break;
                case 1:
                    checksum = new SHA256Managed().ComputeHash(key);
                    break;
                case 2:
                    checksum = new MD5CryptoServiceProvider().ComputeHash(key);
                    break;
                default:
                    checksum = new byte[0];
                    break;
            }
            Array.Reverse(checksum, 0, 8);
            long l = BitConverter.ToInt64(checksum, 0);
            return Math.Abs((int)(Math.Abs(l) % sizeInBits));
        }

        /*private static void showBytes(byte[] bytes)
        {
            if (bytes != null)
            {
                for (byte b : bytes)
                {
                    System.out.print(String.valueOf(b) + "   ");
                }
                System.out.println("");
            }
        }*/

        private static int[] getProductInfo(byte info)
        {
            int[] result = new int[2];
            result[0] = info & 0x0F;
            result[1] = (info >> 4) & 0x0F;
            return result;
        }

        private static byte[] validateChecksum(string key)
        {
            if (key != null)
            {
                byte[] rawKey = base32ToBytes(key);
                if (rawKey != null && rawKey.Length > 5)
                {
                    int checksum = (int)rawKey[4];
                    rawKey[4] = 0;
                    if (checksum < 0)
                    {
                        checksum = checksum + 256;
                    }
                    if (getHash(rawKey, 1, 256) == checksum)
                    {
                        checksum = (int)rawKey[0];
                        rawKey[0] = 0;
                        if (checksum < 0)
                        {
                            checksum = checksum + 256;
                        }
                        if (getHash(rawKey, 0, 256) == checksum)
                        {
                            return rawKey;
                        }
                    }
                }
            }
            return null;
        }

        private static bool verifyHahshes(byte[] hash1, byte[] hash2, byte[] hash3, byte[][] inputs)
        {
            foreach (byte[] input in inputs)
            {
                if (getBitValue(hash1, getHash(input, 0, hash1.Length * 8))
                        && getBitValue(hash2, getHash(input, 1, hash2.Length * 8))
                        && getBitValue(hash3, getHash(input, 2, hash3.Length * 8)))
                {
                    // correct
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private static bool getBitValue(byte[] array, int position)
        {
            if (position >= 0 && position < array.Length * 8)
            {
                int posByte = position / 8;
                int posBit = position % 8;
                int value = array[posByte] & (byte)(1 << posBit);
                if (value == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        private static byte[] getSubByteArray(byte[] rawKey, int position, int size)
        {
            byte[] result = new byte[size];
            for (int i = position; i < position + size; i++)
            {
                result[i - position] = rawKey[i];
            }
            return result;
        }
    }
}
