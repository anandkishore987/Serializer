﻿using Microsoft.Win32;
using SureLockWin8.Interfaces;
using SureLockWin8.Model;
using System;
using System.IO;
using System.Management;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Utility.Common;
using Utility.License;
using Utility.Logger;

namespace SureLockWin8.Common
{
    public class License : ViewModelBase
    {       
        public delegate void ServerResponseEventHandler(string Title, string Message);
        public delegate void DeviceActivatedEventHandler(object sender,bool activated);

        public event DeviceActivatedEventHandler    Activated;
        public event ServerResponseEventHandler     ServerResponse;

        public License(RootModel rm)
        {
            baseModel       = rm;
        }

        public void VerifyRenewKey(string licKey, string renewKey)
        {
            try
            {
                LicKey              = licKey;
                RenewKey            = renewKey;
                bool isKeyExpired   = false;
                KeyInfo keyInfo     = null;
                bool isValid        = Verifier.IsLicenseValid(LicKey, RenewKey, MacWifi, MacBluetooth, GUID, out isKeyExpired, out keyInfo);
                int errorCode       = 0;

                if (isValid)
                {
                    IsActivated(true);
                    ShowServerResponse(ApplicationConstants.ActivationResponseTitle, ApplicationConstants.RenewSuccess);
                    baseModel.LicKeyInfo = keyInfo;
                    SetExpiryDate(baseModel.LicKeyInfo.expiryDate);
                    SaveRenewKey(renewKey);
                }
                else
                {
                    if(keyInfo != null)
                    {
                        errorCode   = keyInfo.errorCode;
                    }

                    ShowServerResponse(ApplicationConstants.Error, string.Format(ApplicationConstants.RenewLicError, errorCode));
                }
            }
            catch (Exception)
            {
            }
        }

        private void SaveRenewKey(string renewKey)
        {
            using (var SureLockKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\42gears\\SureLock", true))
            {
                if (SureLockKey != null)
                {
                    SureLockKey.SetValue("RenewKey", renewKey);
                    SureLockKey.Close();
                }
            }
        }

        //Method to activate the device with specified Activation Code
        public void ActivateDevice(string ActivationCode, bool async = true)
        {
            try
            {
                HTTP httpObject = new HTTP();
                if(async)
                {
                    httpObject.AsyncHttpPost(ApplicationConstants.ActivationServerUrl, GetActivationXML(ActivationCode), GotHttpResponse_Activation);
                }
                else
                {
                    string xml  = httpObject.HttpPost(ApplicationConstants.ActivationServerUrl, GetActivationXML(ActivationCode));
                    GotHttpResponse_Activation(xml);
                }
            }
            catch (Exception exp)
            {
                GotHttpResponse_Activation(exp.Message);
            }
        }

        //Method which receives the HTTP response on Activation
        public void GotHttpResponse_Activation(string xml)
        {
            string detail           = xml.Trim();

            //Checks if the response is a valid XML
            if (!detail.StartsWith("<") && !detail.EndsWith(">"))
            {
                //Exception message during web Request
                if(ServerResponse != null)
                {
                    ShowServerResponse(ApplicationConstants.Error, ApplicationConstants.ActivationErrorMsg);
                }
                else
                {
                    Toast.ShowToast(ApplicationConstants.ActivationErrorMsg);
                }
                IsActivated(false);
                Logger.LogMessage(xml);
            }
            else
            {
                XDocument xmlDoc    = XDocument.Parse(xml);

                foreach (var Result in xmlDoc.Descendants("Result"))
                {
                    //Device activation FAILURE from Server
                    if (Result.Value.ToString().Equals("False"))
                    {
                        foreach (var Message in xmlDoc.Descendants("Message"))
                        {
                            string message = Message.Value.ToString();
                            foreach (var ErrorCode in xmlDoc.Descendants("ErrorCode"))
                            {
                                //message = message + "Error code: " + ErrorCode.Value.ToString();
                                message = ApplicationConstants.ActivationErrorMsg + " \nError code: " + ErrorCode.Value.ToString();
                                if (ServerResponse != null)
                                {
                                    ShowServerResponse(ApplicationConstants.ActivationResponseTitle, message);
                                }
                                else
                                {
                                    Toast.ShowToast(message);
                                }
                                IsActivated(false);
                            }
                        }
                    }

                    //Device Activation SUCCESS from Server
                    else
                    {
                        //Retrieving Lic key from XML
                        foreach (var licKey in xmlDoc.Descendants("LicKey"))
                        {
                            LicKey                   = licKey.Value;
                        }

                        //Retrieving Client name from XML
                        foreach (var name in xmlDoc.Descendants("Name"))
                        {
                            Name                     = name.Value;
                        }
                        foreach (var activationCode in xmlDoc.Descendants("ActivationCode"))
                        {
                            baseModel.ActivationCode = activationCode.Value;
                        }
                        VerifyLicense(xmlDoc, baseModel.ActivationCode);
                    }
                }
            }
        }

        public void GotHttpResponse_Deactivation(string xml)
        {
            string message      = null;
            string detail       = xml.Trim();

            if (!detail.StartsWith("<") && !detail.EndsWith(">"))
            {
                ShowServerResponse(ApplicationConstants.Error, xml);
            }
            else
            {
                XDocument xmlDoc = XDocument.Parse(xml);
                foreach (var Result in xmlDoc.Descendants("Result"))
                {
                    foreach (var msg in xmlDoc.Descendants("Message"))
                    {
                        message = msg.Value.ToString();
                    }

                    //Device activation failure from Server
                    if (Result.Value.ToString().Equals("True"))
                    {
                        using (var SureLockKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\42gears\\SureLock", true))
                        {
                            if (SureLockKey != null)
                            {
                                SureLockKey.SetValue("ActivationCode", "");
                                SureLockKey.SetValue("LicKey", "");
                                SureLockKey.SetValue("Name", "");
                                SureLockKey.SetValue("RenewKey", "");
                            }
                        }
                            
                        baseModel.ActivationCode = null;
                        IsActivated(false);
                    }
                    ShowServerResponse("Deactivation", message);
                }
            }
        }

        private void VerifyLicense(XDocument xmlDoc,string ActivationCode)
        {
            bool isKeyExpired           = false;
            KeyInfo keyInfo             = null;
            bool isValid                = Verifier.IsLicenseValid(LicKey, RenewKey, MacWifi, MacBluetooth, GUID, out isKeyExpired, out keyInfo);

            //NULL check not needed here as server will send us a valid License Key
            baseModel.LicKeyInfo        = keyInfo;

            if (isValid)
            {
                using (var SureLockKey = Registry.LocalMachine.OpenSubKey(@"Software\42Gears\SureLock", true))
                {
                    if (SureLockKey != null)
                    {
                        SureLockKey.SetValue("LicKey", LicKey);
                        SureLockKey.SetValue("Name", Name);
                        SureLockKey.SetValue("ActivationCode", MD5.EncryptString(ActivationCode, ApplicationConstants.PASSPHRASE));
                        SureLockKey.SetValue("Offline", 0, RegistryValueKind.DWord);
                        SureLockKey.SetValue("RenewKey", "");
                        SureLockKey.Close();
                    }
                }

                foreach (var Message in xmlDoc.Descendants("Message"))
                {
                    ShowServerResponse(ApplicationConstants.ActivationResponseTitle, Message.Value.ToString());
                }

                baseModel.licensedTo    = baseModel.LicKeyInfo.licensedTo;
                SetExpiryDate(baseModel.LicKeyInfo.expiryDate);
                IsActivated(true);
            }
            else if (isKeyExpired)
            {
                ShowServerResponse(ApplicationConstants.ActivationResponseTitle, "Error activating the device.Error code: 1069");
                IsActivated(false);
            }
            else
            {
                ShowServerResponse(ApplicationConstants.ActivationResponseTitle, baseModel.LicKeyInfo.ToString());
                IsActivated(false);
            }
        }

        public void DeActivateDevice(string ActivationCode, string name, string Lickey)
        {
            string activationCode   = null;
            try
            {
                activationCode      = MD5.DecryptString(ActivationCode, ApplicationConstants.PASSPHRASE);
                ActivationCode      = activationCode;
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }

            HTTP httpObject         = new HTTP();
            httpObject.AsyncHttpPost(ApplicationConstants.DeactivationServerUrl, GetDeActivationXML(ActivationCode, name, Lickey), GotHttpResponse_Deactivation);
        }

        private void IsActivated(bool value)
        {
            Activated?.Invoke(this, value);
        }

        /// <summary>
        /// Method to Set the date in About page
        /// </summary>
        /// <param name="shortDate"></param>
        private void SetExpiryDate(ShortDate shortDate)
        {
            string expiryDate    = "";

            //Set the month
            switch (shortDate.Month)
            {
                case 1:
                    expiryDate  += "Jan ";
                    break;

                case 2:
                    expiryDate  += "Feb ";
                    break;

                case 3:
                    expiryDate  += "Mar ";
                    break;

                case 4:
                    expiryDate  += "Apr ";
                    break;

                case 5:
                    expiryDate  += "May ";
                    break;

                case 6:
                    expiryDate  += "Jun ";
                    break;

                case 7:
                    expiryDate  += "Jul ";
                    break;

                case 8:
                    expiryDate  += "Aug ";
                    break;

                case 9:
                    expiryDate  += "Sep ";
                    break;

                case 10:
                    expiryDate  += "Oct ";
                    break;

                case 11:
                    expiryDate  += "Nov ";
                    break;

                case 12:
                    expiryDate  += "Dec ";
                    break;
            }

            expiryDate          += shortDate.Day + ",";
            expiryDate          += (2000 + shortDate.Year);
            FreeUpgradeDate      = expiryDate;
        }

        public void GetDeviceInformation()
        {
            var query       = new SelectQuery("Win32_OperatingSystem");
            var searcher    = new ManagementObjectSearcher(query);
            try
            {
                foreach (ManagementObject envVar in searcher.Get())
                {
                    OS      = envVar["Caption"].ToString();
                }
                query       = new SelectQuery("Win32_ComputerSystem");
                searcher    = new ManagementObjectSearcher(query);
                foreach (ManagementObject envVar in searcher.Get())
                {
                    Model   = envVar["Model"].ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.LogException(exp);
            }
        }

        private string GetActivationXML(string ActivationCode)
        {
            string xmlOutput            = null;
            string notAvailableString   = "Not Available";
            try
            {
                var wSettings           = new XmlWriterSettings();
                wSettings.Indent        = true;
                MemoryStream ms         = new MemoryStream();
                GetDeviceInformation();
                XmlWriter xw            = XmlWriter.Create(ms, wSettings);// Write Declaration
                xw.WriteStartDocument();
                xw.WriteStartElement("Request");
                xw.WriteElementString("ActivationCode", ActivationCode);
                xw.WriteElementString("OS", OS);
                xw.WriteElementString("Model", Model);

                //Query preferred activation id type
                switch (baseModel.PreferredActivationID)
                {
                    //NONE
                    case 0:
                        xw.WriteElementString("WIFI_MAC", MacWifi.Equals(notAvailableString) ? string.Empty : MacWifi);
                        xw.WriteElementString("BT_MAC", MacBluetooth.Equals(notAvailableString) ? string.Empty : MacBluetooth);
                        xw.WriteElementString("GUID", GUID);
                        break;

                    //WIFI MAC
                    case 1:
                        xw.WriteElementString("WIFI_MAC", MacWifi.Equals(notAvailableString) ? string.Empty : MacWifi);
                        xw.WriteElementString("BT_MAC", string.Empty);
                        xw.WriteElementString("GUID", string.Empty);
                        break;

                    //BT MAC
                    case 2:
                        xw.WriteElementString("WIFI_MAC", string.Empty);
                        xw.WriteElementString("BT_MAC", MacBluetooth.Equals(notAvailableString) ? string.Empty : MacBluetooth);
                        xw.WriteElementString("GUID", string.Empty);
                        break;

                    //GUID
                    case 3:
                        xw.WriteElementString("WIFI_MAC", string.Empty);
                        xw.WriteElementString("BT_MAC", string.Empty);
                        xw.WriteElementString("GUID", GUID);
                        break;

                    default:
                        xw.WriteElementString("WIFI_MAC", MacWifi.Equals(notAvailableString) ? string.Empty : MacWifi);
                        xw.WriteElementString("BT_MAC", MacBluetooth.Equals(notAvailableString) ? string.Empty : MacBluetooth);
                        xw.WriteElementString("GUID", GUID);
                        break;

                }                

                xw.WriteElementString("Product", ApplicationConstants.SURELOCKWIN8.ToString());
                xw.WriteElementString("KeyVerifierVersion", ApplicationConstants.KeyVerifierVersion);
                xw.WriteEndElement();
                xw.WriteEndDocument();
                xw.Flush();

                Byte[] buffer   = new Byte[ms.Length];
                buffer          = ms.ToArray();
                xmlOutput       = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                buffer          = null;
                ms.Flush();
                ms.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
            return xmlOutput;
        }

        private string GetDeActivationXML(string ActivationCode, string name, string Lickey)
        {
            string notAvailableString   = "Not Available";
            string xmlOutput            = null;
            try
            {
                var wSettings           = new XmlWriterSettings();
                wSettings.Indent        = true;
                MemoryStream ms         = new MemoryStream();
                GetDeviceInformation();
                XmlWriter xw            = XmlWriter.Create(ms, wSettings);// Write Declaration
                xw.WriteStartDocument();
                xw.WriteStartElement("Request");
                xw.WriteElementString("ActivationCode", ActivationCode);
                xw.WriteElementString("OS", OS);
                xw.WriteElementString("Model", Model);
                xw.WriteElementString("WIFI_MAC", MacWifi.Equals(notAvailableString) ? string.Empty : MacWifi);
                xw.WriteElementString("BT_MAC", MacBluetooth.Equals(notAvailableString) ? string.Empty : MacBluetooth);
                //xw.WriteElementString("EthernetMAC",infoObject.EthernetMAC);
                xw.WriteElementString("GUID", GUID);
                xw.WriteElementString("Product", ApplicationConstants.SURELOCKWIN8.ToString());
                xw.WriteElementString("Name", name);
                xw.WriteElementString("LicKey", Lickey);
                xw.WriteEndElement();
                xw.WriteEndDocument();
                xw.Flush();

                Byte[] buffer           = new Byte[ms.Length];
                buffer                  = ms.ToArray();
                xmlOutput               = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                buffer                  = null;
                ms.Flush();
                ms.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
            return xmlOutput;
        }

        private void ShowServerResponse(string title,string message)
        {
            ServerResponse?.Invoke(title, message);
        }

        public string MacWifi
        {
            get { return baseModel.MacWifi; }
        }

        public string MacBluetooth
        {
            get { return baseModel.MacBluetooth; }
        }

        public string GUID
        {
            get { return baseModel.GUID; }
        }

        public string FreeUpgradeDate
        {
            get { return baseModel.LicTill; }
            set
            {
                baseModel.LicTill = value;
                OnPropertyChanged("FreeUpgradeDate");
            }
        }

        string _renewKey = string.Empty;
        public string RenewKey
        {
            get
            {
                return _renewKey;
            }
            set
            {
                _renewKey = value;
                OnPropertyChanged("RenewKey");
            }
        }

        string _licKey;
        public string LicKey
        {
            get { return _licKey; }
            set
            {
                _licKey = value;
                OnPropertyChanged("LicKey");
            }
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        string _osName;
        public string OS
        {
            get { return _osName; }
            set
            {
                _osName = value;
                OnPropertyChanged("OS");
            }
        }

        string _model;
        public string Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged("Model");
            }
        }
    }
}
