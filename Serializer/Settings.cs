﻿using SureLockWin8.Model;
using System;
using System.Collections.ObjectModel;
using System.IO;
using Utility;
using Utility.Common;
using Utility.Model;

namespace Serializer
{
    public class Settings
    {
        public static RootModel Load(string path)
        {
           RootModel rm = ObjectXMLSerializer<RootModel>.Load(path);
           //ValidateSettings(ref rm);
           //Save(rm);
           return rm;
        }

        internal static void Save(RootModel baseModel, string filename = "", bool exportActivationCode = false)
        {
            string path = @"C:\Users\Anand130\Desktop\SureLock.settings";
            string settingsPath = path;
            baseModel.ssModel.ProductVersion = "2.25";
            string xml = ObjectXMLSerializer<RootModel>.Save(baseModel, null, SerializedFormat.String, exportActivationCode);
            File.WriteAllText(settingsPath, xml);
        }

        private static void SetNumberOfTabs(ref RootModel rm)
        {
            if (rm.ssModel != null)
            {
                if (rm.browserSettings.DisplaySettingsModel.NumberOfTabs < 1 || rm.browserSettings.DisplaySettingsModel.NumberOfTabs > 8)
                {
                    rm.browserSettings.DisplaySettingsModel.NumberOfTabs = 8;
                }
            }
        }

        private static void SetNumberOfTaps(ref RootModel rm)
        {
            if (rm.ssModel != null)
            {
                if(rm.ssModel.NumberOfTaps < 4 || rm.ssModel.NumberOfTaps > 60)
                {
                    rm.ssModel.NumberOfTaps = 5;
                }
            }
        }

        public static void SetIconSize(ref RootModel rm)
        {
           // CheckFont_IconProperties(ref rm);
            if (rm.ssModel.IconSize != 0)
            {
                //Allowed Applications
                foreach (ApplicationSettingsModel asm in rm.allowedAppsModel.allowedapps)
                {
                    asm.IconSize        = rm.ssModel.IconSize;
                    asm.FontSize        = rm.ssModel.FontSize;
                    asm.FontFamily      = rm.ssModel.FontFamily;
                    asm.FontColor       = rm.ssModel.FontColor;
                }

                //Allowed Website
                foreach (WebsiteSettingsModel wsm in rm.allowedWebsitesModel.allowedWebsites)
                {
                    wsm.IconSize        = rm.ssModel.IconSize;
                    wsm.FontSize        = rm.ssModel.FontSize;
                    wsm.FontFamily      = rm.ssModel.FontFamily;
                    wsm.FontColor       = rm.ssModel.FontColor;
                }
            }
        }

        public static void SetScreensaverTimeout(ref RootModel rm)
        {
            if (rm != null && rm.ssModel != null)
            {
                if (rm.ssModel.ScreensaverSettingsModel.ScreensaverTimeout > rm.ssModel.TimeoutSettingsModel.idleTimeoutInterval)
                {
                    rm.ssModel.ScreensaverSettingsModel.ScreensaverTimeout = rm.ssModel.TimeoutSettingsModel.idleTimeoutInterval;
                }
            }
        }

        //public static void CheckFont_IconProperties(ref RootModel rm)
        //{
        //    ObservableCollection<string> fontNames = new ObservableCollection<string>();
        //    foreach (System.Drawing.FontFamily family in System.Drawing.FontFamily.Families)
        //    {
        //        fontNames.Add(family.Name);
        //    }

        //    if (!fontNames.Contains(rm.ssModel.FontFamily))
        //    {
        //        rm.ssModel.FontFamily = "Tahoma";
        //    }

        //    if (rm.ssModel.FontSize < ApplicationConstants.FontSizeStart || rm.ssModel.FontSize > ApplicationConstants.FontSizeEnd)
        //    {
        //        rm.ssModel.FontSize = 12;
        //    }

        //    try
        //    {
        //        if (rm.ssModel.IconSize == 0)
        //        {
        //            rm.ssModel.IconSize = 50;
        //        }

        //        if (rm.ssModel.IconSize > System.Windows.SystemParameters.PrimaryScreenWidth * 0.8)
        //        {
        //            rm.ssModel.IconSize = Convert.ToInt32(System.Windows.SystemParameters.PrimaryScreenWidth * 0.8);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        public static int SetTextSize(int iconSize)
        {
            try
            {
                int textSize    = 20;
                switch (iconSize)
                {
                    case 50:
                        textSize = 10;
                        break;
                    case 100:
                        break;
                    case 200:
                        textSize = textSize * 2;
                        break;
                    case 400:
                        textSize = textSize * 4;
                        break;
                   default:
                        textSize = (textSize * iconSize / 100);
                        break;

                }
                return textSize;
            }
            catch (Exception)
            {
                return 12;
            }
        }

        public static void ValidateSettings(ref RootModel rm)
        {
            SetIconSize(ref rm);
            SetNumberOfTaps(ref rm);
            SetNumberOfTabs(ref rm);
            SetScreensaverTimeout(ref rm);
            SetDefaultZoomLevel(ref rm);
            SetScreensaver(ref rm);
        }

        private static void SetScreensaver(ref RootModel rm)
        {
            if (!string.IsNullOrWhiteSpace(rm.ssModel.ScreensaverSettingsModel.ScreensaverPath) && rm.ssModel.ScreensaverSettingsModel.ScreensaverMedia.screensaverMediaModel.Count == 0)
            {
                rm.ssModel.ScreensaverSettingsModel.ScreensaverMedia.screensaverMediaModel.Add(new ScreensaverMediaModel { MediaPath = rm.ssModel.ScreensaverSettingsModel.ScreensaverPath});
            }
        }

        private static void SetDefaultZoomLevel(ref RootModel rm)
        {
            if (rm != null && rm.browserSettings != null && rm.browserSettings.DisplaySettingsModel != null)
            {
                if (rm.browserSettings.DisplaySettingsModel.DefaultZoomLevel < 10 || rm.browserSettings.DisplaySettingsModel.DefaultZoomLevel > 1000)
                {
                    rm.browserSettings.DisplaySettingsModel.DefaultZoomLevel = 100;
                }
            }
        }
    }
}
