﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using Utility.Common;
using Utility.Enum;
using Utility.Model;

namespace Utility.License
{
    public class Verifier
    {
        #region Public Methods
        
        public static bool IsLicenseValid(string LicKey, string RenewKey, string MacWifi, string MacBluetooth, string GUID, out bool isKeyExpired, out KeyInfo keyInfo)
        {
            bool isValid                    = false;
            isKeyExpired                    = false;
            keyInfo                         = null;

            try
            {
                keyInfo                     = VerifyKey(LicKey, RenewKey, MacWifi, MacBluetooth, GUID);

                if(keyInfo != null && keyInfo.errorCode == 0)
                {
                    //Perpetual License
                    if (keyInfo.license_Type == LicenseType.PERPETUAL)
                    {
                        isKeyExpired         = IsKeyExpired(keyInfo.expiryDate, ApplicationConstants.BUILD_DATE);
                    }

                    //Subscription License
                    else if(keyInfo.license_Type == LicenseType.SUBSCRIPTION)
                    {
                        var serverDate       = Utils.GetServerDate();
                        var date             = serverDate.Value.ToString(ApplicationConstants.BuildDateFormatString);
                        var expiryDate       = new ShortDate(keyInfo.expiryDate.Day, keyInfo.expiryDate.Month, keyInfo.expiryDate.Year, false);
                        isKeyExpired         = IsKeyExpired(expiryDate, date);
                    }

                    isValid                  = keyInfo.isVerified && !isKeyExpired;

                    if(keyInfo.isValidKey && !isKeyExpired)
                    {
                        SureLockInfo.ClearLicenseInfo();
                    }
                }
                
            }
            catch (Exception)
            {
            }

            return isValid;
        }

        public static bool IsLicenseValidInGracePeriod(KeyInfo keyInfo)
        {
            bool result                         = false;
            var surelockInfo                    = SureLockInfo.GetInfo();

            if(keyInfo != null)
            {
                DateTime? gracePeriod           = GetGracePeriod(keyInfo, surelockInfo);

                if(gracePeriod != null && gracePeriod.HasValue)
                {
                    string referenceDate        = string.Empty;

                    if(keyInfo.license_Type == LicenseType.PERPETUAL)
                    {
                        referenceDate           = ApplicationConstants.BUILD_DATE;
                    }
                    else if(keyInfo.license_Type == LicenseType.SUBSCRIPTION)
                    {
                        var serverDate          = Utils.GetServerDate();
                        referenceDate           = serverDate.Value.ToString(ApplicationConstants.BuildDateFormatString);
                    }

                    result                      = !IsKeyExpired(gracePeriod.Value, referenceDate);

                    //True  -> Not expired
                    //False -> Expired

                    SureLockInfo.Save(surelockInfo);
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        private static DateTime? GetGracePeriod(KeyInfo keyInfo, SureLockInfoModel surelockInfo)
        {
            DateTime? expiryDate                    = null;

            if(keyInfo != null && surelockInfo != null)
            {
                if(keyInfo.license_Type == LicenseType.PERPETUAL)
                {
                    expiryDate                      = GetBuildExpiryDate(surelockInfo);
                    surelockInfo.ExpiredBuildDate   = expiryDate.Value.ToString(ApplicationConstants.BuildDateFormatString);
                }
                else if(keyInfo.license_Type == LicenseType.SUBSCRIPTION)
                {
                    var date                        = keyInfo.expiryDate;
                    expiryDate                      = new DateTime(date.Year + 2000, date.Month, date.Day);
                }

                //Add 1 month grace period
                if(expiryDate != null && expiryDate.HasValue)
                {                    
                    expiryDate                      = expiryDate.Value.Date.AddMonths(1);
                }
            }

            return expiryDate;
        }
        
        private static DateTime GetBuildExpiryDate(SureLockInfoModel surelockInfo)
        {
            var buildExpiryDate         = DateTime.Now;

            try
            {
                if(surelockInfo != null)
                {
                    if(!string.IsNullOrWhiteSpace(surelockInfo.ExpiredBuildDate))
                    {
                        buildExpiryDate = DateTime.ParseExact(surelockInfo.ExpiredBuildDate, ApplicationConstants.BuildDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Logger.LogException(e);
            }

            return buildExpiryDate;
        }

        /// <summary>
        /// Method to check if License key has expired
        /// </summary>
        /// <param name="date"></param>
        /// <param name="referenceDate"></param>
        /// <returns></returns>
        private static bool IsKeyExpired(ShortDate date,string referenceDate)
        {
            return IsKeyExpired(new DateTime(date.Year + 2000, date.Month, date.Day), referenceDate);
        }

        private static bool IsKeyExpired(DateTime expiryDate, string referenceDate)
        {
            try
            {
                //Adding one because Jan->0 & Dec->11
                //date.Month          += 1;
                DateTime buildDate   = DateTime.ParseExact(referenceDate, "dd/MM/yy", CultureInfo.InvariantCulture);
                int result           = DateTime.Compare(expiryDate, buildDate);
                
                //Key has not expired
                if (result >= 0)
                {
                    return false;
                }
                
                //Key has expired
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return true;
        }

        /// <summary>
        /// Method to Verify Key which uses KeyVerifier
        /// </summary>
        /// <param name="LicKey"></param>
        /// <param name="RenewKey"></param>
        /// <param name="MacWifi"></param>
        /// <param name="MacBluetooth"></param>
        /// <param name="GUID"></param>
        /// <returns></returns>
        private static KeyInfo VerifyKey(string LicKey,string RenewKey, string MacWifi, string MacBluetooth, string GUID)
        {
            try
            {
                KeyInfo info = CheckKeyVerifier4(LicKey, RenewKey, MacWifi, MacBluetooth, GUID);

                if (info != null && !info.isVerified)
                {
                    info     = CheckKeyVerifier2(LicKey, MacWifi, MacBluetooth, GUID);
                }

                //0->December
                //if (info.expiryDate.Month == 0)
                //{
                //    info.expiryDate.Month = 12;
                //}

                info.expiryDate.Month               += 1;

                return info;
            }
            catch (Exception)
            {
            }

            return null;
        }

        private static KeyInfo CheckKeyVerifier4(string LicKey, string RenewKey,string MacWifi, string MacBluetooth, string GUID)
        {
            //Determines which parameter to validate with the Lic Key
            var info                                = KeyVerifier4.verifyKey(LicKey, RenewKey, null, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));

            switch (info.type)
            {
                case DeviceIDType.WIFI_MAC: info    = KeyVerifier4.verifyKey(LicKey, RenewKey, MacWifi, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;

                case DeviceIDType.BT_MAC:   info    = KeyVerifier4.verifyKey(LicKey, RenewKey, MacBluetooth, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;

                case DeviceIDType.GUID:     info    = KeyVerifier4.verifyKey(LicKey, RenewKey, GUID, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;
            }

            return info;
        }

        private static KeyInfo CheckKeyVerifier2(string LicKey, string MacWifi, string MacBluetooth, string GUID)
        {
            //Determines which parameter to validate with the Lic Key
            var info                                = KeyVerifier2.verifyKey(LicKey, null, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));

            switch (info.type)
            {
                case DeviceIDType.WIFI_MAC: info    = KeyVerifier2.verifyKey(LicKey, MacWifi, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;

                case DeviceIDType.BT_MAC:   info    = KeyVerifier2.verifyKey(LicKey, MacBluetooth, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;

                case DeviceIDType.GUID:     info    = KeyVerifier2.verifyKey(LicKey, GUID, Convert.ToInt32(ApplicationConstants.SURELOCKWIN8));
                    break;
            }

            return info;
        }

        #endregion
    }
}
